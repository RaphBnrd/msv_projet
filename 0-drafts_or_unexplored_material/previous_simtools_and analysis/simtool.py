# -*- coding: utf-8 -*-
"""
Created on Wed Jan  3 18:56:42 2024

@author: cress
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.integrate import odeint
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
import deepxde as dde
from deepxde.backend import tf
import matplotlib.pyplot as plt
import datetime
import random
import time
import re

default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']
#   dde.config.real.set_float64()

def feature_transform(t):
        return tf.concat(
            (
                t,
                tf.sin(t),
                tf.sin(2 * t),
                tf.sin(3 * t),
                tf.sin(4 * t),
                tf.sin(5 * t),
                tf.sin(6 * t),
            ),
            axis=1,
        )

# toutes les notations utilisées doivent être calés sur ceux du papier

#On retire le paramètre c inutile dans note configuration

class sample_plan:
    
        def __init__(self, tmax=500, nbpoint=50, plan_type="uniform", batch_params=None, custom_plan=None):
            """
            tmax : float
                temps maximum de la simulation
            nbpoint : int
                nombre de points de la simulation
            plan_type : str
                plan d'échantillonnage, peut être "uniform", "random", "batch_random", "batch_uniform" or "custom"
            batch_params : dict
                dictionnaire contenant les paramètres de l'échantillonnage batch
                    > nbr_batch : int
                        nombre de batch (doit être un entier, divisant nbpoint)
                    > ratio_in_out_batch : float
                        ratio entre la durée d'échantillonnage dans le batch et la durée totale
            custom_plan : array
                plan d'échantillonnage personnalisé (array des temps d'échantillonnage)
            """
            self.tmax = tmax
            self.nbpoint = nbpoint
            self.plan_type = plan_type
            self.batch_params = batch_params
            self.custom_plan = custom_plan
            self.t_samples = None
            self.get_samples()
    
        def get_samples(self):
            if self.plan_type == "uniform":
                self.t_samples = np.linspace(0, self.tmax, self.nbpoint)
            elif self.plan_type == "random":
                self.t_samples = np.random.uniform(0, self.tmax, self.nbpoint)
            elif self.plan_type in ["batch_random", "batch_uniform"]:
                if self.batch_params == None:
                    print("Batch parameters not provided")
                elif self.batch_params['ratio_in_out_batch'] > 1:
                    print("Ratio in/out batch must be lower than 1")
                else:
                    self.t_samples = np.array([])
                    for i in range(self.batch_params['nbr_batch']):
                        t_min_this_batch = i / self.batch_params['nbr_batch'] * self.tmax
                        t_max_this_batch = t_min_this_batch + \
                            self.batch_params['ratio_in_out_batch'] \
                                * self.tmax / self.batch_params['nbr_batch']
                        if self.plan_type == "batch_random":
                            self.t_samples = np.append(
                                self.t_samples,
                                np.random.uniform(t_min_this_batch, t_max_this_batch,
                                                  int(self.nbpoint / self.batch_params['nbr_batch']))
                            )
                        elif self.plan_type == "batch_uniform":
                            self.t_samples = np.append(
                                self.t_samples,
                                np.linspace(t_min_this_batch, t_max_this_batch,
                                             int(self.nbpoint / self.batch_params['nbr_batch']))
                        )
            elif self.plan_type == "custom":
                self.t_samples = self.custom_plan
                self.nbpoint = len(self.t_samples)
                self.tmax = np.max(self.t_samples)
            else:
                print("Sample plan not recognized")

            if self.t_samples is not None:
                self.t_samples = np.sort(self.t_samples)
        
        def plot_samples(self):
            plt.scatter(self.t_samples, np.zeros_like(self.t_samples))
            plt.title(f"Sample plan - {self.plan_type}")
            plt.show()


class data_gen:

    def __init__(self, param, tmax, init_val, func_rep='holling1'):
        self.tmax = tmax # t0 vaut 0, tmax horizon de la simulation
        self.params = param
        self.param_lv = param[0:4] # de forme [a1,a2,e,K]
        self.param_frep = param[4:] # de forme [a,S,b]
        self.func_rep = func_rep
        self.init = init_val # forme [x1_init, x2_init] conditions initiales
        self.simulation = None
        self.t_samples = None
        self.data_noisy = None
        self.func_rep_dict = {'holling1' :     self.holling1,
                              'holling1_sat' : self.holling1_sat,
                              'holling2' :     self.holling2,
                              'holling3' :     self.holling3
                              }

    def holling1(self, x):
        a = self.param_frep[0]
        return a*x

    def holling1_sat(self, x):
        a, S = self.param_frep[0:2]
        e = self.param_lv[2]
        return np.min(a*x, S/e)

    def holling2(self, x):
        a, S, b = self.param_frep
        return (a*x) / (1 + a*b*x)

    def holling3(self, x):
        a, S, b = self.param_frep
        return (a*x**2) / (1 + a*b*x**2)

    def lv_model(self, state, t):
        a1, a2, e, K = self.param_lv
        x1, x2 = state
        frep_predateur = self.func_rep_dict[self.func_rep](x2)
        dx1_dt = -a1*x1 + e*x1*frep_predateur
        dx2_dt = a2*x2*(1-K*x2) - x1*frep_predateur
        return np.array([dx1_dt, dx2_dt])

    def simulate(self, nb_p_t=15000):
        self.t_simu = np.linspace(0, self.tmax, nb_p_t)
        self.simulation = odeint(self.lv_model, self.init, self.t_simu, atol=1e-8, rtol=1e-11)



    def get_data(self, nbpoint=10, noise_intensity=0, noise_seed=None, plot_data=False, plan=None):
        if plan == None:
            plan = sample_plan(self.tmax, nbpoint, "uniform")
        if np.all(self.simulation) == None:
            self.simulate()
        np.random.seed(seed = noise_seed)
        self.noise_seed = noise_seed
        t_samples = plan.t_samples
        interp_func = interp1d(self.t_simu, self.simulation.T, kind='cubic')
        data_true = interp_func(t_samples).T
        mu1, mu2, sigma1, sigma2 = 0, 0, 1, 1
        # Add Gaussian noise to each column independently
        data_noisy = np.column_stack([
            data_true[:, 0] + noise_intensity * np.random.normal(mu1, sigma1, data_true.shape[0]),
            data_true[:, 1] + noise_intensity * np.random.normal(mu2, sigma2, data_true.shape[0])
        ])
        data_noisy = np.maximum(data_noisy, 0) # negative values are replaced by zeros

        self.data_noisy = data_noisy
        self.t_samples = t_samples.reshape((nbpoint,1))

        if plot_data:
            self.plot_initial_data()


    def plot_initial_data(self):
        if np.all(self.simulation) == None:
            self.simulate()

        plt.plot(self.t_simu, self.simulation[:, 0], '--', alpha=.5,
                 color=default_col_pal[0], label="predator model")
        plt.scatter(self.t_samples, self.data_noisy[:, 0],
                    color=default_col_pal[0], label="Noisy data")
        plt.plot(self.t_simu, self.simulation[:, 1], '--', alpha=.5,
                 color=default_col_pal[1], label="Prey model")
        plt.scatter(self.t_samples, self.data_noisy[:, 1],
                    color=default_col_pal[1], label="Noisy data")
        plt.legend()
        plt.ylim(bottom=0)
        plt.title("Lotka-Voltera simulation")

class pinn:

    def __init__(self, data): # data must be a datagen class
        self.data = data
        np.random.seed(seed=self.data.noise_seed)
        #initial_guess_estim = 1
        #the following was edited to have random initial value in order to test robustness
        self.a1_estim = dde.Variable(0.05)
        self.a2_estim = dde.Variable(0.05)
        self.e_estim = dde.Variable(1.0)
        #self.c_estim = dde.Variable(1.0)
        self.K_estim = dde.Variable(0.05)
        self.a_estim = dde.Variable(0.05)
        self.S_estim = dde.Variable(0.05)
        self.b_estim = dde.Variable(0.05)

        self.train_param = [
            self.a1_estim,
            self.a2_estim,
            self.e_estim,
                    #self.c_estim,
            self.K_estim,
            self.a_estim,
            self.S_estim,
            self.b_estim
        ]

        self.geom = dde.geometry.TimeDomain(0, self.data.tmax)
        self.y1_init, self.y2_init =  self.data.init
        self.ic1 = dde.icbc.IC(self.geom, lambda X : self.y1_init, self.boundary, component=0)
        self.ic2 = dde.icbc.IC(self.geom, lambda X : self.y2_init, self.boundary, component=1)
        self.func_rep_dict = {'holling1' :     self.holling1,
                              'holling1_sat' : self.holling1_sat,
                              'holling2' :     self.holling2,
                              'holling3' :     self.holling3
                              }
        self.trained = False
        self.time_last_train_in_sec = None

    def holling1(self, x):
        return self.a_estim * x

    def holling1_sat(self, x):
        return np.min(self.a_estim * x, self.S_estim / self.e_estim)

    def holling2(self, x):
        return (self.a_estim * x) / (1 + self.a_estim * self.b_estim * x**2)

    def holling3(self, x):
        xcarre = x**2
        return (self.a_estim * xcarre) / (1 + self.a_estim * self.b_estim * xcarre)


    def lv_system(self, x, y):
        y1, y2 = y[:,0:1], y[:,1:2]
        dy1_x = dde.grad.jacobian(y, x, i=0)
        dy2_x = dde.grad.jacobian(y, x, i=1)
        frep_predateur = self.func_rep_dict[self.data.func_rep](y2)
        return[
            dy1_x + self.a1_estim*y1 - self.e_estim*y1*frep_predateur,
            dy2_x - self.a2_estim*y2*(1-self.K_estim*y2) + y1*frep_predateur
        ]

    def boundary(self, _, on_initial):
        return on_initial

    def get_train_data(self, new_gen_data=False, nbpoint=10, noise_intensity=0, noise_seed=None):
        if new_gen_data or np.all(self.data.data_noisy)==None:
            self.data.get_data(nbpoint, noise_intensity, noise_seed)
        self.observe_t, self.ob_y = self.data.t_samples, self.data.data_noisy
        self.observe_y1 = dde.icbc.PointSetBC(self.observe_t, self.ob_y[:, 0:1], component=0)
        self.observe_y2 = dde.icbc.PointSetBC(self.observe_t, self.ob_y[:, 1:2], component=1)
        self.traindata = dde.data.PDE(
            self.geom,
            self.lv_system,
            [self.ic1, self.ic2, self.observe_y1, self.observe_y2],
            num_domain = 1000,
            num_boundary = 2,
            anchors = self.observe_t,
        )

    def compile_model(self, lr=1e-3,nnlayers = [1] + [128] * 3 + [7],activation_function="swish",initcond = "Glorot normal", poids=None, path_file_var=None , niter_p1=1000, niter_p2 = 100000):

        #Initialisation du modele
        net = dde.nn.FNN(nnlayers, activation_function, initcond) #pourquoi  ceci devrait mieux marcher ?
        #net.apply_feature_transform(feature_transform)

        self.model = dde.Model(self.traindata,net)

        #Initialisation du path du fichier de train
        self.path = path_file_var
        if path_file_var == None:
            formatted_datetime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            path_file_var = formatted_datetime+"-train_param.dat"
        self.variable = dde.callbacks.VariableValue(
            self.train_param, period=1000, filename=path_file_var
        )

        #Initialisation des poids
        if poids == None:
            poids_p1 = None ; poids_p2 = None
        else:
            poids_p1 = poids[0] ; poids_p2 = poids[1]

        #Partie 1 de l'entrainement
        print("Partie 1 de l'entrainement")
        start_time = time.time()
        self.model.compile("adam", lr=lr,loss_weights = poids_p1, external_trainable_variables=self.train_param)
        self.model.train(iterations=niter_p1)#, callbacks=[self.variable])

        #Partie 2 de l'entrainement
        print("Partie 2 de l'entrainement")
        self.model.compile("adam", lr=lr,loss_weights = poids_p2, external_trainable_variables=self.train_param)
        self.losshistory, self.train_state = self.model.train(iterations=niter_p2, callbacks=[self.variable])
        end_time = time.time()
        self.time_last_train_in_sec = end_time - start_time

#edit fonction de compilation ?

    #def train(self, n_iter=50000):
        #start_time = time.time()
        #self.losshistory, self.train_state = self.model.train(iterations=n_iter, callbacks=[self.variable])
        #end_time = time.time()
        #self.trained = True
        #self.time_last_train_in_sec = end_time - start_time

    def predict(self, npoint=5):
        x = self.geom.uniform_points(npoint*self.data.tmax)
        y = self.model.predict(x)
        return x, y

    def recap_train(self):
        tc_minute = self.time_last_train_in_sec//60
        tc_seconde = self.time_last_train_in_sec%60
        print("L'entrainement a pris "+str(int(tc_minute))+" minutes et "+str(int(tc_seconde))+" secondes")

class displayer: #require a trained model

    def __init__(self, model):
        # self.data = data # must be a data generator object
        self.model = model # must be a pinn object (ou plus tard scipy model)

    def basic_plot(self, fname=None, save=False, disp=True):
        # affiche simplement les courbes du modele
        if self.model.trained == False:
            print('Pour un basic plot merci de train le modele a part')

        t_sim, y_sim = self.model.data.t_simu, self.model.data.simulation # simulation
        t_scatter, y_scatter = self.model.observe_t, self.model.ob_y # data
        t_interp, y_interp = self.model.predict() # prediction

        fig, axs = plt.subplots(2, 3, figsize=(18, 5))

        # Plot 1: Simulation and Scatter Points
        axs[0,0].plot(t_sim, y_sim[:, 0], '--', alpha=.5, color=default_col_pal[0], label='Simulation predator')
        axs[0,0].scatter(t_scatter, y_scatter[:, 0], color=default_col_pal[0], label='Data predator')
        axs[0,0].set_title('Dataset predator')
        axs[0,0].legend()

        # Plot 2: Simulation and Interpolation
        axs[0,1].plot(t_sim, y_sim[:, 0], '--', alpha=.5, color=default_col_pal[0], label='Simulation predator')
        axs[0,1].plot(t_interp, y_interp[:, 0], color='royalblue', label='PINN prediction')
        axs[0,1].set_title('True model and prediction')
        axs[0,1].legend()

        # Plot 3: Scatter Points and Interpolation
        axs[0,2].scatter(t_scatter, y_scatter[:, 0], color=default_col_pal[0], label='Data predator')
        axs[0,2].plot(t_interp, y_interp[:, 0], color='royalblue', label='PINN prediction')
        axs[0,2].set_title('Prediction on data')
        axs[0,2].legend()

        # Plot 1: Simulation and Scatter Points
        axs[1,0].plot(t_sim, y_sim[:, 1], '--', alpha=.5, color=default_col_pal[1], label='Simulation Prey')
        axs[1,0].scatter(t_scatter, y_scatter[:, 1], color=default_col_pal[1], label='Data Prey')
        axs[1,0].set_title('Dataset Prey')
        axs[1,0].legend()

        # Plot 2: Simulation and Interpolation
        axs[1,1].plot(t_sim, y_sim[:, 1], '--', alpha=.5, color=default_col_pal[1], label='Simulation Prey')
        axs[1,1].plot(t_interp, y_interp[:, 1], color='tomato', label='PINN prediction')
        axs[1,1].set_title('True model and prediction')
        axs[1,1].legend()

        # Plot 3: Scatter Points and Interpolation
        axs[1,2].scatter(t_scatter, y_scatter[:, 1], color=default_col_pal[1], label='Data Prey')
        axs[1,2].plot(t_interp, y_interp[:, 1], color='tomato', label='PINN prediction')
        axs[1,2].set_title('Prediction on data')
        axs[1,2].legend()


        plt.tight_layout()

        if save == True:

            if fname == None:
               fname = 'basic_plot.jpg'

            plt.savefig(fname,format='jpg')

        if disp==True:

            plt.show()

    def plot_convergence(self,fname_c = "plot_convergence.png"): #feature pas implémenté pour Holling autre que 1
        #get data from the trained model
        iter_eparam = open_datafile(self.model.path)
        #best_param = iter_eparam[key]
        a1,a2,e,K = self.model.data.param_lv  # de forme [a1,a2,e,K]
        a = self.model.data.param_frep
        #check whether or not we are using a fixed_pinn
        if isinstance(self.model, fixed_pinn) == True:
            isfix = self.model.fixval
        else:
            isfix = [False,False,False,False,False,False,False]
        patron_array = [arr[0] for arr in iter_eparam.values()]

        a1_values = attribvalue(0,iter_eparam,isfix) if attribvalue(0,iter_eparam,isfix) != None else a1*np.ones_like(patron_array)
        a2_values = attribvalue(1,iter_eparam,isfix) if attribvalue(1,iter_eparam,isfix) != None else a2*np.ones_like(patron_array)
        e_values = attribvalue(2,iter_eparam,isfix) if attribvalue(2,iter_eparam,isfix) != None else e*np.ones_like(patron_array)
        K_values = attribvalue(3,iter_eparam,isfix) if attribvalue(3,iter_eparam,isfix) != None else K*np.ones_like(patron_array)
        a_values = attribvalue(4,iter_eparam,isfix) if attribvalue(4,iter_eparam,isfix) != None else a*np.ones_like(patron_array)

        #save les graphes de convergence
        print("Evolution de la valeur des paramètres au cours de l'entrainement")

        plt.figure(figsize=(12, 8))

        plt.subplot(3, 2, 1)
        plt.plot(a1_values, label='a1_values')
        plt.plot(a1*np.ones_like(a1_values),label='true value')
        plt.title('Graph for a1_values')
        plt.legend()

        plt.subplot(3, 2, 2)
        plt.plot(a2_values, label='a2_values')
        plt.plot(a2*np.ones_like(a1_values),label='true value')
        plt.title('Graph for a2_values')
        plt.legend()

        plt.subplot(3, 2, 3)
        plt.plot(e_values, label='e_values')
        plt.plot(e*np.ones_like(a1_values),label='true value')
        plt.title('Graph for e_values')
        plt.legend()

        plt.subplot(3, 2, 4)
        plt.plot(K_values, label='K_values')
        plt.plot(K*np.ones_like(a1_values),label='true value')
        plt.title('Graph for K_values')
        plt.legend()

        plt.subplot(3, 2, 5)
        plt.plot(a_values, label='a_values')
        plt.plot(a*np.ones_like(a1_values),label='true value')
        plt.title('Graph for a_values')
        plt.legend()

        plt.tight_layout()
        plt.savefig(fname_c)
        plt.show()

    def plot_tlong(self,sp_model,fname = "plot_tlong.png",key=None):

        trained_param = open_datafile(self.model.path)

        if key == None:
            key , bestparam = list(trained_param.items())[-1]
        else :
            bestparam = trained_param[key]

        if isinstance(self.model, fixed_pinn) == True:
            isfix = self.model.fixval
            upfix = np.zeros(len(isfix))
            nbfix = isfix.count(True)
            fixcount = 0
            for i in range(len(isfix)):
                if isfix[i] == True:
                    upfix[i] = self.model.data.params[i]
                    fixcount += 1
                else :
                    upfix[i] = bestparam[i - fixcount]

            bestparam = upfix
            #print(bestparam)

        tb = np.linspace(0,2000,10000)
        bestestim = sp_model.solver_LV(self.model.data.init, tb, bestparam)
        self.model.data.tmax = 2000
        self.model.data.simulate()
        plt.plot(tb,bestestim,label='pinn estim')
        plt.plot(self.model.data.t_simu,self.model.data.simulation,'--',label='data')
        plt.legend()
        plt.title('PINN prediction')
        plt.savefig(fname)
        plt.show()

class model_scipy:

    def __init__(self,data):

        self.data = data
        self.trained = True #juste pour la forme et homogénéiser avec le PINN.
        self.func_rep_dict = { 'holling1' : self.holling1,
                              'holling1_sat' : self.holling1_sat,
                              'holling2' : self.holling2,
                              'holling3' :self.holling3
                              }

    def get_train_data(self, new_gen_data=False, nbpoint=10, noise_intensity=0, noise_seed=None):
        if new_gen_data or np.all(self.data.data_noisy)==None:
            self.data.get_data(nbpoint, noise_intensity, noise_seed)
        self.observe_t, self.ob_y = self.data.t_samples, self.data.data_noisy
        self.observe_t = self.observe_t.reshape((nbpoint))
        self.observe_y1 = self.ob_y[:,0:1]
        self.observe_y2 = self.ob_y[:,1:2]

    def holling1(self,x,p_frep):
        a = p_frep[0]
        return a*x

    def holling1_sat(self,x,p_frep):
        a , S = p_frep[0:2]
        e = self.param_lv[2]
        return np.min(a*x,S/e)

    def holling2(self,x,p_frep):
        a, S, b = p_frep
        return (a*x)/(1. + a*b*x**2)

    def holling3(self,x,p_frep):
        a, S, b = p_frep
        xcarre = x**2
        return (a*xcarre)/(1. + a*b*xcarre)

    def lv_model(self,state,t,params):
        a1, a2, e, K , a , S , b = params
        p_frep = [a,S,b]
        x1, x2 = state
        frep_predateur = self.func_rep_dict[self.data.func_rep](x2,p_frep)
        dx1_dt = -a1*x1 + e*x1*frep_predateur
        dx2_dt = a2*x2*(1.0-K*x2) - x1*frep_predateur
        return np.array([dx1_dt, dx2_dt])

    def solver_LV(self,state, t, *params):
        return odeint(self.lv_model, state, t, args = params, atol=1e-8, rtol=1e-11)

    def integration_LV(self,state_arr, *params):

        a1, a2, e, K, a, S, b, y1_0, y2_0 = params
        state_0 = [y1_0, y2_0]
        params_LV = [a1,a2,e,K,a,S,b]
        T = self.observe_t
        res0 = self.solver_LV(state_0, T, params_LV)
        res1 = [self.solver_LV(state, [t,t+1], params_LV)[-1] for t, state in enumerate(state_arr[:-1]) ]
        return np.concatenate([res0,res1]).flatten()

    def predict(self,npoint = 500):

        data1_for_fit = self.ob_y
        data2_for_fit = np.concatenate([self.ob_y, self.ob_y[1:]]).flatten()
        p0 = np.concatenate(([0.08 , 0.020 , 0.9, 0.03, 0.08,1.0,1.0],self.data.init))

        params_fit, info_fit = curve_fit(self.integration_LV, data1_for_fit, data2_for_fit, p0=p0)

        t_rebuild = np.linspace(0, self.data.tmax, npoint)
        state_rebuild = self.solver_LV(self.data.init, t_rebuild, params_fit[0:7])

        return t_rebuild,state_rebuild

def open_datafile(filepath):
    steps_hist, param_hist = [], []
    with open(filepath, 'r') as file:
        for line in file:
            steps_hist.append(int(line.split(' ')[0]))
            param_hist.append(
            [float(value) for value in
             re.search(r'\[(.*?)\]', line).group(1).split(', ')]
            )
        steps_hist = np.array(steps_hist)
        param_hist = np.array(param_hist)
        data_dicts = dict(zip(steps_hist,param_hist))
    return data_dicts

def attribvalue(ind,dic,verite):
    nbfix = verite[:ind].count(True)
    if verite[ind]==True:
        return None
    else :
        return [arr[ind - nbfix] for arr in dic.values()]

class fixed_pinn(pinn):

   def __init__(self, data,fixval = [False,False,False,False,False,False,False,False]):

        self.data = data
        np.random.seed(seed=self.data.noise_seed)

        self.fixval = fixval
        self.a1_estim = dde.Variable(0.05) if self.fixval[0]==False else self.data.param_lv[0]
        self.a2_estim = dde.Variable(0.05) if self.fixval[1]==False else self.data.param_lv[1]
        self.e_estim = dde.Variable(1.0) if self.fixval[2]==False else self.data.param_lv[2]
        self.K_estim = dde.Variable(0.05) if self.fixval[3]==False else self.data.param_lv[3]
        self.a_estim = dde.Variable(0.05) if self.fixval[4]==False else self.data.param_frep[0]
        self.S_estim = dde.Variable(0.05) if self.fixval[5]==False else self.data.param_frep[1]
        self.b_estim = dde.Variable(0.05) if self.fixval[6]==False else self.data.param_frep[2]
        plist = [
            self.a1_estim,
            self.a2_estim,
            self.e_estim,
                    #self.c_estim,
            self.K_estim,
            self.a_estim,
            self.S_estim,
            self.b_estim
        ]
        self.train_param = self.update_train_param(plist)

        self.geom = dde.geometry.TimeDomain(0, self.data.tmax)
        self.y1_init, self.y2_init =  self.data.init
        self.ic1 = dde.icbc.IC(self.geom, lambda X : self.y1_init, self.boundary, component=0)
        self.ic2 = dde.icbc.IC(self.geom, lambda X : self.y2_init, self.boundary, component=1)
        self.func_rep_dict = {'holling1' :     self.holling1,
                              'holling1_sat' : self.holling1_sat,
                              'holling2' :     self.holling2,
                              'holling3' :     self.holling3
                              }
        self.trained = False
        self.time_last_train_in_sec = None

   def update_train_param(self,plist):
        train_param_new = []
        for i in range(len(self.fixval)):
            if self.fixval[i] == False:
                train_param_new.append(plist[i])
        return train_param_new
