clear
clf

cd ~/Dropbox/PRIMER/

% GM_interactions_Fox_Proies = readtable("Parametres_modele_ChatRenard-Proies/GM_interactions_Fox-Proies.csv");
% GM_interactions_Cat_Proies = readtable("Parametres_modele_ChatRenard-Proies/GM_interactions_Cat-Proies.csv");
% Recherche_proie_aFoxj_Holling = readtable("Parametres_modele_ChatRenard-Proies/Recherche_proie_aFoxj_Holling_Fox6.5kg.csv");
% Recherche_proie_aCatj_Holling = readtable("Parametres_modele_ChatRenard-Proies/Recherche_proie_aCatj_Holling.csv");
% GM_ressources_alternatives = readtable("Parametres_modele_ChatRenard-Proies/GM_ressources_alternatives_Fox6.5kg.csv");
% Temps_manip_bFoxj_Holling = readtable("Parametres_modele_ChatRenard-Proies/Temps_manip_repos_bFoxj_Holling_Fox6.5kg.csv");
% Temps_manip_bCatj_Holling = readtable("Parametres_modele_ChatRenard-Proies/Temps_manip_repos_bCatj_Holling.csv")
% BMR_pred = readtable("Parametres_modele_ChatRenard-Proies/BMR_pred_Fox6.5kg.csv");
% BMR_pred = table2array(BMR_pred(1:2,2));

Pop=[];

dt = 1; % pas de temps = 1 jour

XR1 = 0.12;
XR2 = 0.057;
XR3 = 2.19;
XR4 = 0.058;
XR5 = 0.91;
XR6 = 1.22;

kappa3 = 0.055;
kappa4 = 0.014;
kappa5 = 0.031;
kappa6 = 0.030;

%% lambda pour H1(S), H2, H3, Autonme, Hiver, Printemps, Ete
% Pr�dateur 1 - Proies (3 � 6)

H1.lambda13A = 0.34;
H1.lambda14A = 0.0013;
H1.lambda15A = 0.27;
H1.lambda16A = 0.16;
H1.lambda13H = 0.091;
H1.lambda14H = 0.0013;
H1.lambda15H = 0.58;
H1.lambda16H = 0.13;
H1.lambda13P = 0.24;
H1.lambda14P = 0.0031;
H1.lambda15P = 1.4;
H1.lambda16P = 0.062;
H1.lambda13E = 0.083;
H1.lambda14E = 0.0051;
H1.lambda15E = 0.18;
H1.lambda16E = 0.47;
H1.lambda13M = 0.19;
H1.lambda14M = 0.0028;
H1.lambda15M = 0.64;
H1.lambda16M = 0.21;

H2.lambda13A = 0.57;
H2.lambda14A = 0.0022;
H2.lambda15A = 0.42;
H2.lambda16A = 0.26;
H2.lambda13H = 0.14;
H2.lambda14H = 0.0018;
H2.lambda15H = 0.91;
H2.lambda16H = 0.15;
H2.lambda13P = 0.35;
H2.lambda14P = 0.0044;
H2.lambda15P = 1.96;
H2.lambda16P = 0.094;
H2.lambda13E = 0.11;
H2.lambda14E = 0.011;
H2.lambda15E = 0.46;
H2.lambda16E = 0.086;
H2.lambda13M = 0.30;
H2.lambda14M = 0.0047;
H2.lambda15M = 0.99;
H2.lambda16M = 0.88;

H3.lambda13A = 0.84;
H3.lambda14A = 0.0014;
H3.lambda15A = 13;
H3.lambda16A = 0.34;
H3.lambda13H = 0.21;
H3.lambda14H = 0.0018;
H3.lambda15H = 5.5;
H3.lambda16H = 0.26;
H3.lambda13P = 1.1;
H3.lambda14P = 0.0045;
H3.lambda15P = 21;
H3.lambda16P = 0.13;
H3.lambda13E = 0.045;
H3.lambda14E = 0.015;
H3.lambda15E = 1.8;
H3.lambda16E = 3.1;
H3.lambda13M = 0.57;
H3.lambda14M = 0.0059;
H3.lambda15M = 10;
H3.lambda16M = 0.97;

% Pr�dateur 2 - Proies (3 � 6)

H1.lambda23A = 0.21;
H1.lambda24A = 0;
H1.lambda25A = 1.2;
H1.lambda26A = 0.38;
H1.lambda23H = 0.27;
H1.lambda24H = 0.0031;
H1.lambda25H = 0.44;
H1.lambda26H = 0.29;
H1.lambda23P = 0.59;
H1.lambda24P = 0.0039;
H1.lambda25P = 0.41;
H1.lambda26P = 0.54;
H1.lambda23E = 0.063;
H1.lambda24E = 0.0021;
H1.lambda25E = 0.22;
H1.lambda26E = 0.47;
H1.lambda23M = 0.29;
H1.lambda24M = 0.0023;
H1.lambda25M = 0.59;
H1.lambda26M = 0.43;

H2.lambda23A = 0.66;
H2.lambda24A = 0;
H2.lambda25A = 3.0;
H2.lambda26A = 0.97;
H2.lambda23H = 0.60;
H2.lambda24H = 0.0067;
H2.lambda25H = 0.58;
H2.lambda26H = 0.61;
H2.lambda23P = 1.5;
H2.lambda24P = 0.012;
H2.lambda25P = 1.2;
H2.lambda26P = 1.6;
H2.lambda23E = 0.17;
H2.lambda24E = 0.0047;
H2.lambda25E = 0.54;
H2.lambda26E = 1.2;
H2.lambda23M = 0.74;
H2.lambda24M = 0.0059;
H2.lambda25M = 0.13;
H2.lambda26M = 1.1;

H3.lambda23A = 1.1;
H3.lambda24A = 0;
H3.lambda25A = 130;
H3.lambda26A = 1.2;
H3.lambda23H = 0.79;
H3.lambda24H = 0.0067;
H3.lambda25H = 3.6;
H3.lambda26H = 0.88;
H3.lambda23P = 5.0;
H3.lambda24P = 0.012;
H3.lambda25P = 14;
H3.lambda26P = 2.6;
H3.lambda23E = 0.073;
H3.lambda24E = 0.0069;
H3.lambda25E = 2.4;
H3.lambda26E = 3.9;
H3.lambda23M = 1.7;
H3.lambda24M = 0.0064;
H3.lambda25M = 37;
H3.lambda26M = 2.2;

%% Temps de d�croissance pr�dateurs

A.Tr1 = 11;
H.Tr1 = 11;
P.Tr1 = 11;
E.Tr1 = 12;
M.Tr1 = 11;
A.Tr2 = 11;
H.Tr2 = 11;
P.Tr2 = 11;
E.Tr2 = 11;
M.Tr2 = 11;

%% Temps de croissance proies

Tr3 = 286;
Tr4 = 238;
Tr5 = 50;
Tr6 = 189;

%% Temps d'interaction

H1.Tc13A = 25;
H1.Tc14A = 6700;
H1.Tc15A = 33;
H1.Tc16A = 53;
H1.Tc13H = 86;
H1.Tc14H = 5900;
H1.Tc15H = 14;
H1.Tc16H = 59;
H1.Tc13P = 33;
H1.Tc14P = 2500;
H1.Tc15P = 5.7;
H1.Tc16P = 130;
H1.Tc13E = 100;
H1.Tc14E = 1700;
H1.Tc15E = 48;
H1.Tc16E = 18;
H1.Tc13M = 44;
H1.Tc14M = 3100;
H1.Tc15M = 13;
H1.Tc16M = 41;

H2.Tc13A = 15;
H2.Tc14A = 3900;
H2.Tc15A = 20;
H2.Tc16A = 33;
H2.Tc13H = 56;
H2.Tc14H = 4300;
H2.Tc15H = 8.8;
H2.Tc16H = 51;
H2.Tc13P = 22;
H2.Tc14P = 1800;
H2.Tc15P = 4.1;
H2.Tc16P = 91;
H2.Tc13E = 79;
H2.Tc14E = 810;
H2.Tc15E = 19;
H2.Tc16E = 9.6;
H2.Tc13M = 28;
H2.Tc14M = 1.800;
H2.Tc15M = 8.8;
H2.Tc16M = 23;

H3.Tc13A = 10;
H3.Tc14A = 6100;
H3.Tc15A = 0.66;
H3.Tc16A = 25;
H3.Tc13H = 37;
H3.Tc14H = 4300;
H3.Tc15H = 1.4;
H3.Tc16H = 30;
H3.Tc13P = 7.4;
H3.Tc14P = 1800;
H3.Tc15P = 0.38;
H3.Tc16P = 61;
H3.Tc13E = 190;
H3.Tc14E = 550;
H3.Tc15E = 4.3;
H3.Tc16E = 2.7;
H3.Tc13M = 15;
H3.Tc14M = 1.400;
H3.Tc15M = 0.79;
H3.Tc16M = 8.8;

H1.Tc23A = 44;
H1.Tc24A = 10000;
H1.Tc25A = 7.6;
H1.Tc26A = 25;
H1.Tc23H = 34;
H1.Tc24H = 3000;
H1.Tc25H = 22;
H1.Tc26H = 31;
H1.Tc23P = 16;
H1.Tc24P = 2400;
H1.Tc25P = 23;
H1.Tc26P = 17;
H1.Tc23E = 150;
H1.Tc24E = 4500;
H1.Tc25E = 42;
H1.Tc26E = 20;
H1.Tc23M = 33;
H1.Tc24M = 4100;
H1.Tc25M = 16;
H1.Tc26M = 22;

H2.Tc23A = 14;
H2.Tc24A = 9999999999;
H2.Tc25A = 3.2;
H2.Tc26A = 9.6;
H2.Tc23H = 15;
H2.Tc24H = 1400;
H2.Tc25H = 16;
H2.Tc26H = 15;
H2.Tc23P = 6.2;
H2.Tc24P = 760;
H2.Tc25P = 7.6;
H2.Tc26P = 5.7;
H2.Tc23E = 56;
H2.Tc24E = 2000;
H2.Tc25E = 18;
H2.Tc26E = 7.8;
H2.Tc23M = 13;
H2.Tc24M = 1600;
H2.Tc25M = 72;
H2.Tc26M = 8.3;

H3.Tc23A = 8.5;
% H3.Tc24A = NA;
H3.Tc25A = 0.074;
H3.Tc26A = 7.7;
H3.Tc23H = 12;
H3.Tc24H = 1400;
H3.Tc25H = 2.6;
H3.Tc26H = 10;
H3.Tc23P = 1.9;
H3.Tc24P = 780;
H3.Tc25P = 0.66;
H3.Tc26P = 3.6;
H3.Tc23E = 130;
H3.Tc24E = 1300;
H3.Tc25E = 3.9;
H3.Tc26E = 2.4;
H3.Tc23M = 5.4;
H3.Tc24M = 1400;
H3.Tc25M = 0.26;
H3.Tc26M = 4.3;

H1.Ta13A = 330;
H1.Ta14A = 2400;
H1.Ta15A = 240;
H1.Ta16A = 130;
H1.Ta13H = 1200;
H1.Ta14H = 2000;
H1.Ta15H = 100;
H1.Ta16H = 150;
H1.Ta13P = 440;
H1.Ta14P = 900;
H1.Ta15P = 42;
H1.Ta16P = 320;
H1.Ta13E = 1400;
H1.Ta14E = 600;
H1.Ta15E = 350;
H1.Ta16E = 46;
H1.Ta13M = 600;
H1.Ta14M = 1100;
H1.Ta15M = 98;
H1.Ta16M = 100;

H2.Ta13A = 200;
H2.Ta14A = 1400;
H2.Ta15A = 150;
H2.Ta16A = 83;
H2.Ta13H = 760;
H2.Ta14H = 1500;
H2.Ta15H = 64;
H2.Ta16H = 130;
H2.Ta13P = 300;
H2.Ta14P = 640;
H2.Ta15P = 30;
H2.Ta16P = 230;
H2.Ta13E = 1100;
H2.Ta14E = 290;
H2.Ta15E = 140;
H2.Ta16E = 25;
H2.Ta13M = 380;
H2.Ta14M = 640;
H2.Ta15M = 64;
H2.Ta16M = 60;

H3.Ta13A = 140;
H3.Ta14A = 2200;
H3.Ta15A = 4.8;
H3.Ta16A = 63;
H3.Ta13H = 500;
H3.Ta14H = 1500;
H3.Ta15H = 11;
H3.Ta16H = 77;
H3.Ta13P = 100;
H3.Ta14P = 620;
H3.Ta15P = 2.8;
H3.Ta16P = 150;
H3.Ta13E = 2500;
H3.Ta14E = 200;
H3.Ta15E = 32;
H3.Ta16E = 6.9;
H3.Ta13M = 200;
H3.Ta14M = 510;
H3.Ta15M = 5.7;
H3.Ta16M = 22;

H1.Ta23A = 1300;
H1.Ta24A = 10000;
H1.Ta25A = 120;
H1.Ta26A = 76;
H1.Ta23H = 970;
H1.Ta24H = 2200;
H1.Ta25H = 330;
H1.Ta26H = 97;
H1.Ta23P = 450;
H1.Ta24P = 1800;
H1.Ta25P = 350;
H1.Ta26P = 53;
H1.Ta23E = 4200;
H1.Ta24E = 3400;
H1.Ta25E = 650;
H1.Ta26E = 60;
H1.Ta23M = 920;
H1.Ta24M = 3100;
H1.Ta25M = 250;
H1.Ta26M = 67;

H2.Ta23A = 400;
H2.Ta24A = 9999999999;
H2.Ta25A = 49;
H2.Ta26A = 30;
H2.Ta23H = 440;
H2.Ta24H = 1000;
H2.Ta25H = 250;
H2.Ta26H = 47;
H2.Ta23P = 180;
H2.Ta24P = 570;
H2.Ta25P = 120;
H2.Ta26P = 18;
H2.Ta23E = 1600;
H2.Ta24E = 1500;
H2.Ta25E = 270;
H2.Ta26E = 24;
H2.Ta23M = 360;
H2.Ta24M = 1200;
H2.Ta25M = 1100;
H2.Ta26M = 26;

H3.Ta23A = 240;
% H3.Ta24A = NA;
H3.Ta25A = 1.1;
H3.Ta26A = 24;
H3.Ta23H = 330;
H3.Ta24H = 1000;
H3.Ta25H = 40;
H3.Ta26H = 32;
H3.Ta23P = 53;
H3.Ta24P = 580;
H3.Ta25P = 10;
H3.Ta26P = 11;
H3.Ta23E = 3600;
H3.Ta24E = 1000;
H3.Ta25E = 60;
H3.Ta26E = 7.3;
H3.Ta23M = 150;
H3.Ta24M = 1100;
H3.Ta25M = 3.9;
H3.Ta26M = 13;

%% R�ponse fonctionnelle

% phi = @(Y,i,j,a,b) a(i,j-2)*Y(j).^2./(1+(a(i,:).*b(i,:))*(Y(3:6).^2)); %% pH contient deux produits tps*fr�quence : pH(1) au num�rateur -> attaque /  pH(2) au num�rateur -> manipulation
% R = @(i,SR,LS) SR(i)*LS(i)/364;

H1.fcv = @(t,X,Tr, Tc, Ta, lambda) [X(1)*(-1/Tr(1)+(X(3)/Tc(1,3)+X(4)/Tc(1,4)+X(5)/Tc(1,5)+X(6)/Tc(1,6))*min(1, 1/(lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6)))); % equations des predateurs et des proies
    X(2)*(-1/Tr(2)+(X(3)/Tc(2,3)+X(4)/Tc(2,4)+X(5)/Tc(2,5)+X(6)/Tc(2,6))*min(1, 1/(lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6)))); 
    X(3)*((1-kappa3*X(3))/Tr(3) - X(1)/Tc(1,3)*min(1, 1/(lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6))) - X(2)/Tc(2,3)*min(1, 1/(lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6)))) ;
    X(4)*((1-kappa4*X(4))/Tr(4) - X(1)/Tc(1,4)*min(1, 1/(lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6))) - X(2)/Tc(2,4)*min(1, 1/(lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6)))) ;
    X(5)*((1-kappa5*X(5))/Tr(5) - X(1)/Tc(1,5)*min(1, 1/(lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6))) - X(2)/Tc(2,5)*min(1, 1/(lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6)))) ;
    X(6)*((1-kappa6*X(6))/Tr(6) - X(1)/Tc(1,6)*min(1, 1/(lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6))) - X(2)/Tc(2,6)*min(1, 1/(lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6)))) ] ;

H2.fcv = @(t,X,Tr, Tc, Ta, lambda) [X(1)*(-1/Tr(1)+(X(3)/Tc(1,3)+X(4)/Tc(1,4)+X(5)/Tc(1,5)+X(6)/Tc(1,6))/(1+lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6))); % equations des predateurs et des proies
    X(2)*(-1/Tr(2)+(X(3)/Tc(2,3)+X(4)/Tc(2,4)+X(5)/Tc(2,5)+X(6)/Tc(2,6))/(1+lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6))); 
    X(3)*((1-kappa3*X(3))/Tr(3) - X(1)/Tc(1,3)/(1+lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6)) - X(2)/Tc(2,3)/(1+lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6))) ;
    X(4)*((1-kappa4*X(4))/Tr(4) - X(1)/Tc(1,4)/(1+lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6)) - X(2)/Tc(2,4)/(1+lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6))) ;
    X(5)*((1-kappa5*X(5))/Tr(5) - X(1)/Tc(1,5)/(1+lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6)) - X(2)/Tc(2,5)/(1+lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6))) ;
    X(6)*((1-kappa6*X(6))/Tr(6) - X(1)/Tc(1,6)/(1+lambda(1,3)*X(3)+lambda(1,4)*X(4)+lambda(1,5)*X(5)+lambda(1,6)*X(6)) - X(2)/Tc(2,6)/(1+lambda(2,3)*X(3)+lambda(2,4)*X(4)+lambda(2,5)*X(5)+lambda(2,6)*X(6)))] ;

% fcv = @(t,X,a,b,alpha,GM,R1,R2) [alpha(1)*X(1)+R1(SR,LS,GP,t)*X(1)+X(1)*phi(X,1,3,a,b)*GM(1,3)+X(1)*phi(X,1,4,a,b)*GM(1,4)+X(1)*phi(X,1,5,a,b)*GM(1,5)+X(1)*phi(X,1,6,a,b)*GM(1,6); % equations des predateurs et des proies
%     alpha(2)*X(2)+R2(SR,LS,GP,t)*X(2)+X(2)*phi(X,2,3,a,b)*GM(2,3)+X(2)*phi(X,2,4,a,b)*GM(2,4)+X(2)*phi(X,2,5,a,b)*GM(2,5)+X(2)*phi(X,2,6,a,b)*GM(2,6); 
%     alpha(3)*X(3)*(1-X(3)/kappa3)-X(1)*phi(X,1,3,a,b)*GM(3,1)-X(2)*phi(X,2,3,a,b)*GM(3,2);
%     alpha(4)*X(4)*(1-X(4)/kappa4)-X(1)*phi(X,1,4,a,b)*GM(4,1)-X(2)*phi(X,2,4,a,b)*GM(4,2);
%     alpha(5)*X(5)*(1-X(5)/kappa5)-X(1)*phi(X,1,5,a,b)*GM(5,1)-X(2)*phi(X,2,5,a,b)*GM(5,2);
%     alpha(6)*X(6)*(1-X(6)/kappa6)-X(1)*phi(X,1,6,a,b)*GM(6,1)-X(2)*phi(X,2,6,a,b)*GM(6,2)] ;

%% Choix de la fonction de Holling

holl = H2;


%%% Import des fichiers GM et a : 
%%% 1 ligne par saison en commen�ant par Automne
%%% 1 proie par colonne (gros oiseaux, petits oiseaux, micromammif�res, lapins)

GM0 = zeros(6,6);

GM0(3:6,1:2) = 1;

%% Constantes saisonni�res

A.nbJours = 91 ; % nombre de jours d'automne

A.Tr = [A.Tr1, A.Tr2, Tr3, Tr4, Tr5, Tr6];

A.lambda = [zeros(2,2) [holl.lambda13A, holl.lambda14A, holl.lambda15A, holl.lambda16A;
            holl.lambda23A, holl.lambda24A, holl.lambda25A, holl.lambda26A]];

A.Tc = [zeros(2,2) [holl.Tc13A, holl.Tc14A, holl.Tc15A, holl.Tc16A;
        holl.Tc23A, holl.Tc24A, holl.Tc25A, holl.Tc26A]];

A.Ta = [zeros(2,2) [holl.Ta13A, holl.Ta14A, holl.Ta15A, holl.Ta16A;
        holl.Ta23A, holl.Ta24A, holl.Ta25A, holl.Ta26A]];

H.nbJours = 91 ; % nombre de jours d'hiver

H.Tr = [H.Tr1, H.Tr2, Tr3, Tr4, Tr5, Tr6];

H.lambda = [zeros(2,2) [holl.lambda13H, holl.lambda14H, holl.lambda15H, holl.lambda16H;
            holl.lambda23H, holl.lambda24H, holl.lambda25H, holl.lambda26H]];

H.Tc = [zeros(2,2) [holl.Tc13H, holl.Tc14H, holl.Tc15H, holl.Tc16H;
        holl.Tc23H, holl.Tc24H, holl.Tc25H, holl.Tc26H]];

H.Ta = [zeros(2,2) [holl.Ta13H, holl.Ta14H, holl.Ta15H, holl.Ta16H;
        holl.Ta23H, holl.Ta24H, holl.Ta25H, holl.Ta26H]];

P.nbJours = 91 ; % nombre de jours de printemps

P.Tr = [P.Tr1, P.Tr2, Tr3, Tr4, Tr5, Tr6];

P.lambda = [zeros(2,2) [holl.lambda13P, holl.lambda14P, holl.lambda15P, holl.lambda16P;
            holl.lambda23P, holl.lambda24P, holl.lambda25P, holl.lambda26P]];

P.Tc = [zeros(2,2) [holl.Tc13P, holl.Tc14P, holl.Tc15P, holl.Tc16P;
        holl.Tc23P, holl.Tc24P, holl.Tc25P, holl.Tc26P]];

P.Ta = [zeros(2,2) [holl.Ta13P, holl.Ta14P, holl.Ta15P, holl.Ta16P;
        holl.Ta23P, holl.Ta24P, holl.Ta25P, holl.Ta26P]];

E.nbJours = 91 ; % nombre de jours d'�t�

E.Tr = [E.Tr1, E.Tr2, Tr3, Tr4, Tr5, Tr6];

E.lambda = [zeros(2,2) [holl.lambda13E, holl.lambda14E, holl.lambda15E, holl.lambda16E;
            holl.lambda23E, holl.lambda24E, holl.lambda25E, holl.lambda26E]];

E.Tc = [zeros(2,2) [holl.Tc13E, holl.Tc14E, holl.Tc15E, holl.Tc16E;
        holl.Tc23E, holl.Tc24E, holl.Tc25E, holl.Tc26E]];

E.Ta = [zeros(2,2) [holl.Ta13E, holl.Ta14E, holl.Ta15E, holl.Ta16E;
        holl.Ta23E, holl.Ta24E, holl.Ta25E, holl.Ta26E]];

%% R�solution du syst�me diff�rentiel

Pinit = [1;1;1;1;1;1]; % nombre de densit� de r�f�rence
% Pinit = [XR1;XR2;XR3;XR4;XR5;XR6]; % moyennes entre automne 2014 et �t� 2020
% Pinit = [0.0493;0.0343;1.1416;0.0943;0.0183;0.9520]; % populations initiales (en biomasse/hectare) ; automne 2014
%Pinit = [0.0493;0.0343;1.6895;0.0899;0.0848;0.9714]; % automne 2015
%Pinit = [0.0493;0;67.4751;0.0598;0.4843;0.6217]; % automne 2018
%Pinit = [0.0986;0.0686;1.0350;0.0648;2.6222;0.8548]; % automne 2019
%Pinit = [0.0986;0;7.0398;0.1045;1.4267;1.1657]; % automne 2020
% Pinit = [0;0.0352;1.2329;0.0943;4.869;0.952]; % populations initiales (en biomasse/hectare)

nb_annees = 60 ; % nombre d'annees sur lequel le modele va tourner

%Pinit = [0;500;1;1;1;1].*[1;15.52;1.2329;0.0943;4.869;0.952];

Pode = Pinit;

PopD = Pinit; % populations de depart

PinitD = Pinit;

t=0;

Tode = 0;

TD = 0;

for n=0:4*nb_annees-1
    ns = mod(n,4); %% ns=0 : automne, ns=1 : hiver1, ns = 2 : hiver2 ...
switch ns
        case 0
            periode = A;
        case 1
            periode = H;
        case 2
            periode = P;
        case 3
            periode = E;
end

   
    f = @(t,X) (holl.fcv(t, X, periode.Tr, periode.Tc, periode.Ta, periode.lambda));
    
    Pop = ode45(f,[0 periode.nbJours],Pinit); %% y'=f(t,y)
    
    Pode = [Pode Pop.y(:,2:end)];
    
    Tode = [Tode Tode(end)+Pop.x(2:end)];
    
    Pinit = Pode(:,end);
    
   
    PinitD = PopD(:,end); % a quoi sert cela ?
    
end


matriceSim = transpose([Tode;Pode])

writematrix(matriceSim, 'Mod_6comp_sim_HII_bis.csv')

% clf
% 
% debS = [91, 182, 273, 364]
% for n=1:nb_annees-1
%     debS = [debS, debS(end)+[91, 182, 273, 364]];
% end
% 
% figure(1)
% 
% renards=Pode(1,:)/Pode(1,1);
% chats=Pode(2,:)/Pode(2,1);
% 
% gros_oiseaux=Pode(3,:)/Pode(3,1);
% petits_oiseaux=Pode(4,:)/Pode(4,1);
% micromammiferes=Pode(5,:)/Pode(5,1);
% lapins=Pode(6,:)/Pode(6,1);
% 
% renardsD=PopD(1,:)/PopD(1,1);
% chatsD=PopD(2,:)/PopD(2,1);
% 
% gros_oiseauxD=PopD(3,:)/PopD(3,1);
% petits_oiseauxD=PopD(4,:)/PopD(4,1);
% micromammiferesD=PopD(5,:)/PopD(5,1);
% lapinsD=PopD(6,:)/PopD(6,1);
% 
% %Tode=Tode/90;
% 
% figure(1)
% newDefaultColors=lines(6);
% set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren'); % a quoi sert cela ?
% 
% % h1 = plot(Tode/90,[renards;chats;gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
% h1 = plot(Tode,[renards;chats;gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
% xlabel('Number of seasons')
% axis tight
% % for n=1:4*nb_annees
% %     xline(n);
% % end
% for l=debS
%     xline(l)
% end
% xticks(debS(mod(1:nb_annees*4, 10) == 0));
% xticklabels(string((1:floor(nb_annees*4/10))*10))
% title('All populations - autumn 2014 ->');
% 
% legend(h1,{'foxes','cats','large birds','small birds','small mammals','lagomorphs'});
% 
% figure(2)
% subplot(2,1,1)
% newDefaultColors2 = newDefaultColors(1:2,:);
% set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');
% 
% % h2 = plot(Tode/90,[renards;chats],'LineWidth',2);
% h2 = plot(Tode,[renards;chats],'LineWidth',2);
% xlabel('Number of seasons')
% axis tight
% % for n=1:4*nb_annees
% %     xline(n);
% % end
% for l=debS
%     xline(l)
% end
% xticks(debS(mod(1:nb_annees*4, 10) == 0));
% xticklabels(string((1:floor(nb_annees*4/10))*10))
% title('Predators');
% 
% legend(h2,{'foxes','cats'});
% 
% 
% subplot(2,1,2)
% newDefaultColors3 = newDefaultColors(3:6,:);
% set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');
% 
% % h3 = plot(Tode/90, [gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
% h3 = plot(Tode, [gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
% axis tight
% % for n=1:4*nb_annees
% %     xline(n);
% % end
% for l=debS
%     xline(l)
% end
% xticks(debS(mod(1:nb_annees*4, 10) == 0));
% xticklabels(string((1:floor(nb_annees*4/10))*10))
% title('Prey');
% legend(h3,{'large birds','small birds','small mammals','lagomorphs'});
% 
% % subplot(1,2,2)
% % h2 = plot(TD,renardsD,'r',TD,chatsD,'b', TD, gros_oiseauxD,'g',TD, petits_oiseauxD,'k', TD, micromammiferesD,'c', TD, lapinsD,'m');
% % legend('renards','chats','gros oiseaux','petits oiseaux','micromammif�res','lapins')
% % 
% % for n=1:4*nb_annees
% %     xline(90*n);
% % end
% % 
% % legend(h2,{'renards','chats','gros oiseaux','petits oiseaux','micromammif�res','lapins'});
% % 
% 
