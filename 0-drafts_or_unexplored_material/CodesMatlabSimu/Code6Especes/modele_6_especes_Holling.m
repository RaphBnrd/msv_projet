clear
clf

GM_interactions_Fox_Proies = readtable("../Parametres_modele_ChatRenard-Proies/GM_interactions_Fox-Proies.csv");
GM_interactions_Cat_Proies = readtable("../Parametres_modele_ChatRenard-Proies/GM_interactions_Cat-Proies.csv");
Recherche_proie_aFoxj_Holling = readtable("../Parametres_modele_ChatRenard-Proies/Recherche_proie_aFoxj_Holling.csv");
Recherche_proie_aCatj_Holling = readtable("../Parametres_modele_ChatRenard-Proies/Recherche_proie_aCatj_Holling.csv");
GM_ressources_alternatives = readtable("../Parametres_modele_ChatRenard-Proies/GM_ressources_alternatives.csv");
Temps_manip_bij_Holling = readtable("../Parametres_modele_ChatRenard-Proies/Temps_manip_bij_Holling.csv");
GM_ressources_alternatives = readtable("../Parametres_modele_ChatRenard-Proies/GM_ressources_alternatives.csv");
BMR_pred = readtable("../Parametres_modele_ChatRenard-Proies/BMR_pred.csv");
BMR_pred = table2array(BMR_pred(1:2,2));

Pop=[];

dt = 1;

alpha_proies = [0.0035; 0.0042; 0.011; 0.0053];

kappa3 = 39.9; % par hectare ?
kappa4 = 4.19;
kappa5 = 15.9; % en kg/ha
kappa6 = 40.2; % 1 lapin par ha

b = table2array(Temps_manip_bij_Holling(1:4,2:3))';

%%% Import des fichiers GM et a : 
%%% 1 ligne par saison en commençant par Automne
%%% 1 proie par colonne (gros oiseaux, petits oiseaux, micromammifères, lapins)

GM0 = zeros(6,6);

GM0(3:6,1:2) = 1;

A.alpha = [table2array(GM_ressources_alternatives(1,2));table2array(GM_ressources_alternatives(2,2))] - BMR_pred; %% GM_autres-BMR

A.a = [table2array(Recherche_proie_aFoxj_Holling(1,2:5));table2array(Recherche_proie_aCatj_Holling(1,2:5))];

A.GM = GM0;
A.GM(1:2,3:6) = [table2array(GM_interactions_Fox_Proies(1,2:5));table2array(GM_interactions_Cat_Proies(1,2:5))];

W.alpha = [table2array(GM_ressources_alternatives(1,3));table2array(GM_ressources_alternatives(2,3))] - BMR_pred; %% GM_autres-BMR

W.a = [table2array(Recherche_proie_aFoxj_Holling(2,2:5));table2array(Recherche_proie_aCatj_Holling(2,2:5))];

W.GM = GM0;
W.GM(1:2,3:6)= [table2array(GM_interactions_Fox_Proies(2,2:5));table2array(GM_interactions_Cat_Proies(2,2:5))];

Sp.alpha = [table2array(GM_ressources_alternatives(1,4));table2array(GM_ressources_alternatives(2,4))] - BMR_pred; %% GM_autres-BMR

Sp.a = [table2array(Recherche_proie_aFoxj_Holling(3,2:5));table2array(Recherche_proie_aCatj_Holling(3,2:5))];;

Sp.GM = GM0;
Sp.GM(1:2,3:6) = [table2array(GM_interactions_Fox_Proies(3,2:5));table2array(GM_interactions_Cat_Proies(3,2:5))];


Su.alpha = [table2array(GM_ressources_alternatives(1,5));table2array(GM_ressources_alternatives(2,5))] - BMR_pred; %% GM_autres-BMR

Su.a = [table2array(Recherche_proie_aFoxj_Holling(4,2:5));table2array(Recherche_proie_aCatj_Holling(4,2:5))];;

Su.GM = GM0;
Su.GM(1:2,3:6) = [table2array(GM_interactions_Fox_Proies(4,2:5));table2array(GM_interactions_Cat_Proies(4,2:5))];

b

BMR_pred

disp("A.alpha")
A.alpha

disp("A.a")
A.a

disp("A.GM")
A.GM

disp("W.alpha")
W.alpha

disp("W.a")
W.a

disp("W.GM")
W.GM

disp("Sp.alpha")
Sp.alpha

disp("W.a")
Sp.a

disp("W.GM")
Sp.GM

disp("Su.alpha")
Su.alpha

disp("Su.a")
Su.a

disp("Su.GM")
Su.GM



phi = @(Y,i,j,a,b) a(i,j-2)*Y(j).^2./(1+(a(i,:).*b(i,:))*(Y(3:6).^2)); %% pH contient deux produits tps*fréquence : pH(1) au numérateur -> attaque /  pH(2) au numérateur -> manipulation

fcv = @(t,X,a,b,alpha,GM) [alpha(1)*X(1)+X(1)*phi(X,1,3,a,b)*GM(1,3)+X(1)*phi(X,1,4,a,b)*GM(1,4)+X(1)*phi(X,1,5,a,b)*GM(1,5)+X(1)*phi(X,1,6,a,b)*GM(1,6);
   alpha(2)*X(2)+X(2)*phi(X,2,3,a,b)*GM(2,3)+X(2)*phi(X,2,4,a,b)*GM(2,4)+X(2)*phi(X,2,5,a,b)*GM(2,5)+X(2)*phi(X,2,6,a,b)*GM(2,6); 
    alpha(3)*X(3)*(1-X(3)/kappa3)-X(1)*phi(X,1,3,a,b)*GM(3,1)-X(2)*phi(X,2,3,a,b)*GM(3,2);
    alpha(4)*X(4)*(1-X(4)/kappa4)-X(1)*phi(X,1,4,a,b)*GM(4,1)-X(2)*phi(X,2,4,a,b)*GM(4,2);
    alpha(5)*X(5)*(1-X(5)/kappa5)-X(1)*phi(X,1,5,a,b)*GM(5,1)-X(2)*phi(X,2,5,a,b)*GM(5,2);
    alpha(6)*X(6)*(1-X(6)/kappa6)-X(1)*phi(X,1,6,a,b)*GM(6,1)-X(2)*phi(X,2,6,a,b)*GM(6,2)] ;



%Pinit = [0.0343;0.0352;1.2329;0.0943;4.869;0.952];

nb_annees = 20 ;

Pinit = [0;500;1;1;1;1].*[1;15.52;1.2329;0.0943;4.869;0.952];

Pode = Pinit;

PopD = Pinit;

PinitD = Pinit;

t=0;

Tode = 0;

TD = 0;

for n=0:4*nb_annees-1
    ns = mod(n,4); %% ns=0 : automne, ns=1 : hiver ...
    switch ns
        case 3
            periode = A;
        case 0
            periode = W;
        case 1
            periode = Sp;
        case 2
            periode = Su;
    end
    
    f = @(t,X) (fcv(t,X,periode.a,b,[periode.alpha;alpha_proies],periode.GM));
    
    Pop = ode45(f,[0 90],Pinit); %% y'=f(t,y)
    
    Pode = [Pode Pop.y(:,2:end)];
    
    Tode = [Tode Tode(end)+Pop.x(2:end)];
    
    Pinit = Pode(:,end);
    
%     popd = PinitD;
%     
%     for i=1:90
%         
%         popd = popd + dt*f(t,popd);
%         
%         PopD = [PopD popd];
%         
%         TD = [TD TD(end)+1];
%     end
   
    PinitD = PopD(:,end);
    
end

clf
figure(1)

renards=100*Pode(1,:)/Pode(1,1);
chats=100*Pode(2,:)/Pode(2,1);

gros_oiseaux=100*Pode(3,:)/Pode(3,1);
petits_oiseaux=100*Pode(4,:)/Pode(4,1);
micromammiferes=100*Pode(5,:)/Pode(5,1);
lapins=100*Pode(6,:)/Pode(6,1);

renardsD=100*PopD(1,:)/PopD(1,1);
chatsD=100*PopD(2,:)/PopD(2,1);

gros_oiseauxD=100*PopD(3,:)/PopD(3,1);
petits_oiseauxD=100*PopD(4,:)/PopD(4,1);
micromammiferesD=100*PopD(5,:)/PopD(5,1);
lapinsD=100*PopD(6,:)/PopD(6,1);

%Tode=Tode/90;

figure(1)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren');

h1 = plot(Tode/90,[renards;chats;gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
xlabel('Number of seasons')
axis tight
for n=1:4*nb_annees
    xline(n);
end
title('All populations: % of the inital population - autumn 2014 ->');

legend(h1,{'foxes','cats','large birds','small birds','small mammals','lagomorphs'});

figure(2)
subplot(2,1,1)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

h2 = plot(Tode/90,[renards;chats],'LineWidth',2);
xlabel('Number of seasons')
axis tight
for n=1:4*nb_annees
    xline(n);
end
title('Predators');

legend(h2,{'foxes','cats'});


subplot(2,1,2)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h3 = plot(Tode/90, [gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
axis tight
for n=1:4*nb_annees
    xline(n);
end
title('Prey');
legend(h3,{'large birds','small birds','small mammals','lagomorphs'});

% subplot(1,2,2)
% h2 = plot(TD,renardsD,'r',TD,chatsD,'b', TD, gros_oiseauxD,'g',TD, petits_oiseauxD,'k', TD, micromammiferesD,'c', TD, lapinsD,'m');
% legend('renards','chats','gros oiseaux','petits oiseaux','micromammifères','lapins')
% 
% for n=1:4*nb_annees
%     xline(90*n);
% end
% 
% legend(h2,{'renards','chats','gros oiseaux','petits oiseaux','micromammifères','lapins'});
% 
