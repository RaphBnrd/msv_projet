clear
clf
Pop=[];

nb_annees = 15 ;

dt = 1;

alpha3 = 0.0035;
alpha4 = 0.0042;
alpha5 = 0.011;
alpha6 = 0.0053;

kappa1 = 1.685;  % par hectare
kappa2 = 9.905;
kappa3 = 4.119;
kappa4 = 0.269;
kappa5 = 1.179; % en kg/ha
kappa6 = 5.195; % 1 lapin par ha

A.alpha1 = -0.0027;
A.alpha2 = -0.032;

A.F1 = [0.42	0.16	1	0.13];
A.F2 = [0.3	0	1.4	0.37];

A.Bpc1 = [0.061	0.0022	0.0045	0.17];
A.Bpc2 = [0.053	0.0025	0.0051	0.093];

A.B = zeros(6,6);
A.B(1,3:6) = A.F1.*A.Bpc1;
A.B(2,3:6) = A.F2.*A.Bpc2;
A.B(3:6,1) = - A.F1';
A.B(3:6,2) = - A.F2';

W.alpha1 = -0.021;
W.alpha2 = -0.035;

W.F1 = [0.25	0.21	2.6	0.07];
W.F2 = [0.42	0.25	1.6	0.25];

W.Bpc1 = [0.064	0.0021	0.005	0.14];
W.Bpc2 = [0.055	0.22	0.0049	0.088];

W.B = zeros(6,6);
W.B(1,3:6) = W.F1.*W.Bpc1;
W.B(2,3:6) = W.F2.*W.Bpc2;
W.B(3:6,1) = - W.F1';
W.B(3:6,2) = - W.F2';

Sp.alpha1 = -0.034;
Sp.alpha2 = -0.037;

Sp.F1 = [0.3	0.3	3.7	0.05];
Sp.F2 = [0.3	0.22	1.4	0.44];

Sp.Bpc1 = [0.066	0.002	0.0048	0.19];
Sp.Bpc2 = [0.052	0.0025	0.0049	0.1];

Sp.B = zeros(6,6);
Sp.B(1,3:6) = Sp.F1.*Sp.Bpc1;
Sp.B(2,3:6) = Sp.F2.*Sp.Bpc2;
Sp.B(3:6,1) = - Sp.F1';
Sp.B(3:6,2) = - Sp.F2';

Su.alpha1 = -0.004;
Su.alpha2 = -0.032;

Su.F1 = [0.053	0.0024	0.0048	0.13];
Su.F2 = [0.054	0.0024	0.0048	0.082];

Su.Bpc1 = [0.36	0.22	2.1	0.16];
Su.Bpc2 = [0.52	0.081	2.4	0.22;];

Su.B = zeros(6,6);
Su.B(1,3:6) = Su.F1.*Sp.Bpc1;
Su.B(2,3:6) = Su.F2.*Sp.Bpc2;
Su.B(3:6,1) = - Su.F1';
Su.B(3:6,2) = - Su.F2';

paramHolling = 5;

pH1 = [1,3];
pH2 = [5,2];

phi = @(y,pH) pH(1)*y.^2./(1+pH(2)*y.^2); %% pH contient deux produits tps*fréquence : pH(1) au numérateur -> attaque /  pH(2) au numérateur -> manipulation
psi = @(y) y;

fcv = @(t,X,B,alpha1,alpha2) [alpha1*X(1)+(B(1,3)*X(1)*phi(X(3)/kappa3,pH1)+B(1,4)*X(1)*phi(X(4)/kappa4,pH1)+B(1,5)*X(1)*phi(X(5)/kappa5,pH1)+B(1,6)*X(1)*phi(X(6)/kappa6,pH1));
    alpha2*X(2)+(B(2,3)*X(2)*phi(X(3)/kappa3,pH2)+B(2,4)*X(2)*phi(X(4)/kappa4,pH2)+B(2,5)*X(2)*phi(X(5)/kappa5,pH2)+B(2,6)*X(2)*phi(X(6)/kappa6,pH2));
    alpha3*(1-X(3)/kappa3)*X(3)+B(3,1)*psi(X(1)/kappa1)*X(3)+B(3,2)*psi(X(2)/kappa2)*X(3);
    alpha4*(1-X(4)/kappa4)*X(4)+B(4,1)*psi(X(1)/kappa1)*X(4)+B(4,2)*psi(X(2)/kappa2)*X(4);
    alpha5*(1-X(5)/kappa5)*X(5)+B(5,1)*psi(X(1)/kappa1)*X(5)+B(5,2)*psi(X(2)/kappa2)*X(5);
    alpha6*(1-X(6)/kappa6)*X(6)+B(6,1)*psi(X(1)/kappa1)*X(6)+B(6,2)*psi(X(2)/kappa2)*X(6);] ;


Pinit = [0.0343;0.0352;1.2329;0.0943;4.869;0.952];

Pode = Pinit;

PopD = Pinit;

PinitD = Pinit;

t=0;

Tode = 0;

TD = 0;

for n=0:4*nb_annees-1
    ns = mod(n,4); %% ns=0 : automne, ns=1 : hiver ...
    switch ns
        case 0
            periode = A;
        case 1
            periode = W;
        case 2
            periode = Sp;
        case 3
            periode = Su;
    end
    
    f = @(t,X) (fcv(t,X,periode.B,periode.alpha1,periode.alpha2));
       
    Pop = ode45(f,[0 90],Pinit);
    
    Pode = [Pode Pop.y(:,2:end)];
    
    Tode = [Tode Tode(end)+Pop.x(2:end)];
    
    Pinit = Pode(:,end);
    
    popd = PinitD;
    
    for i=1:90
        
        popd = popd + dt*f(t,popd);
        
        PopD = [PopD popd];
        
        TD = [TD TD(end)+1];
    end
   
    PinitD = PopD(:,end);
    
end

clf
figure(1)

renards=100*Pode(1,:)/Pode(1,1);
chats=100*Pode(2,:)/Pode(2,1);

gros_oiseaux=100*Pode(3,:)/Pode(3,1);
petits_oiseaux=100*Pode(4,:)/Pode(4,1);
micromammiferes=100*Pode(5,:)/Pode(5,1);
lapins=100*Pode(6,:)/Pode(6,1);

renardsD=100*PopD(1,:)/PopD(1,1);
chatsD=100*PopD(2,:)/PopD(2,1);

gros_oiseauxD=100*PopD(3,:)/PopD(3,1);
petits_oiseauxD=100*PopD(4,:)/PopD(4,1);
micromammiferesD=100*PopD(5,:)/PopD(5,1);
lapinsD=100*PopD(6,:)/PopD(6,1);

%Tode=Tode/90;

figure(1)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren');

h1 = plot(Tode/90,[renards;chats;gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
xlabel('Number of seasons')
axis tight
for n=1:4*nb_annees
    xline(n);
end
title('All populations: % of the inital population - autumn 2014 ->');

legend(h1,{'foxes','cats','large birds','small birds','small mammals','lagomorphs'});

figure(2)
subplot(2,1,1)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

h2 = plot(Tode/90,[renards;chats],'LineWidth',2);
xlabel('Number of seasons')
axis tight
for n=1:4*nb_annees
    xline(n);
end
title('Predators');

legend(h2,{'foxes','cats'});


subplot(2,1,2)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h3 = plot(Tode/90, [gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
axis tight
for n=1:4*nb_annees
    xline(n);
end
title('Prey');
legend(h3,{'large birds','small birds','small mammals','lagomorphs'});

% subplot(1,2,2)
% h2 = plot(TD,renardsD,'r',TD,chatsD,'b', TD, gros_oiseauxD,'g',TD, petits_oiseauxD,'k', TD, micromammiferesD,'c', TD, lapinsD,'m');
% legend('renards','chats','gros oiseaux','petits oiseaux','micromammifères','lapins')
% 
% for n=1:4*nb_annees
%     xline(90*n);
% end
% 
% legend(h2,{'renards','chats','gros oiseaux','petits oiseaux','micromammifères','lapins'});
% 
