clear
clf

cd ~/Dropbox/PRIMER/

GM_interactions_Fox_Proies = readtable("Parametres_modele_ChatRenard-Proies/GM_interactions_Fox-Proies.csv");
GM_interactions_Cat_Proies = readtable("Parametres_modele_ChatRenard-Proies/GM_interactions_Cat-Proies.csv");
Recherche_proie_aFoxj_Holling = readtable("Parametres_modele_ChatRenard-Proies/Recherche_proie_aFoxj_Holling_Fox6.5kg.csv");
Recherche_proie_aCatj_Holling = readtable("Parametres_modele_ChatRenard-Proies/Recherche_proie_aCatj_Holling.csv");
GM_ressources_alternatives = readtable("Parametres_modele_ChatRenard-Proies/GM_ressources_alternatives_Fox6.5kg.csv");
Temps_manip_bij_Holling = readtable("Parametres_modele_ChatRenard-Proies/Temps_manip_bij_Holling.csv");
BMR_pred = readtable("Parametres_modele_ChatRenard-Proies/BMR_pred_Fox6.5kg.csv");
BMR_pred = table2array(BMR_pred(1:2,2));

% GM_interactions_Fox_Proies = readtable("Donnees/Donnees_pour_le_modele/GM_interactions_Fox-Proies.csv");
% GM_interactions_Cat_Proies = readtable("Donnees/Donnees_pour_le_modele/GM_interactions_Cat-Proies.csv");
% %Recherche_proie_aFoxj_Holling = readtable("Donnees/Donnees_pour_le_modele/Recherche_proie_aFoxj_Holling.csv");
% Recherche_proie_aFoxj_Holling = readtable("Donnees/Donnees_pour_le_modele/Recherche_proie_aFoxj_Holling_Fox5.96892kg.csv");
% Recherche_proie_aCatj_Holling = readtable("Donnees/Donnees_pour_le_modele/Recherche_proie_aCatj_Holling.csv");
% %GM_ressources_alternatives = readtable("Donnees/Donnees_pour_le_modele/GM_ressources_alternatives.csv");
% GM_ressources_alternatives = readtable("Donnees/Donnees_pour_le_modele/GM_ressources_alternatives_Fox5.96892kg.csv");
% Temps_manip_bij_Holling = readtable("Donnees/Donnees_pour_le_modele/Temps_manip_bij_Holling.csv");
% %BMR_pred = readtable("Donnees/Donnees_pour_le_modele/BMR_pred.csv");
% BMR_pred = readtable("Donnees/Donnees_pour_le_modele/BMR_pred_Fox5.96892kg.csv");
% BMR_pred = table2array(BMR_pred(1:2,2));

Pop=[];

dt = 1; % pas de temps = 1 jour

alpha_proies = [0.0035; 0.0042; 0.011; 0.0053]; % taux de croissance intrinseque per capita de chaque groupe de proies

kappa3 = 39.9; % par hectare ?
kappa4 = 4.19;
kappa5 = 15.9; % en kg/ha
kappa6 = 40.2; % 1 lapin par ha

SR = [0.5; 0.5]; % sexe-ratio des renards et des chats
LS = [3.5; 3.88]; % taille moyenne de portee (litter size) des renards et des chats
GT = [235; 364]; % duree de croissance des jeunes entre la fecondation et l'age adulte

b = table2array(Temps_manip_bij_Holling(1:4,2:3))'; % suppression de la premiere colonne qui est inutile

%%% Import des fichiers GM et a : 
%%% 1 ligne par saison en commençant par Automne
%%% 1 proie par colonne (gros oiseaux, petits oiseaux, micromammifères, lapins)

GM0 = zeros(6,6);

GM0(3:6,1:2) = 1;

A.nbJours = 91 ; % nombre de jours d'automne

A.alpha = [table2array(GM_ressources_alternatives(1,2));table2array(GM_ressources_alternatives(2,2))] - BMR_pred; %% GM_autres-BMR

A.R1 = @(SR,LS,GT,t) 0;
%A.R2 = @(SR,LS,GT,t) SR(2)*LS(2)/GT(2); % 1 seule reproduction dans l'annee pour le chat (la premiere)
A.R2 = @(SR,LS,GT,t) 2*SR(2)*LS(2)/GT(2); % 2 reproductions par an pour le chat

A.a = [table2array(Recherche_proie_aFoxj_Holling(1,2:5));table2array(Recherche_proie_aCatj_Holling(1,2:5))];

A.GM = GM0;
A.GM(1:2,3:6) = [table2array(GM_interactions_Fox_Proies(1,2:5));table2array(GM_interactions_Cat_Proies(1,2:5))];

W.nbJours = 91 ; % nombre de jours d'hiver

W.alpha = [table2array(GM_ressources_alternatives(1,3));table2array(GM_ressources_alternatives(2,3))] - BMR_pred; %% GM_autres-BMR

W.R1 = @(SR,LS,GT,t) (t<=38)*0 + (t>38)*SR(1)*LS(1)/GT(1) ;
%W.R2 = @(SR,LS,GT,t) (t<=28)*SR(2)*LS(2)/GT(2) + (t>28)*SR(2)*LS(2)/GT(2) ; % 1 seule reproduction dans l'annee pour le chat (la premiere)
W.R2 = @(SR,LS,GT,t) (t<=28)*SR(2)*LS(2)/GT(2) + (t>28)*SR(2)*LS(2)/GT(2) + SR(2)*LS(2)/GT(2) ; % 2 reproductions par an pour le chat

W.a = [table2array(Recherche_proie_aFoxj_Holling(2,2:5));table2array(Recherche_proie_aCatj_Holling(2,2:5))];

W.GM = GM0;
W.GM(1:2,3:6)= [table2array(GM_interactions_Fox_Proies(2,2:5));table2array(GM_interactions_Cat_Proies(2,2:5))];

Sp.nbJours = 91 ; % nombre de jours d'automne
%Sp.nbJours = 90 ; % nombre de jours d'automne

Sp.alpha = [table2array(GM_ressources_alternatives(1,4));table2array(GM_ressources_alternatives(2,4))] - BMR_pred; %% GM_autres-BMR

Sp.R1 = @(SR,LS,GT,t) SR(1)*LS(1)/GT(1) ;
%Sp.R2 = @(SR,LS,GT,t) SR(2)*LS(2)/GT(2) ; % 1 seule reproduction pour le chat
Sp.R2 = @(SR,LS,GT,t) SR(2)*LS(2)/GT(2) + (t<=28)*SR(2)*LS(2)/GT(2) + (t>28)*SR(2)*LS(2)/GT(2) ; % 2 reproductions pour le chat

Sp.a = [table2array(Recherche_proie_aFoxj_Holling(3,2:5));table2array(Recherche_proie_aCatj_Holling(3,2:5))];;

Sp.GM = GM0;
Sp.GM(1:2,3:6) = [table2array(GM_interactions_Fox_Proies(3,2:5));table2array(GM_interactions_Cat_Proies(3,2:5))];


Su.nbJours = 91 ; % nombre de jours d'ete
%Su.nbJours = 90;

Su.alpha = [table2array(GM_ressources_alternatives(1,5));table2array(GM_ressources_alternatives(2,5))] - BMR_pred; %% GM_autres-BMR

Su.R1 = @(SR,LS,GT,t) SR(1)*LS(1)/GT(1) ;
%Su.R2 = @(SR,LS,GT,t) SR(2)*LS(2)/GT(2) ; % 1 seule reproduction pour le chat
Su.R2 = @(SR,LS,GT,t) 2*SR(2)*LS(2)/GT(2) ; % 2 reproduction pour le chat

Su.a = [table2array(Recherche_proie_aFoxj_Holling(4,2:5));table2array(Recherche_proie_aCatj_Holling(4,2:5))*1];

Su.GM = GM0;
Su.GM(1:2,3:6) = [table2array(GM_interactions_Fox_Proies(4,2:5));table2array(GM_interactions_Cat_Proies(4,2:5))];


b

BMR_pred

disp("A.alpha")
A.alpha

disp("A.a")
A.a

disp("A.GM")
A.GM

disp("W.alpha")
W.alpha

disp("W.a")
W.a

disp("W.GM")
W.GM


disp("Sp.alpha")
Sp.alpha

disp("W.a")
Sp.a

disp("W.GM")
Sp.GM

disp("Su.alpha")
Su.alpha

disp("Su.a")
Su.a

disp("Su.GM")
Su.GM


phi = @(Y,i,j,a,b) a(i,j-2)*Y(j).^2./(1+(a(i,:).*b(i,:))*(Y(3:6).^2)); %% pH contient deux produits tps*fréquence : pH(1) au numérateur -> attaque /  pH(2) au numérateur -> manipulation
% R = @(i,SR,LS) SR(i)*LS(i)/364;


fcv = @(t,X,a,b,alpha,GM,R1,R2) [alpha(1)*X(1)+R1(SR,LS,GT,t)*X(1)+X(1)*phi(X,1,3,a,b)*GM(1,3)+X(1)*phi(X,1,4,a,b)*GM(1,4)+X(1)*phi(X,1,5,a,b)*GM(1,5)+X(1)*phi(X,1,6,a,b)*GM(1,6); % equations des predateurs et des proies
    alpha(2)*X(2)+R2(SR,LS,GT,t)*X(2)+X(2)*phi(X,2,3,a,b)*GM(2,3)+X(2)*phi(X,2,4,a,b)*GM(2,4)+X(2)*phi(X,2,5,a,b)*GM(2,5)+X(2)*phi(X,2,6,a,b)*GM(2,6); 
    alpha(3)*X(3)*(1-X(3)/kappa3)-X(1)*phi(X,1,3,a,b)*GM(3,1)-X(2)*phi(X,2,3,a,b)*GM(3,2);
    alpha(4)*X(4)*(1-X(4)/kappa4)-X(1)*phi(X,1,4,a,b)*GM(4,1)-X(2)*phi(X,2,4,a,b)*GM(4,2);
    alpha(5)*X(5)*(1-X(5)/kappa5)-X(1)*phi(X,1,5,a,b)*GM(5,1)-X(2)*phi(X,2,5,a,b)*GM(5,2);
    alpha(6)*X(6)*(1-X(6)/kappa6)-X(1)*phi(X,1,6,a,b)*GM(6,1)-X(2)*phi(X,2,6,a,b)*GM(6,2)] ;

Pinit = [0.0343;0.0352;1.2329;0.0943;4.869;0.952]; % populations initiales (en biomasse/hectare)
% Pinit = [0;0.0352;1.2329;0.0943;4.869;0.952]; % populations initiales (en biomasse/hectare)

nb_annees = 20 ; % nombre d'annees sur lequel le modele va tourner

%Pinit = [0;500;1;1;1;1].*[1;15.52;1.2329;0.0943;4.869;0.952];

Pode = Pinit;

PopD = Pinit; % populations de depart

PinitD = Pinit;

t=0;

Tode = 0;

TD = 0;

for n=0:4*nb_annees-1
    ns = mod(n,4); %% ns=0 : automne, ns=1 : hiver1, ns = 2 : hiver2 ...
switch ns
        case 0
            periode = A;
        case 1
            periode = W;
        case 2
            periode = Sp;
        case 3
            periode = Su;
end
    
     f = @(t,X) (fcv(t,X,periode.a,b,[periode.alpha;alpha_proies],periode.GM, periode.R1, periode.R2));
    
    Pop = ode45(f,[0 periode.nbJours],Pinit); %% y'=f(t,y)
    
    Pode = [Pode Pop.y(:,2:end)];
    
    Tode = [Tode Tode(end)+Pop.x(2:end)];
    
    Pinit = Pode(:,end);
    
   
    PinitD = PopD(:,end); % a quoi sert cela ?
    
end

clf

debS = [91, 182, 273, 364]
for n=1:nb_annees-1
    debS = [debS, debS(end)+[91, 182, 273, 364]];
end

figure(1)

renards=100*Pode(1,:)/Pode(1,1);
chats=100*Pode(2,:)/Pode(2,1);

gros_oiseaux=100*Pode(3,:)/Pode(3,1);
petits_oiseaux=100*Pode(4,:)/Pode(4,1);
micromammiferes=100*Pode(5,:)/Pode(5,1);
lapins=100*Pode(6,:)/Pode(6,1);

renardsD=100*PopD(1,:)/PopD(1,1);
chatsD=100*PopD(2,:)/PopD(2,1);

gros_oiseauxD=100*PopD(3,:)/PopD(3,1);
petits_oiseauxD=100*PopD(4,:)/PopD(4,1);
micromammiferesD=100*PopD(5,:)/PopD(5,1);
lapinsD=100*PopD(6,:)/PopD(6,1);

%Tode=Tode/90;

figure(1)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren'); % a quoi sert cela ?

% h1 = plot(Tode/90,[renards;chats;gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
h1 = plot(Tode,[renards;chats;gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
xlabel('Number of seasons')
axis tight
% for n=1:4*nb_annees
%     xline(n);
% end
for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('All populations: % of the inital population - autumn 2014 ->');

legend(h1,{'foxes','cats','large birds','small birds','small mammals','lagomorphs'});

figure(2)
subplot(2,1,1)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

% h2 = plot(Tode/90,[renards;chats],'LineWidth',2);
h2 = plot(Tode,[renards;chats],'LineWidth',2);
xlabel('Number of seasons')
axis tight
% for n=1:4*nb_annees
%     xline(n);
% end
for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Predators');

legend(h2,{'foxes','cats'});


subplot(2,1,2)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

% h3 = plot(Tode/90, [gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
h3 = plot(Tode, [gros_oiseaux;petits_oiseaux;micromammiferes;lapins],'LineWidth',2);
axis tight
% for n=1:4*nb_annees
%     xline(n);
% end
for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Prey');
legend(h3,{'large birds','small birds','small mammals','lagomorphs'});

% subplot(1,2,2)
% h2 = plot(TD,renardsD,'r',TD,chatsD,'b', TD, gros_oiseauxD,'g',TD, petits_oiseauxD,'k', TD, micromammiferesD,'c', TD, lapinsD,'m');
% legend('renards','chats','gros oiseaux','petits oiseaux','micromammifères','lapins')
% 
% for n=1:4*nb_annees
%     xline(90*n);
% end
% 
% legend(h2,{'renards','chats','gros oiseaux','petits oiseaux','micromammifères','lapins'});
% 
