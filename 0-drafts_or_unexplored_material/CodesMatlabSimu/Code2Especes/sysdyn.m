clear

dt=1

K2=2

alpha1=-1
alpha2=1

B(1,2)=0.00001
B(2,1)=-1

Nmax=40



eq=[alpha2*(B(1,2)+alpha1/K2)/alpha1/B(2,1);-alpha1/B(1,2)]+[0.1;-0.2]

P=eq
Pop=P

phi={@(Q) alpha1*Q(1)+B(1,2)*Q(1)*Q(2); @(Q) alpha2*(1-Q(2)/K2)+B(2,1)*Q(1)*Q(2)};
dphi={@(Q) [alpha1+B(1,2)*Q(2) B(1,2)*Q(1)];@(Q) [B(2,1)*Q(2) -alpha2/K2+B(2,1)*Q(1)]};

A=[dphi{1}(eq);dphi{2}(eq)];
eigs(A)

for n=1:Nmax
    Q=P;
    P(1)=Q(1)+dt*phi{1}(Q);
    P(2)=Q(2)+dt*phi{2}(Q);
    Pop=[Pop P];
end

    plot(1:41,Pop)
    
    