clear
clf

cd ~/Dropbox/PRIMER/

X=[];

mu=0.6; %% alpha*Tc
nu=0.004; %% alpha*Ta
%kappa=0.22999970305;
kappa=0.05;

Tref=1;
Tc=0.2;
Ta=0.1;

h = @(X,Tref,Tc,Ta,mu,nu) ([(Tref/Tc)*(-mu+min(1,X(2))).*X(1);
                            (Tref/Ta)*(nu*(1-X(2)*kappa)-X(1)./(max(1,X(2)))).*X(2)]);

Xinit = [1;1];

nb_Tref = 10000; % nombre d'annees sur lequel le modele va tourner
Xode = Xinit;

t=0;
Xode = 0;
XD = 0;

f = @(t,X) (h(X,Tref,Tc,Ta,mu,nu));

Xres = ode45(f,[0 nb_Tref],Xinit); %% y'=f(t,y)
    
Xode = Xres.y; % on ajoute à Pode les nouvelles valeurs de y (nos populations) 
    
Tode=Xres.x; % les nouvelles valeurs de t

display(nb_Tref)

clf

figure(1)

predateur=100*Xode(1,:)/Xode(1,1);
proie=100*Xode(2,:)/Xode(2,1);

%Tode=Tode/90;

figure(1)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren'); % a quoi sert cela ?

h1 = plot(Tode,[predateur;proie],'LineWidth',1);
xlabel('Number of seasons')
axis tight


%xticks(mod(1:nb_annees*360, 30) == 0);
%xticklabels(string((1:floor(nb_annees*365))*30))
title('All populations: % of the initial population');

legend(h1,{'predateur','proie'});

figure(2)
subplot(2,1,1)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

h2 = plot(Tode,predateur,'LineWidth',1);
xlabel('Number of seasons')
axis tight

%xticks(debS(mod(1:nb_annees*4, 10) == 0));
%xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Predators');

legend(h2,{'predateur'});


subplot(2,1,2)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h3 = plot(Tode, proie,'LineWidth',2);
axis tight


%xticks(debS(mod(1:nb_annees*4, 10) == 0));
%xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Prey');
legend(h3,{'proie'});

