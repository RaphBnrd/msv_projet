clear all

Pop=[];

Tmax = 10000 ; 

dt = 1;

alpha1n = 2.5/365 %% 2.5 femelles juveniles par an
alpha1m = 1/17 %% 17 jours si aucune interaction - perte de 171 g/j

alpha1 = alpha1n-alpha1m

alpha3 = 0.005

kappa3 = 215;

T13 = 1;
T31 = 1; %% 1 lapin par jour

B(1,3) = (0.850/5) * (1/kappa3)*(1/T13);
B(3,1) = - (1/T31) / 5 ;
 
f = @(t,X) [alpha1*X(1)+B(1,3)*X(1)*X(3)/(1+X(3)/kappa3);0;alpha3*(1-X(3)/kappa3)*X(3)+B(3,1)*X(1)*X(3)] ;

Xinit = [0.094;0;4]; 

X = Xinit;

Pop=X;

t=0;

T=t;

while t<Tmax
    dt=min(dt,Tmax-t)
    
    %X=X+dt*f(t,X)
    
    t=t+dt
    
    %Pop= [Pop X];
    
    T=[T t];
end

Pop = ode45(f,T,Xinit)

clf
figure(1)

plot(Pop.x, Pop.y(1,:),'r')

figure(2)

plot(Pop.x, Pop.y(3,:),'b')

figure(3)

plot(Pop.y(1,:),Pop.y(3,:))
