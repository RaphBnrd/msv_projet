clear
clf
a=0.01
b=0.0008
c=0.0009
d=0.0002
% a=0.5
% b=0
% c=0.5
% d=0

r0=2
p0=1
r=r0
p=p0
R=r
P=p

s=5

dt=1/s

N=1:10*s

for n=N
    r=(1-a*dt)*r+dt*b*r*p;
    p=(1+dt*c)*p-dt*d*r*p;
    R=[R r];
    P=[P p];
end
figure(1)
clf
plot([0 dt*N],R,'b',[0 dt*N],P,'r','linewidth',2)
xlabel('années')

hold on

r=r0
p=p0
R=r
P=p
N=1:3000*s

for n=N
    r=(1-a)*r+b*r*p;
    p=(1+c)*p-d*r*p;
    R=[R r];
    P=[P p];
end
figure(2)
plot([0 dt*N],R,'b',[0 dt*N],P,'r','linewidth',2)
legend('prédateurs','proies')
xlabel('n')

r=r0
p=p0
R=r
P=p
N=1:12000*s

figure(3)
for n=N
    r=(1-a)*r+b*r*p;
    p=(1+c)*p-d*r*p;
    R=[R r];
    P=[P p];
end

plot([0 dt*N],R,'b',[0 dt*N],P,'r','linewidth',2)
legend('prédateurs','proies')
xlabel('n')