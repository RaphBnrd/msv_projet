clear
clf

cd ~/Dropbox/PRIMER/

Pop=[];

alpha = [-0.5;1]; % taux de croissance intrinseque per capita de chaque groupe de proies

a = 1;

EM = 1;

GM = 1;

dfc = EM;

phi = @(X,a) a*X;
tebm = @(X, a, EM) phi(X,a)*EM;
rebm = @(X, a, EM, dfc) (tebm(X, a, EM) <= dfc)*tebm(X, a, EM) + (tebm(X, a, EM) > dfc)*dfc;
pokbm = @(X, a, EM, dfc) (tebm(X, a, EM) > dfc)*(tebm(X, a, EM) - dfc)/dfc;
rkbm = @(X, Y, a, EM, dfc) Y*phi(X,a)*EM/(1+pokbm(X, a, EM, dfc));

fcv = @(t, X, alpha, a, EM, GM, dfc) [X(1)*alpha(1) + X(1)*rebm(X(2), a, EM, dfc)*GM; % equations des predateurs et des proies
    X(2)*alpha(2)-rkbm(X(2), X(1), a, GM, dfc)];

fcvfi = @(t, X, alpha, a, EM, GM, dfc) [X(1)*alpha(1) + X(1)*rebm(X(2), a, EM, dfc)*GM; % equations des predateurs et des proies
    X(2)*alpha(2)*(1-0.01*X(2))-rkbm(X(2), X(1), a, GM, dfc)];

fcvlv = @(t, X, alpha, a, EM, GM, dfc) [X(1)*alpha(1) + X(1)*X(2)*GM*EM; % equations des predateurs et des proies
    X(2)*alpha(2)-X(2)*X(1)*GM];


Pinit = [1;1];
nb_annees = 1; % nombre d'annees sur lequel le modele va tourner
Pode = Pinit;

t=0;
Tode = 0;
TD = 0;

f = @(t,X) (fcvfi(t, X, alpha, a, EM, GM, dfc));

Pop = ode45(f,[0 360*nb_annees],Pinit); %% y'=f(t,y)
    
Pode = Pop.y; % on ajoute à Pode les nouvelles valeurs de y (nos populations) 
    
Tode =Pop.x; % les nouvelles valeurs de t

display(nb_annees)

clf

figure(1)

predateur=100*Pode(1,:)/Pode(1,1);
proie=100*Pode(2,:)/Pode(2,1);

%Tode=Tode/90;

figure(1)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren'); % a quoi sert cela ?

h1 = plot(Tode,[predateur;proie],'LineWidth',1);
xlabel('Number of seasons')
axis tight


%xticks(mod(1:nb_annees*360, 30) == 0);
%xticklabels(string((1:floor(nb_annees*365))*30))
title('All populations: % of the initial population');

legend(h1,{'predateur','proie'});

figure(2)
subplot(2,1,1)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

h2 = plot(Tode,predateur,'LineWidth',1);
xlabel('Number of seasons')
axis tight

%xticks(debS(mod(1:nb_annees*4, 10) == 0));
%xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Predators');

legend(h2,{'predateur'});


subplot(2,1,2)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h3 = plot(Tode, proie,'LineWidth',2);
axis tight


%xticks(debS(mod(1:nb_annees*4, 10) == 0));
%xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Prey');
legend(h3,{'proie'});

