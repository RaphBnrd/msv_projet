clear
clf

cd ~/Dropbox/PRIMER/



Pop=[];

alpha = [-0.5;1]; % taux de croissance intrinseque per capita de chaque groupe de proies

a = 1;

EM = 1;

GM = 1;

dfc = 1;


phi = @(X,a) a*X;
tebm = @(X, a, EM) phi(X,a)*EM;
rebm = @(X, a, EM, dfc) (tebm(X, a, EM) <= dfc)*tebm(X, a, EM) + (tebm(X, a, EM) > dfc)*dfc;
pokbm = @(X, a, EM, dfc) (tebm(X, a, GM) > dfc)*(tebm(X, a, GM) - dfc)/dfc;
rkbm = @(X, Y, a, EM, dfc) Y*phi(X,a)*EM/(1+pokbm(X, a, EM, dfc));

fcv = @(t, X, alpha, a, EM, GM, dfc) [X(1)*alpha(1) + X(1)*rebm(X(2), a, EM, dfc)*GM; % equations des predateurs et des proies
    X(2)*alpha(2)-rkbm(X(2), X(1), a, GM, dfc)];


Pinit = [1;1];
nb_annees = 1 ; % nombre d'annees sur lequel le modele va tourner
Pode = Pinit;


t=0;
Tode = 0;
TD = 0;

for n=0:4*nb_annees-1

     f = @(t,X) (fcv(t, X, alpha, a, EM, GM, dfc));
    
    Pop = ode45(f,[0 91],Pinit); %% y'=f(t,y)
    
    Pode = [Pode Pop.y(:,2:end)]; % on ajoute à Pode les nouvelles valeurs de y (nos populations) 
    
    Tode = [Tode Tode(end)+Pop.x(2:end)]; % les nouvelles valeurs de t
    
    Pinit = Pode(:,end); % on assigne la dernière valeur de y (les dernières valeurs connues de nos populations) à Pinit : c'est le point d'où commencera la prochaine série de calculs
    
        
end

TEBM = tebm(Pode(2,:), a, EM);

REBM = []
for i=Pode(2,:)
     res = rebm(i, a, EM, dfc);
     REBM = [REBM res]
end

POKBM = []
for i=1:length(Pode(1,:))
     res = pokbm(Pode(2,i), a, EM, dfc);
     POKBM = [POKBM res]
end

RKBM = []
for i=1:length(Pode(1,:))
     res = rkbm(Pode(2,i), Pode(1,i), a, EM, dfc);
     RKBM = [RKBM res]
end

clf

debS = [91, 182, 273, 364]
for n=1:nb_annees-1
    debS = [debS, debS(end)+[91, 182, 273, 364]];
end

figure(1)

predateur=100*Pode(1,:)/Pode(1,1);
proie=100*Pode(2,:)/Pode(2,1);


%Tode=Tode/90;

figure(1)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren'); % a quoi sert cela ?

h1 = plot(Tode,[predateur;proie],'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('All populations: % of the inital population');

legend(h1,{'predateur','proie'});

figure(2)
subplot(2,1,1)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

h2 = plot(Tode,predateur,'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Predators');

legend(h2,{'predateur'});


subplot(2,1,2)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h3 = plot(Tode, proie,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Prey');
legend(h3,{'proie'});

figure(3)

subplot(4,1,1)
newDefaultColors3 = newDefaultColors(1:4,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h4 = plot(Tode,TEBM,'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Theorical eaten biomass');

legend(h4,{'Theorical eaten biomass'});


subplot(4,1,2)
newDefaultColors4 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors4, 'NextPlot', 'replacechildren');

h5 = plot(Tode, REBM,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Theorical eaten biomass');
legend(h5,{'Theorical eaten biomass'});

subplot(4,1,3)
newDefaultColors5 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors5, 'NextPlot', 'replacechildren');

h6 = plot(Tode, POKBM,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Proportion of overkilled biomass');
legend(h6,{'Proportion of overkilled biomass'});

subplot(4,1,4)
newDefaultColors6 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors6, 'NextPlot', 'replacechildren');

h7 = plot(Tode, REBM,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Real killed biomass');
legend(h7,{'Real killed biomass'});
title('Real killed biomass');