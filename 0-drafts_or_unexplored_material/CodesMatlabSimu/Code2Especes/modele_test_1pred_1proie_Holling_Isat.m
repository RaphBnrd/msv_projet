clear
clf

cd ~/Dropbox/PRIMER/



Pop=[];

alpha = [-0.5;1]; % taux de croissance intrinseque per capita de chaque groupe de proies

a = 1; % taux d'attaque

EM = 1; % proportion de biomasse mangée sur un item de proie

GM = 1; % conversion de 1kg de biomasse de proie mangée en 1kg de prédateur

dfc = 1; % daily food consumption du prédateur = quantité max de biomasse (en kg) qu'il peut ingérer par jour et par kg


phi = @(X,a) a*X; % reponse fonctionnelle Holling I sans saturation
tebm = @(X, a, EM) phi(X,a)*EM; % biomasse mangee theorique sans saturation
rebm = @(X, a, EM, dfc) (tebm(X, a, EM) <= dfc)*tebm(X, a, EM) + (tebm(X, a, EM) > dfc)*dfc;
pokbm = @(X, a, EM, dfc) (tebm(X, a, EM) > dfc)*(tebm(X, a, EM) - dfc)/dfc;
rkbm = @(X, Y, a, EM, dfc) Y*phi(X,a)/(1+pokbm(X, a, EM, dfc));

fcvlv = @(t, X, alpha, a, EM, GM, dfc) [X(1)*alpha(1) + X(1)*X(2)*GM*EM; % equations des predateurs et des proies Lotka-Volterra classique
    X(2)*alpha(2)-X(2)*X(1)];

fcv = @(t, X, alpha, a, EM, GM, dfc) [X(1)*alpha(1) + X(1)*rebm(X(2), a, EM, dfc)*GM; % equations des predateurs et des proies Lotka-Volterra avec Holling I saturee
    X(2)*alpha(2)-rkbm(X(2), X(1), a, GM, dfc)];




Pinit = [1;1]; % populations initiales
nb_annees = 10 ; % nombre d'annees sur lequel le modele va tourner
Pode = Pinit;


t=0;
Tode = 0;
f = @(t,X) (fcv(t, X, alpha, a, EM, GM, dfc));



     
    
    Pop = ode45(f,[0 364*nb_annees],Pinit); %% y'=f(t,y)
    
    Pode = [Pode Pop.y(:,2:end)]; % on ajoute à Pode les nouvelles valeurs de y (nos populations) 
    
    Tode = [Tode Tode(end)+Pop.x(2:end)]; % les nouvelles valeurs de t
    
    Pinit = Pode(:,end); % on assigne la dernière valeur de y (les dernières valeurs connues de nos populations) à Pinit : c'est le point d'où commencera la prochaine série de calculs
    
        


TEBM = tebm(Pode(2,:), a, EM);

REBM = []
for i=Pode(2,:)
     res = rebm(i, a, EM, dfc);
     REBM = [REBM res]
end

POKBM = []
for i=1:length(Pode(1,:))
     res = pokbm(Pode(2,i), a, EM, dfc);
     POKBM = [POKBM res]
end

RKBM = []
for i=1:length(Pode(1,:))
     res = rkbm(Pode(2,i), Pode(1,i), a, EM, dfc);
     RKBM = [RKBM res]
end




Pinitlv = [1;1]; % populations initiales
Podelv = Pinit;


t=0;
Todelv = 0;

flv = @(t,X) (fcvlv(t, X, alpha, a, EM, GM, dfc));


     
    
    Poplv = ode45(flv,[0 364*nb_annees],Pinitlv); %% y'=f(t,y)
    
    Podelv = [Podelv Poplv.y(:,2:end)]; % on ajoute à Podelv les nouvelles valeurs de y (nos populations) 
    
    Todelv = [Todelv Todelv(end)+Poplv.x(2:end)]; % les nouvelles valeurs de t
    
    Pinitlv = Podelv(:,end); % on assigne la dernière valeur de y (les dernières valeurs connues de nos populations) à Pinitlv : c'est le point d'où commencera la prochaine série de calculs
    
        


TEBMlv = tebm(Podelv(2,:), a, EM);

REBMlv = []
for i=Podelv(2,:)
     res = rebm(i, a, EM, dfc);
     REBMlv = [REBMlv res]
end

POKBMlv = []
for i=1:length(Podelv(1,:))
     res = pokbm(Podelv(2,i), a, EM, dfc);
     POKBMlv = [POKBMlv res]
end

RKBMlv = []
for i=1:length(Podelv(1,:))
     res = rkbm(Podelv(2,i), Podelv(1,i), a, EM, dfc);
     RKBMlv = [RKBMlv res]
end



%%%_____________FIGURES______________________________________________

clf

debS = [91, 182, 273, 364]
for n=1:nb_annees-1
    debS = [debS, debS(end)+[91, 182, 273, 364]];
end

figure(1)

predateurlv=100*Podelv(1,:)/Podelv(1,1);
proielv=100*Podelv(2,:)/Podelv(2,1);
predateur=100*Pode(1,:)/Pode(1,1);
proie=100*Pode(2,:)/Pode(2,1);

%Tode=Tode/90;

figure(1)
subplot(2,1,1)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren'); % a quoi sert cela ?

h11 = plot(Todelv,[predateurlv;proielv],'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('All populations: % of the inital population');

legend(h11,{'predateur','proie'});

subplot(2,1,2)
newDefaultColors=lines(6);
set(gca, 'ColorOrder', newDefaultColors, 'NextPlot', 'replacechildren'); % a quoi sert cela ?

h12 = plot(Tode,[predateur;proie],'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Predators');

legend(h12,{'predateur','proie'});


figure(2)
subplot(2,2,1)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

h211 = plot(Todelv,predateurlv,'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Predators LV');

legend(h211,{'predateur'});


subplot(2,2,3)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h221 = plot(Todelv, proielv,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Prey LV');
legend(h221,{'proie'});

subplot(2,2,2)
newDefaultColors2 = newDefaultColors(1:2,:);
set(gca, 'ColorOrder', newDefaultColors2, 'NextPlot', 'replacechildren');

h212 = plot(Tode,predateur,'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Predators');

legend(h212,{'predateur'});


subplot(2,2,4)
newDefaultColors3 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h222 = plot(Tode, proie,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Prey');
legend(h222,{'proie'});


figure(3)

subplot(4,2,1)
newDefaultColors3 = newDefaultColors(1:4,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h311 = plot(Todelv,TEBMlv,'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Theorical eaten biomass');

legend(h311,{'Theorical eaten biomass'});


subplot(4,2,3)
newDefaultColors4 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors4, 'NextPlot', 'replacechildren');

h312 = plot(Todelv, REBMlv,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Real eaten biomass');
legend(h312,{'Real eaten biomass'});

subplot(4,2,5)
newDefaultColors5 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors5, 'NextPlot', 'replacechildren');

h313 = plot(Todelv, POKBMlv,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Proportion of overkilled biomass');
legend(h313,{'Proportion of overkilled biomass'});

subplot(4,2,7)
newDefaultColors6 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors6, 'NextPlot', 'replacechildren');

h314 = plot(Todelv, REBMlv,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Real killed biomass');
legend(h314,{'Real killed biomass'});
title('Real killed biomass');

subplot(4,2,2)
newDefaultColors3 = newDefaultColors(1:4,:);
set(gca, 'ColorOrder', newDefaultColors3, 'NextPlot', 'replacechildren');

h321 = plot(Tode,TEBM,'LineWidth',1);
xlabel('Number of seasons')
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Theorical eaten biomass');

legend(h321,{'Theorical eaten biomass'});


subplot(4,2,4)
newDefaultColors4 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors4, 'NextPlot', 'replacechildren');

h322 = plot(Tode, REBM,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Real eaten biomass');
legend(h322,{'Real eaten biomass'});

subplot(4,2,6)
newDefaultColors5 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors5, 'NextPlot', 'replacechildren');

h323 = plot(Tode, POKBM,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Proportion of overkilled biomass');
legend(h323,{'Proportion of overkilled biomass'});

subplot(4,2,8)
newDefaultColors6 = newDefaultColors(3:6,:);
set(gca, 'ColorOrder', newDefaultColors6, 'NextPlot', 'replacechildren');

h324 = plot(Tode, REBM,'LineWidth',2);
axis tight

for l=debS
    xline(l)
end
xticks(debS(mod(1:nb_annees*4, 10) == 0));
xticklabels(string((1:floor(nb_annees*4/10))*10))
title('Real killed biomass');
legend(h314,{'Real killed biomass'});
title('Real killed biomass');