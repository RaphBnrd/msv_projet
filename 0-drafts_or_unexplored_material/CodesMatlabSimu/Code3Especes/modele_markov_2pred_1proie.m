clear all

Pop=[];

Tmax = 10000 ; 

dt = 1;

alpha1n = 2.5/365 %% 2.5 femelles juveniles par an
alpha1m = 1/17 %% 17 jours si aucune interaction - perte de 171 g/j
alpha1 = alpha1n-alpha1m

alpha1 = -0.023

alpha2n = 5/365 %% 5 femelles juveniles par an
alpha2m = 1/18 %% 17 jours si aucune interaction - perte de 171 g/j
alpha2 = alpha2n-alpha2m

alpha2 = -0.026

alpha3 = 0.0023
alpha4 = 0.0026
alpha5 = 0.0044
alpha6 = 0.0029

kappa1 = 1;  % par hectare
kappa2 = 1;
kappa3 = 4.119;
kappa4 = 0.269;
kappa5 = 1.179; % en kg/ha 
kappa6 = 5.195; % 1 lapin par ha



F13aut = 0.42;
F14aut = 0.16;
F15aut = 1;
F16aut = 0.13;

F23aut = 0.3;
F24aut = 0;
F25aut = 1.4;
F26aut = 0.37;

Baut(1,3) = 0.061  * F(1,3);
Baut(1,4) = 0.0022 * F(1,4);
Baut(1,5) = 0.0045 * F15aut;
Baut(1,6) = 0.17   * F16aut;
Baut(2,3) = 0.053  * F23aut;
Baut(2,4) = 0.0025 * F24aut;
Baut(2,5) = 0.0051 * F25aut;
Baut(2,6) = 0.093  * F26aut;


B(3,1) = - F13aut ;
B(3,2) = - F23 ;
B(4,1) = - F14 ;
B(4,2) = - F24 ;
B(5,1) = - F15 ;
B(5,2) = - F25 ;
B(6,1) = - F16 ;
B(6,2) = - F26 ;

phi = @(y) [51*y./(1+y)]
psi = @(y) [y]
phi=psi

f = @(t,X) [alpha1*X(1)+B(1,3)*X(1)*phi(X(3)/kappa3)+B(1,4)*X(1)*phi(X(4)/kappa4)+B(1,5)*X(1)*phi(X(5)/kappa5)+B(1,6)*X(1)*phi(X(6)/kappa6);
    alpha2*X(2)+B(2,3)*X(2)*X(3)/(1+X(3)/kappa3)+B(2,4)*X(2)*phi(X(4)/kappa4)+B(2,5)*X(2)*phi(X(5)/kappa5)+B(2,6)*X(2)*phi(X(6)/kappa6);
    alpha3*(1-X(3)/kappa3)*X(3)+B(3,1)*psi(X(1)/kappa1)*X(3)+B(3,2)*psi(X(2)/kappa2)*X(3);
    alpha4*(1-X(4)/kappa4)*X(4)+B(4,1)*psi(X(1)/kappa1)*X(4)+B(4,2)*psi(X(2)/kappa2)*X(4);
    alpha5*(1-X(5)/kappa5)*X(5)+B(5,1)*psi(X(1)/kappa1)*X(5)+B(5,2)*psi(X(2)/kappa2)*X(5);
    alpha6*(1-X(6)/kappa6)*X(6)+B(6,1)*psi(X(1)/kappa1)*X(6)+B(6,2)*psi(X(2)/kappa2)*X(6);] ;

Xinit = [0.0176;0.0171;2.44;0.13;1.575;0.758]; 

X = Xinit;

Pop=X;

t=0;

T=t;

while t<Tmax
    dt=min(dt,Tmax-t)
    
    t=t+dt
    
    T=[T t];
end

Pop = ode45(f,T,Xinit)

clf
figure(1)

renards=100*Pop.y(1,:)/Xinit(1);
chats=100*Pop.y(2,:)/Xinit(2);

gros_oiseaux=100*Pop.y(3,:)/Xinit(3);
petits_oiseaux=100*Pop.y(4,:)/Xinit(4);
micromammiferes=100*Pop.y(5,:)/Xinit(5);
lapins=100*Pop.y(6,:)/Xinit(6);


plot(Pop.x,renards,'r',Pop.x,chats,'b', Pop.x, gros_oiseaux,'g',Pop.x, petits_oiseaux,'k', Pop.x, micromammiferes,'c', Pop.x, lapins,'m')
legend('renards','chats','gros oiseaux','petits oiseaux','micromammifères','lapins')
title('Pourcentage de la population de départ - Automne 2014 et 2015')