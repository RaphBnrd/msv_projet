clear all

Pop=[];

Tmax =1000;

nb_annees = 2 ; 

dt = 1;

alpha1n = 2.5/365 %% 2.5 femelles juveniles par an
alpha1m = 1/17 %% 17 jours si aucune interaction - perte de 171 g/j
alpha1 = alpha1n-alpha1m

alpha1 = -0.0023

alpha2n = 5/365 %% 5 femelles juveniles par an
alpha2m = 1/18 %% 17 jours si aucune interaction - perte de 171 g/j
alpha2 = alpha2n-alpha2m

alpha2 = -0.0026

alpha3 = 0.0023
alpha4 = 0.0026
alpha5 = 0.0044
alpha6 = 0.0029

kappa1 = 1;  % par hectare
kappa2 = 1;
kappa3 = 4.119;
kappa4 = 0.269;
kappa5 = 1.179; % en kg/ha 
kappa6 = 5.195; % 1 lapin par ha

A.F1 = [0.42	0.16	1	0.13];
A.F2 = [0.3	0	1.4	0.37];

A.Bpc1 = [0.061	0.0022	0.0045	0.17];
A.Bpc2 = [0.053	0.0025	0.0051	0.093];
 
A.B = zeros(6,6);
A.B(1,3:6) = A.F1.*A.Bpc1;
A.B(2,3:6) = A.F2.*A.Bpc2;
A.B(3:6,1) = - A.F1';
A.B(3:6,2) = - A.F2';



W.F1 = [0.25	0.21	2.6	0.07];
W.F2 = [0.42	0.25	1.6	0.25];

W.Bpc1 = [0.064	0.0021	0.005	0.14];
W.Bpc2 = [0.055	0.22	0.0049	0.088]; 

W.B = zeros(6,6);
W.B(1,3:6) = W.F1.*W.Bpc1;
W.B(2,3:6) = W.F2.*W.Bpc2;
W.B(3:6,1) = - W.F1';
W.B(3:6,2) = - W.F2';

Sp.F1 = [0.3	0.3	3.7	0.05];
Sp.F2 = [0.3	0.22	1.4	0.44];

Sp.Bpc1 = [0.066	0.002	0.0048	0.19];
Sp.Bpc2 = [0.052	0.0025	0.0049	0.1];

Sp.B = zeros(6,6);
Sp.B(1,3:6) = Sp.F1.*Sp.Bpc1;
Sp.B(2,3:6) = Sp.F2.*Sp.Bpc2;
Sp.B(3:6,1) = - Sp.F1';
Sp.B(3:6,2) = - Sp.F2';

Su.F1 = [0.053	0.0024	0.0048	0.13];
Su.F2 = [0.054	0.0024	0.0048	0.082];

Su.Bpc1 = [0.36	0.22	2.1	0.16];
Su.Bpc2 = [0.52	0.081	2.4	0.22;]

Su.B = zeros(6,6);
Su.B(1,3:6) = Su.F1.*Sp.Bpc1;
Su.B(2,3:6) = Su.F2.*Sp.Bpc2;
Su.B(3:6,1) = - Su.F1';
Su.B(3:6,2) = - Su.F2';

phi = @(y) [y./(1+y)]
psi = @(y) [y]
phi=psi

fcv = @(t,X,B) [alpha1*X(1)+B(1,3)*X(1)*phi(X(3)/kappa3)+B(1,4)*X(1)*phi(X(4)/kappa4)+B(1,5)*X(1)*phi(X(5)/kappa5)+B(1,6)*X(1)*phi(X(6)/kappa6);
    alpha2*X(2)+B(2,3)*X(2)*X(3)/(1+X(3)/kappa3)+B(2,4)*X(2)*phi(X(4)/kappa4)+B(2,5)*X(2)*phi(X(5)/kappa5)+B(2,6)*X(2)*phi(X(6)/kappa6);
    alpha3*(1-X(3)/kappa3)*X(3)+B(3,1)*psi(X(1)/kappa1)*X(3)+B(3,2)*psi(X(2)/kappa2)*X(3);
    alpha4*(1-X(4)/kappa4)*X(4)+B(4,1)*psi(X(1)/kappa1)*X(4)+B(4,2)*psi(X(2)/kappa2)*X(4);
    alpha5*(1-X(5)/kappa5)*X(5)+B(5,1)*psi(X(1)/kappa1)*X(5)+B(5,2)*psi(X(2)/kappa2)*X(5);
    alpha6*(1-X(6)/kappa6)*X(6)+B(6,1)*psi(X(1)/kappa1)*X(6)+B(6,2)*psi(X(2)/kappa2)*X(6);] ;

Pinit = [0.0176;0.0171;2.44;0.13;1.575;0.758]; 

P = Pinit;

Pop=P;

t=0;

Tsaison=t;

saison = 90 ;

while t<saison
    dt=min(dt,saison-t)
    
    t=t+dt
    
    Tsaison=[Tsaison t];
end

T = 0:360*nb_annees;

for n=0:4*nb_annees
     %% On commence toujours une ann�e en automne
     
     ns = mod(n,4) %% ns=0 : automne, ns=1 : hiver ...
    switch ns
        case 0
            periode = A
        case 1
            periode = W
        case 2
            periode = Sp
        case 3  
            periode = Su
    end
    
     f = @(t,X) (fcv(t,X,periode.B))

     Pop = ode45(f,Tsaison,Pinit); 
     
     P = [P Pop.y(:,2:end)];
     
     size(P)
     Pinit = P(:,end)
     
     
end    

clf
figure(1)

renards=100*P(1,:)/P(1,1);
chats=100*P(2,:)/P(2,1);

gros_oiseaux=100*P(3,:)/P(3,1);
petits_oiseaux=100*P(4,:)/P(4,1);
micromammiferes=100*P(5,:)/P(5,1);
lapins=100*P(6,:)/P(6,1);


plot(T,renards,'r',T,chats,'b', T, gros_oiseaux,'g',T, petits_oiseaux,'k', T, micromammiferes,'c', T, lapins,'m')
legend('renards','chats','gros oiseaux','petits oiseaux','micromammif�res','lapins')
title('Pourcentage de la population de d�part - Automne 2014 et 2015')