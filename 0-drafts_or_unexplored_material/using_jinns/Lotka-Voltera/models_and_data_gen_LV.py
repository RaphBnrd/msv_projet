import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']


# Functions to go from the ecological parameters to the dimensionless parameters
def eco_to_dimless(eco_params, func_rep="holling1"):
    """
    This function transforms the ecological parameters into the dimensionless parameters
    source: part 2.2 in 
        Lusardi, Léo and André, Eliot and Castañeda, Irene and Lemler, Sarah and Lafitte, Pauline and Zarzoso-Lacoste, Diane and Bonnaud, Elsa, 
        Methods for Comparing Theoretical Models Parameterized with Field Data Using Biological Criteria and Sobol Analysis. 
        Available at SSRN: https://ssrn.com/abstract=4659041 or http://dx.doi.org/10.2139/ssrn.4659041 
    -------------------
    input:
        eco_params: array of ecological parameters with the following keys
          . a1: the net intrinsic decay rate of the predator biomass
          . a2: the net intrinsic growth rate of the prey biomass
          . e: the proportion of biomass consumed by the predator on one biomass unit of prey
          . c: the conversion rate of one biomass unit of prey into one biomass unit of predator
          . K: area of the environment saturated per biomass unit of prey
          . a: the successful attack rate of the predator on the prey for Holling I (saturated or not) and II, or the successful attack rate of the predator on the prey per prey biomass unit 190 for Holling III
          . S: the ingestion capacity of the predator per time unit
          . b: the handling and resting time necessary for one biomass unit of predator after catching one biomass unit of prey, thereafter the handling time
          . xR1: quantity of reference biomass density units of the predator
          . xR2: quantity of reference biomass density units of the prey
        func_rep: the functional response of the predator on the prey, either "holling1", "holling1_sat", "holling2" or "holling3"
        
        Note that we have the following relationships between these parameters
          >  b = e / S
    output:
        array of dimensionless parameters with the following keys
          . kappa_tild: saturation rate of the environment in the presence of the reference biomass density of the prey
          . Tr1_tild: characteristic intrinsic decay time of the predators
          . Tr2_tild: characteristic intrinsic growth time of the prey
          . tc_tild: characteristic intrinsic growth time of the predator via the predation on the prey and in the presence of the reference prey density
          . ta_tild: characteristic decay time of the prey due to the predation in the presence of the reference predator density
          . lambda_tild: daily saturation rate of a predator's stomach in the presence of the reference prey biomass density (which is a constant of reference)
    """
    kappa_tild = eco_params["K"] * eco_params["xR2"]
    Tr1_tild = 1 / eco_params["a1"]
    Tr2_tild = 1 / eco_params["a2"]
    if func_rep in ["holling1", "holling1_sat", "holling2"]:
        Tc_tild = 1 / ( eco_params["a"] * eco_params["e"] * eco_params["c"] * eco_params["xR2"] )
        Ta_tild = 1 / ( eco_params["a"] * eco_params["xR1"] )
        lambda_tild = ( eco_params["a"] * eco_params["xR2"] * eco_params["e"] ) / eco_params["S"]
    elif func_rep == "holling3":
        Tc_tild = 1 / ( eco_params["a"] * eco_params["e"] * eco_params["c"] * eco_params["xR2"]**2 )
        Ta_tild = 1 / ( eco_params["a"] * eco_params["xR1"] * eco_params["xR2"] )
        lambda_tild = ( eco_params["a"] * eco_params["xR2"]**2 * eco_params["e"] ) / eco_params["S"]

    return {"kappa_tild": kappa_tild,
            "Tr1_tild": Tr1_tild,
            "Tr2_tild": Tr2_tild,
            "Tc_tild": Tc_tild,
            "Ta_tild": Ta_tild,
            "lambda_tild": lambda_tild}



def mod_dimless_LV_2pop(state, dimless_params, func_pred="holling1", Tref=1):
    """
    input:
        state = list of the 2 dimensionless populations (x1_tild : predator and x2_tild : prey)
        dimless_params = dictionary of the dimensionless parameters
        func_pred = functional response of the predator on the prey, either "holling1", "holling1_sat", "holling2" or "holling3"
        Tref = reference time unit (default is 1)
    output:
        list of the 2 derivatives of the dimensionless populations (dx1_tild/dt and dx2_tild/dt)
    
    source: part 2.2 in 
        Lusardi, Léo and André, Eliot and Castañeda, Irene and Lemler, Sarah and Lafitte, Pauline and Zarzoso-Lacoste, Diane and Bonnaud, Elsa, 
        Methods for Comparing Theoretical Models Parameterized with Field Data Using Biological Criteria and Sobol Analysis. 
        Available at SSRN: https://ssrn.com/abstract=4659041 or http://dx.doi.org/10.2139/ssrn.4659041 
    
    * * * Model of Lotka-Voltera with 2 populations * * *
    x1 : predator | x2 : prey
    dx1/dt = -a1*x1 + e*c*x1*func_rep(x2)
    dx2/dt = a2*x2*(1-K*x2) - x1*func_rep(x2)
    
    a1 death rate of predator | a2 growth rate of prey | c conversion efficiency | K carrying capacity | 
    e proportion of biomass consumed by the predator for a unit of prey
    
    func_rep : functional response of the predator
    a : attack rate | S : satiation | b : handling time (coefficient in function response)

    * * * Dimensionless model * * *
    Holling I
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * x2_tild ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * x1_tild ) * x2_tild
    Holling I with saturation
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * min(x2_tild, 1/lambda_tild) ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * min(x2_tild, 1/lambda_tild) * x1_tild / x2_tild ) * x2_tild
    Holling II
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * x2_tild / (1 + lambda_tild * x2_tild) ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * x1_tild / (1 + lambda_tild * x2_tild) ) * x2_tild
    Holling III
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * x2_tild**2 / (1 + lambda_tild * x2_tild**2) ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * x1_tild * x2_tild / (1 + lambda_tild * x2_tild**2) ) * x2_tild
    """
    x1_tild , x2_tild = state
    kappa_tild = dimless_params["kappa_tild"]
    Tr1_tild = dimless_params["Tr1_tild"]
    Tr2_tild = dimless_params["Tr2_tild"]
    Tc_tild = dimless_params["Tc_tild"]
    Ta_tild = dimless_params["Ta_tild"]
    lambda_tild = dimless_params["lambda_tild"]

    if func_pred == "holling1":
        coef_hol = 1
    elif func_pred == "holling1_sat":
        coef_hol = min(1, 1/(x2_tild*lambda_tild))
    elif func_pred == "holling2":
        coef_hol = 1 / (1 + lambda_tild * x2_tild)
    elif func_pred == "holling3":
        coef_hol = x2_tild / (1 + lambda_tild * x2_tild**2)
    
    dx1_tild_dt = ( - Tref / Tr1_tild \
                    + Tref / Tc_tild  * x2_tild * coef_hol )   *   x1_tild
    dx2_tild_dt = (   Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) \
                    - Tref / Ta_tild  * x1_tild * coef_hol )   *   x2_tild
    
    return [dx1_tild_dt, dx2_tild_dt]
    
    
class data_gen_LV2pop_adim:
    
    def __init__(self, dict_dimless_param, tmax, init_val, func_rep='holling1', 
                 nbpoint=10, noise_intensity=0, rd_seed=None, plot_data=False):
        self.tmax = tmax # t0 is 0 and tmax is the upper boundary of the simulation
        self.param = dict_dimless_param # with the parameters "kappa_tild", "Tr1_tild", "Tr2_tild", "Tc_tild", "Ta_tild", "lambda_tild"
        self.func_rep = func_rep
        self.init = init_val # forme [x1_tild_init, x2_tild_init] conditions initiales
        self.simulation = None
        self.t_samples = None
        self.data_noisy = None
        self.func_rep = func_rep
        self.file_data = None

        self.get_data(nbpoint=nbpoint, noise_intensity=noise_intensity, 
                      rd_seed=rd_seed, plot_data=plot_data)

    def lv_model(self, state, t):
        """
        Lotka-Voltera model
        """
        dX_tild_dt = mod_dimless_LV_2pop(state, self.param, func_pred=self.func_rep, Tref=self.tmax)
        return dX_tild_dt

    def simulate(self, nb_p_t=15000):
        self.t_simu = np.linspace(0, self.tmax, nb_p_t)
        self.simulation = odeint(self.lv_model, self.init, self.t_simu, atol=1e-8, rtol=1e-11)

    def get_data(self, nbpoint=10, noise_intensity=0, rd_seed=None, plot_data=False):
        if np.all(self.simulation) == None:
            self.simulate()
        np.random.seed(seed = rd_seed)
        t_samples = np.concatenate(( [self.t_simu[0]], 
                                     np.random.choice(self.t_simu[1:len(self.t_simu)], 
                                                      nbpoint-1, replace=False) ))
        t_samples.sort()
        idxs = np.where(np.isin(self.t_simu, t_samples))[0]
        data_true = self.simulation[idxs, :]
        # Add Gaussian noise to each column independently
        data_noisy = np.column_stack([
            data_true[:, 0] + noise_intensity * np.random.normal(0, 1, data_true.shape[0]),
            data_true[:, 1] + noise_intensity * np.random.normal(0, 1, data_true.shape[0])
        ])
        data_noisy = np.maximum(data_noisy, 0) # negative values are replaced by zeros

        self.data_noisy = data_noisy
        self.t_samples = t_samples.reshape((nbpoint,1))

        if plot_data:
            self.plot_initial_data()


    def plot_initial_data(self, elements_to_plot=["model", "data"], title=None):
        if title == None:
            title = "Simulation LV dimensionless " + self.func_rep
        if np.all(self.simulation) == None:
            self.simulate()
        if 'model' in elements_to_plot:
            plt.plot(self.t_simu, self.simulation[:, 0], '--', alpha=.5,
                     color=default_col_pal[0], label="Predator model")
            plt.plot(self.t_simu, self.simulation[:, 1], '--', alpha=.5,
                     color=default_col_pal[1], label="Prey model")
        if 'data' in elements_to_plot:
            plt.scatter(self.t_samples, self.data_noisy[:, 0],
                        color=default_col_pal[0], label="Predator data")
            plt.scatter(self.t_samples, self.data_noisy[:, 1],
                        color=default_col_pal[1], label="Prey data")
        plt.legend()
        plt.ylim(bottom=0)
        plt.title(title)

    def export_data(self, file_path=None):
        """
        Export the data in a .dat file
        """
        data_simu = np.hstack((self.t_samples, self.data_noisy))
        if file_path == None:
            file_path = "data_LV2pop_" + self.func_rep + ".dat"
        np.savetxt(file_path, data_simu)
        self.file_data = file_path


