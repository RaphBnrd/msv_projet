"""
This file contains various models of population dynamics, and the methods to transform ecological parameters into dimensionless parameters
"""

import numpy as np
import tensorflow as tf


                 # # # # # # # # # # # # # # # # # # # # # # # #
# ----------  # # # # # # # #    MODEL TO COMPARE   # # # # # # # # ---------- #
                 # # # # # # # # # # # # # # # # # # # # # # # #

def glycolysis_model(state,t,param = {"J0":2.5,"k1":100,"k2":6,"k3" : 16,"k4" : 100, "k5":1.28, "k6":12,"k":1.8,"kappa":13,"q":4,"K1":0.52,"psi":0.1,"N":1,"A":4} ):

    #Compute Core Quantities
    v1 = param["k1"] * state[0] * state[5] / (1 + (state[5] / param["K1"]) ** param["q"])
    v2 = param["k2"] * state[1] * (param["N"] - state[4])
    v3 = param["k3"] * state[2] * (param["A"] - state[5])
    v4 = param["k4"] * state[3] * state[4]
    v5 = param["k5"] * state[5]
    v6 = param["k6"] * state[1] * state[4]
    v7 = param["k"] * state[6]
    J = param["kappa"] * (state[3] - state[6])

    dX1_dt = param["J0"] - v1
    dX2_dt = 2 * v1 - v2 - v6
    dX3_dt = v2 - v3
    dX4_dt = v3 - v4 - J
    dX5_dt = v2 - v4 - v6
    dX6_dt = -2 * v1 + 2 * v3 - v5
    dX7_dt = param["psi"] * J - v7

    return [dX1_dt,dX2_dt,dX3_dt,dX4_dt,dX5_dt,dX6_dt,dX7_dt]

                 # # # # # # # # # # # # # # # # # # # # # # # #
# ----------  # # # # # # # #    SIMPLE EXP DECAY   # # # # # # # # ---------- #
                 # # # # # # # # # # # # # # # # # # # # # # # #

def exp_decay_model_derivatives(state, t, param={"k":1}):
    dX_dt = [-param["k"] * state[0]]
    return dX_dt


                 # # # # # # # # # # # # # # # # # # # # # # # #
# ----------  # # # # # # # #    LV 2 POPULATIONS   # # # # # # # # ---------- #
                 # # # # # # # # # # # # # # # # # # # # # # # #

def eco_to_dimless_2pop(eco_params, func_resp="holling1"):
    """
    This function transforms the ecological parameters into the dimensionless parameters
    source: part 2.2 in
        Lusardi, Léo and André, Eliot and Castañeda, Irene and Lemler, Sarah and Lafitte, Pauline and Zarzoso-Lacoste, Diane and Bonnaud, Elsa,
        Methods for Comparing Theoretical Models Parameterized with Field Data Using Biological Criteria and Sobol Analysis.
        Available at SSRN: https://ssrn.com/abstract=4659041 or http://dx.doi.org/10.2139/ssrn.4659041
    -------------------
    input:
        eco_params: array of ecological parameters with the following keys
          . a1: the net intrinsic decay rate of the predator biomass
          . a2: the net intrinsic growth rate of the prey biomass
          . e: the proportion of biomass consumed by the predator on one biomass unit of prey
          . c: the conversion rate of one biomass unit of prey into one biomass unit of predator
          . K: area of the environment saturated per biomass unit of prey
          . a: the successful attack rate of the predator on the prey for Holling I (saturated or not) and II, or the successful attack rate of the predator on the prey per prey biomass unit 190 for Holling III
          . S: the ingestion capacity of the predator per time unit
          . b: the handling and resting time necessary for one biomass unit of predator after catching one biomass unit of prey, thereafter the handling time
          . xR1: quantity of reference biomass density units of the predator
          . xR2: quantity of reference biomass density units of the prey
        func_resp: the functional response of the predator on the prey, either "holling1", "holling1_sat", "holling2" or "holling3"

        Note that we have the following relationships between these parameters
          >  b = e / S
    output:
        array of dimensionless parameters with the following keys
          . kappa_tild: saturation rate of the environment in the presence of the reference biomass density of the prey
          . Tr1_tild: characteristic intrinsic decay time of the predators
          . Tr2_tild: characteristic intrinsic growth time of the prey
          . tc_tild: characteristic intrinsic growth time of the predator via the predation on the prey and in the presence of the reference prey density
          . ta_tild: characteristic decay time of the prey due to the predation in the presence of the reference predator density
          . lambda_tild: daily saturation rate of a predator's stomach in the presence of the reference prey biomass density (which is a constant of reference)
    """
    kappa_tild = eco_params["K"] * eco_params["xR2"]
    Tr1_tild = 1 / eco_params["a1"]
    Tr2_tild = 1 / eco_params["a2"]
    if func_resp in ["holling1", "holling1_sat", "holling2"]:
        Tc_tild = 1 / ( eco_params["a"] * eco_params["e"] * eco_params["c"] * eco_params["xR2"] )
        Ta_tild = 1 / ( eco_params["a"] * eco_params["xR1"] )
        lambda_tild = ( eco_params["a"] * eco_params["xR2"] * eco_params["e"] ) / eco_params["S"]
    elif func_resp == "holling3":
        Tc_tild = 1 / ( eco_params["a"] * eco_params["e"] * eco_params["c"] * eco_params["xR2"]**2 )
        Ta_tild = 1 / ( eco_params["a"] * eco_params["xR1"] * eco_params["xR2"] )
        lambda_tild = ( eco_params["a"] * eco_params["xR2"]**2 * eco_params["e"] ) / eco_params["S"]

    return {"kappa_tild": kappa_tild,
            "Tr1_tild": Tr1_tild,
            "Tr2_tild": Tr2_tild,
            "Tc_tild": Tc_tild,
            "Ta_tild": Ta_tild,
            "lambda_tild": lambda_tild}


def lv_model_2pop_derivatives_basic(state, t, params):
    """
    input:
        state = list of the 2 populations (x1 : predator and x2 : prey)
        params = dictionary of the parameters
        func_resp = functional response of the predator on the prey, either "holling1", "holling1_sat", "holling2" or "holling3"
    output:
        list of the 2 derivatives of the populations (dx1/dt and dx2/dt)
    """
    x1 , x2 = state # predator and prey
    a1 = params["a1"]
    a2 = params["a2"]
    e = params["e"]
    c = params["c"]
    K = params["K"]
    a = params["a"]
    S = params["S"]
    if "b" in params:
        raise ValueError("The parameter 'b' is not used in the model of Lotka-Voltera with 2 populations...\nIt's value is necessarily e/S")
        # b = params["b"]
    b = e / S
    func_resp = params["func_resp"]

    if func_resp == "holling1":
        coef_hol = a*x2
    elif func_resp == "holling1_sat":
        if type(a) in [float, int, np.ndarray, np.float64, np.float32]:
            coef_hol = min(a*x2, S/e)
        else:
            coef_hol = tf.math.minimum(a*x2, S/e)
    elif func_resp == "holling2":
        coef_hol = a*x2 / (1 + a*b*x2)
    elif func_resp == "holling3":
        coef_hol = a*x2**2 / (1 + a*b*x2**2)

    dx1_dt = -a1*x1 + e*c*x1*coef_hol
    dx2_dt = a2*x2*(1-K*x2) - x1*coef_hol

    return [dx1_dt, dx2_dt]


def mod_dimless_LV_2pop(state, dimless_params, func_resp="holling1", Tref=1):
    """
    input:
        state = list of the 2 dimensionless populations (x1_tild : predator and x2_tild : prey)
        dimless_params = dictionary of the dimensionless parameters
        func_resp = functional response of the predator on the prey, either "holling1", "holling1_sat", "holling2" or "holling3"
        Tref = reference time unit (default is 1)
    output:
        list of the 2 derivatives of the dimensionless populations (dx1_tild/dt and dx2_tild/dt)

    source: part 2.2 in
        Lusardi, Léo and André, Eliot and Castañeda, Irene and Lemler, Sarah and Lafitte, Pauline and Zarzoso-Lacoste, Diane and Bonnaud, Elsa,
        Methods for Comparing Theoretical Models Parameterized with Field Data Using Biological Criteria and Sobol Analysis.
        Available at SSRN: https://ssrn.com/abstract=4659041 or http://dx.doi.org/10.2139/ssrn.4659041

    * * * Model of Lotka-Voltera with 2 populations * * *
    x1 : predator | x2 : prey
    dx1/dt = -a1*x1 + e*c*x1*func_resp(x2)
    dx2/dt = a2*x2*(1-K*x2) - x1*func_resp(x2)

    a1 death rate of predator | a2 growth rate of prey | c conversion efficiency | K carrying capacity |
    e proportion of biomass consumed by the predator for a unit of prey

    func_resp : functional response of the predator
    a : attack rate | S : satiation | b : handling time (coefficient in function response)

    * * * Dimensionless model * * *
    Holling I
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * x2_tild ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * x1_tild ) * x2_tild
    Holling I with saturation
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * min(x2_tild, 1/lambda_tild) ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * min(x2_tild, 1/lambda_tild) * x1_tild / x2_tild ) * x2_tild
    Holling II
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * x2_tild / (1 + lambda_tild * x2_tild) ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * x1_tild / (1 + lambda_tild * x2_tild) ) * x2_tild
    Holling III
        dx1_tild/dt = ( - Tref / Tr1_tild + Tref / Tc_tild * x2_tild**2 / (1 + lambda_tild * x2_tild**2) ) * x1_tild
        dx2_tild/dt = ( Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) - Tref / Ta_tild * x1_tild * x2_tild / (1 + lambda_tild * x2_tild**2) ) * x2_tild
    """
    x1_tild , x2_tild = state
    kappa_tild = dimless_params["kappa_tild"]
    Tr1_tild = dimless_params["Tr1_tild"]
    Tr2_tild = dimless_params["Tr2_tild"]
    Tc_tild = dimless_params["Tc_tild"]
    Ta_tild = dimless_params["Ta_tild"]
    lambda_tild = dimless_params["lambda_tild"]

    if func_resp == "holling1":
        coef_hol = 1
    elif func_resp == "holling1_sat":
        if isinstance(lambda_tild, tf.Variable):
            coef_hol = tf.minimum(1, 1/(x2_tild*lambda_tild))
        else:
            coef_hol = min(1, 1/(x2_tild*lambda_tild))
    elif func_resp == "holling2":
        coef_hol = 1 / (1 + lambda_tild * x2_tild)
    elif func_resp == "holling3":
        coef_hol = x2_tild / (1 + lambda_tild * x2_tild**2)

    dx1_tild_dt = ( - Tref / Tr1_tild \
                    + Tref / Tc_tild  * x2_tild * coef_hol )   *   x1_tild
    dx2_tild_dt = (   Tref / Tr2_tild * ( 1 - kappa_tild * x2_tild ) \
                    - Tref / Ta_tild  * x1_tild * coef_hol )   *   x2_tild

    return [dx1_tild_dt, dx2_tild_dt]


def lv_model_2pop_derivatives(state, t, dimless_params):
    """
    Compute the derivatives of the dimensionless populations
    according to the Lotka-Voltera model
        NB: dimless_params also contains func_resp, Tref
    """
    dX_tild_dt = mod_dimless_LV_2pop(state, dimless_params,
                                     func_resp = dimless_params["func_resp"],
                                     Tref = dimless_params["Tref"])
    return dX_tild_dt





                 # # # # # # # # # # # # # # # # # # # # # # # #
# ----------  # # # # # # # #    LV 2 POPULATIONS   # # # # # # # # ---------- #
                 # # # # # # # # # # # # # # # # # # # # # # # #


def eco_to_dimless_Npop(eco_params, func_resp="holling1"):
    """
        # # # # # # # # # # # # #
    # # # # # NOT COMPLETED # # # # #
        # # # # # # # # # # # # #

    This function transforms the ecological parameters into the dimensionless parameters
    - n_preds : number of predators
    - n_preys : number of preys
    - K : vector length n_preys (carrying capacities)
    - xR : vector length n_preds + n_preys (reference biomass densities)
    - alpha : vector length n_preds + n_preys (intrinsic growth rates)
    """
    n_preds = len(eco_params["xR"]) - len(eco_params["K"])
    n_preys = len(eco_params["K"])

    kappa_tild = eco_params["K"] * eco_params["xR"][n_preds:] # vector with n_preys elements
    Tr_tild = 1 / eco_params["alpha"] # vector with n_preds+n_preys elements
    # if func_resp in ["holling1", "holling1_sat", "holling2"]:
    #     Tc_tild = 1 / ( eco_params["a"] * eco_params["e"] * eco_params["c"] * eco_params["xR"][n_preds:] )
    #     Ta_tild = 1 / ( eco_params["a"] * eco_params["xR"][:n_preds] )
    #     lambda_tild = ( eco_params["a"] * eco_params["xR"][n_preds:] * eco_params["e"] ) / eco_params["S"]
    # elif func_resp == "holling3":
    #     Tc_tild = 1 / ( eco_params["a"] * eco_params["e"] * eco_params["c"] * eco_params["xR2"]**2 )
    #     Ta_tild = 1 / ( eco_params["a"] * eco_params["xR1"] * eco_params["xR2"] )
    #     lambda_tild = ( eco_params["a"] * eco_params["xR2"]**2 * eco_params["e"] ) / eco_params["S"]

    # Tc_tild = ["Tc_tild"]         # matrix with n_preds rows and n_preys columns
    # Ta_tild = ["Ta_tild"]         # matrix with n_preds rows and n_preys columns
    # lambda_tild = ["lambda_tild"] # matrix with n_preds rows and n_preys columns

    return {"kappa_tild": kappa_tild,
            # "Tc_tild": Tc_tild,
            # "Ta_tild": Ta_tild,
            # "lambda_tild": lambda_tild,
            "Tr_tild": Tr_tild}


"""
Prédateurs : Renards roux   |   Chats domestiques
Proies : Oiseaux de moyenne et grande taille   |   Oiseaux de petite taille   |   Micromammifères   |   Lagomorphes
"""

params_6pop_dimless = {
    "kappa_tild": np.array([0.055 , 0.014 , 0.031 , 0.030]), # vector with n_preys elements

    "lambda_tild_H1": np.array([[0.19 , 0.0028 , 0.64 , 0.21], # row1=fox, row1=cat
                               [0.29 , 0.0023 , 0.59 , 0.43]]), # matrix with n_preds rows and n_preys columns
    "lambda_tild_H2": np.array([[0.30 , 0.0047 , 0.99 , 0.88],
                               [0.74 , 0.0059 , 0.13 , 1.1]]),
    "lambda_tild_H3": np.array([[0.57 , 0.0059 , 10 , 0.97],
                               [1.7 , 0.0064 , 37 , 2.2]]),

    "lambda_tild_autumn_H1": np.array([[0.34 , 0.0013 , 0.27 , 0.16],
                                      [0.21 , 0 , 1.2 , 0.38]]),
    "lambda_tild_winter_H1": np.array([[0.091 , 0.0013 , 0.58 , 0.13],
                                      [0.27 , 0.0031 , 0.44 , 0.29]]),
    "lambda_tild_spring_H1": np.array([[0.24 , 0.0031 , 1.4 , 0.062],
                                      [0.59 , 0.0039 , 0.41 , 0.54]]),
    "lambda_tild_summer_H1": np.array([[0.083 , 0.0051 , 0.18 , 0.47],
                                      [0.063 , 0.0021 , 0.22 , 0.47]]),
    "lambda_tild_autumn_H2": np.array([[0.57 , 0.0022 , 0.42 , 0.26],
                                      [0.66 , 0 , 3.0 , 0.97]]),
    "lambda_tild_winter_H2": np.array([[0.14 , 0.0018 , 0.91 , 0.15],
                                      [0.60 , 0.0067 , 0.58 , 0.61]]),
    "lambda_tild_spring_H2": np.array([[0.35 , 0.0044 , 1.96 , 0.094],
                                      [1.5 , 0.012 , 1.2 , 1.6]]),
    "lambda_tild_summer_H2": np.array([[0.11 , 0.011 , 0.46 , 0.086],
                                      [0.17 , 0.0047 , 0.54 , 1.2]]),
    "lambda_tild_autumn_H3": np.array([[0.84 , 0.0014 , 13 , 0.34],
                                      [1.1 , 0 , 1.3e2 , 1.2]]),
    "lambda_tild_winter_H3": np.array([[0.21 , 0.0018 , 5.5 , 0.26],
                                      [0.79 , 0.0067 , 3.6 , 0.88]]),
    "lambda_tild_spring_H3": np.array([[1.1 , 0.0045 , 21 , 0.13],
                                      [5.0 , 0.012 , 14 , 2.6]]),
    "lambda_tild_summer_H3": np.array([[0.045 , 0.015 , 1.8 , 3.1],
                                      [0.073 , 0.0069 , 2.4 , 3.9]]),

    "Tr_tild": np.array([11 , 11 , 286 , 238 , 50 , 189]), # vector with n_preds + n_preys elements
    "Tr_tild_autumn": np.array([11 , 11]),
    "Tr_tild_winter": np.array([11 , 11]),
    "Tr_tild_spring": np.array([11 , 11]),
    "Tr_tild_summer": np.array([12 , 11]),

    "Tc_tild_H1": np.array([[44 , 3.1e3 , 13 , 41],
                           [33 , 4.1e3 , 16 , 22]]), # matrix with n_preds rows and n_preys columns
    "Tc_tild_H2": np.array([[28 , 1.8e3 , 8.8 , 23],
                           [13 , 1.6e3 , 72 , 8.3]]),
    "Tc_tild_H3": np.array([[15 , 1.4e3 , 0.79 , 8.8],
                           [5.4 , 1.4e3 , 0.26 , 4.3]]),
    "Tc_tild_autumn_H1": np.array([[25 , 6.7e3 , 33 , 53],
                                  [44 , np.nan , 7.6 , 25]]),
    "Tc_tild_winter_H1": np.array([[86 , 5.9e3 , 14 , 59],
                                  [34 , 3.0e3 , 22 , 31]]),
    "Tc_tild_spring_H1": np.array([[33 , 2.5e3 , 5.7 , 1.3e2],
                                  [16 , 2.4e3 , 23 , 17]]),
    "Tc_tild_summer_H1": np.array([[1.0e2 , 1.7e3 , 48 , 18],
                                  [1.5e2 , 4.5e3 , 42 , 20]]),
    "Tc_tild_autumn_H2": np.array([[15 , 3.9e3 , 20 , 33],
                                  [14 , np.nan , 3.2 , 9.6]]),
    "Tc_tild_winter_H2": np.array([[56 , 4.3e3 , 8.8 , 51],
                                  [15 , 1.4e3 , 16 , 15]]),
    "Tc_tild_spring_H2": np.array([[22 , 1.8e3 , 4.1 , 91],
                                  [6.2 , 7.6e2 , 7.6 , 5.7]]),
    "Tc_tild_summer_H2": np.array([[79 , 8.1e2 , 19 , 9.6],
                                  [56 , 2.0e3 , 18 , 7.8]]),
    "Tc_tild_autumn_H3": np.array([[10 , 6.1e3 , 0.66 , 25],
                                  [8.5 , np.nan , 0.074 , 7.7]]),
    "Tc_tild_winter_H3": np.array([[37 , 4.3e3 , 1.4 , 30],
                                  [12 , 1.4e3 , 2.6 , 10]]),
    "Tc_tild_spring_H3": np.array([[7.4 , 1.8e3 , 0.38 , 61],
                                  [1.9 , 7.8e2 , 0.66 , 3.6]]),
    "Tc_tild_summer_H3": np.array([[1.9e2 , 5.5e2 , 4.3 , 2.7],
                                  [1.3e2 , 1.3e3 , 3.9 , 2.4]]),

    "Ta_tild_H1": np.array([[6.0e2 , 1.1e3 , 98 , 1.0e2],
                           [9.2e2 , 3.1e3 , 2.5e2 , 67]]), # matrix with n_preds rows and n_preys columns
    "Ta_tild_H2": np.array([[3.8e2 , 6.4e2 , 64 , 60],
                           [3.6e2 , 1.2e3 , 1.1e3 , 26]]),
    "Ta_tild_H3": np.array([[2.0e2 , 5.1e2 , 5.7 , 22],
                           [1.5e2 , 1.1e3 , 3.9 , 13]]),
    "Ta_tild_autumn_H1": np.array([[3.3e2 , 2.4e3 , 2.4e2 , 1.3e2],
                                  [1.3e3 , np.nan , 1.2e2 , 76]]),
    "Ta_tild_winter_H1": np.array([[1.2e3 , 2.0e3 , 1.0e2 , 1.5e2],
                                  [9.7e2 , 2.2e3 , 3.3e2 , 97]]),
    "Ta_tild_spring_H1": np.array([[4.4e2 , 9.0e2 , 42 , 3.2e2],
                                  [4.5e2 , 1.8e3 , 3.5e2 , 53]]),
    "Ta_tild_summer_H1": np.array([[1.4e3 , 6.0e2 , 3.5e2 , 46],
                                  [4.2e3 , 3.4e3 , 6.5e2 , 60]]),
    "Ta_tild_autumn_H2": np.array([[2.0e2 , 1.4e3 , 1.5e2 , 83],
                                  [4.0e2 , np.nan , 49 , 30]]),
    "Ta_tild_winter_H2": np.array([[7.6e2 , 1.5e3 , 64 , 1.3e2],
                                  [4.4e2 , 1.0e3 , 2.5e2 , 47]]),
    "Ta_tild_spring_H2": np.array([[3.0e2 , 6.4e2 , 30 , 2.3e2],
                                  [1.8e2 , 5.7e2 , 1.2e2 , 18]]),
    "Ta_tild_summer_H2": np.array([[1.1e3 , 2.9e2 , 1.4e2 , 25],
                                  [1.6e3 , 1.5e3 , 2.7e2 , 24]]),
    "Ta_tild_autumn_H3": np.array([[1.4e2 , 2.2e3 , 4.8 , 63],
                                  [2.4e2 , np.nan , 1.1 , 24]]),
    "Ta_tild_winter_H3": np.array([[5.0e2 , 1.5e3 , 11 , 77],
                                  [3.3e2 , 1.0e3 , 40 , 32]]),
    "Ta_tild_spring_H3": np.array([[1.0e2 , 6.2e2 , 2.8 , 1.5e2],
                                  [53 , 5.8e2 , 10 , 11]]),
    "Ta_tild_summer_H3": np.array([[2.5e3 , 2.0e2 , 32 , 6.9],
                                  [3.6e3 , 1.0e3 , 60 , 7.3]]),


}


def dict_dimless_params_compact_to_long(dimless_params_Npop):
    dimless_params_Npop_long = {}
    for k in range(dimless_params_Npop['n_preys']):
        dimless_params_Npop_long['kappa_tild_prey'+str(k+1)] = dimless_params_Npop['kappa_tild'][k]
    for i in range(dimless_params_Npop['n_preds']):
        for j in range(dimless_params_Npop['n_preys']):
            dimless_params_Npop_long['Tc_tild_pred'+str(i+1)+'prey'+str(j+1)] = dimless_params_Npop['Tc_tild'][i,j]
            dimless_params_Npop_long['Ta_tild_pred'+str(i+1)+'prey'+str(j+1)] = dimless_params_Npop['Ta_tild'][i,j]
            dimless_params_Npop_long['lambda_tild_pred'+str(i+1)+'prey'+str(j+1)] = dimless_params_Npop['lambda_tild'][i,j]
    for i in range(dimless_params_Npop['n_preds']):
        dimless_params_Npop_long['Tr_tild_pred'+str(i+1)] = dimless_params_Npop['Tr_tild'][i]
    for j in range(dimless_params_Npop['n_preys']):
        dimless_params_Npop_long['Tr_tild_prey'+str(j+1)] = dimless_params_Npop['Tr_tild'][j+dimless_params_Npop['n_preds']]
    dimless_params_Npop['Tr_tild']
    dimless_params_Npop_long['func_resp'] = dimless_params_Npop['func_resp']
    dimless_params_Npop_long['Tref'] = dimless_params_Npop['Tref']
    dimless_params_Npop_long['n_preds'] = dimless_params_Npop['n_preds']
    dimless_params_Npop_long['n_preys'] = dimless_params_Npop['n_preys']

    return dimless_params_Npop_long


def dict_dimless_params_long_to_compact(dimless_params_Npop_long):
    dimless_params_Npop = {}
    # Invert the 'kappa_tild_preyX' keys
    kappa_tild_keys = sorted([key for key in dimless_params_Npop_long if key.startswith('kappa_tild_prey')])
    dimless_params_Npop['kappa_tild'] = np.array([dimless_params_Npop_long[key] for key in kappa_tild_keys])
    # Invert the 'Tc_tild_preyXpredY', 'Ta_tild_preyXpredY', and 'lambda_tild_preyXpredY' keys
    Tc_tild_keys = sorted([key for key in dimless_params_Npop_long if key.startswith('Tc_tild_pred')])
    Ta_tild_keys = sorted([key for key in dimless_params_Npop_long if key.startswith('Ta_tild_pred')])
    lambda_tild_keys = sorted([key for key in dimless_params_Npop_long if key.startswith('lambda_tild_pred')])
    dimless_params_Npop['Tc_tild'] = np.full((dimless_params_Npop_long['n_preds'], dimless_params_Npop_long['n_preys']), np.nan)
    dimless_params_Npop['Ta_tild'] = np.full((dimless_params_Npop_long['n_preds'], dimless_params_Npop_long['n_preys']), np.nan)
    dimless_params_Npop['lambda_tild'] = np.full((dimless_params_Npop_long['n_preds'], dimless_params_Npop_long['n_preys']), np.nan)
    for i in range(dimless_params_Npop_long['n_preds']):
        for j in range(dimless_params_Npop_long['n_preys']):
            dimless_params_Npop['Tc_tild'][i, j] = dimless_params_Npop_long[Tc_tild_keys[i * dimless_params_Npop_long['n_preys'] + j]]
            dimless_params_Npop['Ta_tild'][i, j] = dimless_params_Npop_long[Ta_tild_keys[i * dimless_params_Npop_long['n_preys'] + j]]
            dimless_params_Npop['lambda_tild'][i, j] = dimless_params_Npop_long[lambda_tild_keys[i * dimless_params_Npop_long['n_preys'] + j]]
    # Invert the 'Tr_tild_predX' keys
    dimless_params_Npop['Tr_tild'] = np.full((dimless_params_Npop_long['n_preds']+dimless_params_Npop_long['n_preys'],), np.nan)
    Tr_tild_pred_keys = [key for key in dimless_params_Npop_long if key.startswith('Tr_tild_pred')]
    dimless_params_Npop['Tr_tild'][:dimless_params_Npop_long['n_preds']] = [dimless_params_Npop_long[key] for key in Tr_tild_pred_keys]
    # Invert the 'Tr_tild_preyX' keys
    Tr_tild_prey_keys = [key for key in dimless_params_Npop_long if key.startswith('Tr_tild_prey')]
    dimless_params_Npop['Tr_tild'][dimless_params_Npop_long['n_preds']:] = [dimless_params_Npop_long[key] for key in Tr_tild_prey_keys]
    # Copy the remaining keys
    dimless_params_Npop['func_resp'] = dimless_params_Npop_long['func_resp']
    dimless_params_Npop['Tref'] = dimless_params_Npop_long['Tref']
    dimless_params_Npop['n_preds'] = dimless_params_Npop_long['n_preds']
    dimless_params_Npop['n_preys'] = dimless_params_Npop_long['n_preys']

    return dimless_params_Npop




def mod_dimless_LV_Npop(state, dimless_params, func_resp="holling1", Tref=1, n_preds=2, n_preys=4):
    """
    input:
        state = list of the dimensionless populations (n_preds predators, and then n_preys preys)
        dimless_params = dictionary of the dimensionless parameters
        func_resp = functional response of the predator on the prey, either "holling1", "holling1_sat", "holling2" or "holling3"
        Tref = reference time unit (default is 1)
    output:
        list of the 6 derivatives of the dimensionless populations (dx1_tild/dt and dx2_tild/dt)

    source: p.212 in
        Lusardia, L., 2023. Modélisation et prédiction de la dynamique d'un réseau trophique
        en réponse aux pressions d'origine anthropique. Université Paris-Saclay.

    """
    if n_preds + n_preys != len(state):
        raise ValueError("Error: the number of predators and preys does not match the length of the state vector")

    X = np.array(state)
    X_preds = X[:n_preds]
    X_preys = X[n_preds:]

    lambda_tild = dimless_params["lambda_tild"] # matrix with n_preds rows and n_preys columns
    Tr_tild = dimless_params["Tr_tild"] # vector with n_preds+n_preys elements
    Tc_tild = dimless_params["Tc_tild"] # matrix with n_preds rows and n_preys columns
    Ta_tild = dimless_params["Ta_tild"] # matrix with n_preds rows and n_preys columns
    kappa_tild = dimless_params["kappa_tild"] # vector with n_preys elements

    coef_hol = []  # matrix with n_preds rows and n_preys columns
    for i in range(n_preds):
        if func_resp == "holling1":
            coef_hol.append( [1] * n_preys )
        elif func_resp == "holling1_sat":
            coef_hol.append( [min(  1  ,  1 / ( lambda_tild[i,:].dot(X_preys) )  )] * n_preys )
        elif func_resp == "holling2":
            coef_hol.append(  [1 / ( 1 + lambda_tild[i,:].dot(X_preys) )] * n_preys  )
        elif func_resp == "holling3":
            coef_hol.append(  [ X_preys[j] / ( 1 + lambda_tild[i,:].dot(X_preys**2) ) for j in range(n_preys) ]  )
    coef_hol = np.array(coef_hol)

    dX_tild_dt = []
    for i in range(n_preds):
        dX_tild_dt.append( Tref * state[i] * ( - 1 / Tr_tild[i]   +   np.sum( X_preys / Tc_tild[i,:] * coef_hol[i,:] ) )  )
    for i in range(n_preds, n_preds+n_preys):
        dX_tild_dt.append( Tref * state[i] *  ( (1 - kappa_tild[i-n_preds] * state[i] ) / Tr_tild[i-n_preds]  \
                                                 - np.sum( X_preds / Ta_tild[:,i-n_preds] * coef_hol[:,i-n_preds] )  ) )

    return dX_tild_dt


def lv_model_Npop_derivatives(state, t, dimless_params_long):
    """
    Compute the derivatives of the dimensionless populations
    according to the Lotka-Voltera model
        NB: dimless_params also contains func_resp, Tref, n_preds, n_preys
    """
    dimless_params = dict_dimless_params_long_to_compact(dimless_params_long)
    dX_tild_dt = mod_dimless_LV_Npop(state, dimless_params,
                                     func_resp = dimless_params["func_resp"], Tref = dimless_params["Tref"],
                                     n_preds = dimless_params["n_preds"], n_preys = dimless_params["n_preys"])
    return dX_tild_dt


def mod_dimless_LV_Npop_noarray(state, dimless_params_long, func_resp="holling1", Tref=1, n_preds=2, n_preys=4):
    if n_preds + n_preys != len(state):
        raise ValueError("Error: the number of predators and preys does not match the length of the state vector")

    X_preds = state[:n_preds]
    X_preys = state[n_preds:]

    coef_hol = []  # matrix with n_preds rows and n_preys columns
    for i in range(n_preds):
        if func_resp == "holling1":
            coef_hol.append( [1] * n_preys )
        elif func_resp == "holling1_sat":
            lambda_dot_X_preys = 0
            for k in range(n_preys):
                lambda_dot_X_preys += dimless_params_long["lambda_tild_pred"+str(i+1)+"prey"+str(k+1)] * X_preys[k]
            coef_hol.append( [min(  1  ,  1 / lambda_dot_X_preys  )] * n_preys )
        elif func_resp == "holling2":
            lambda_dot_X_preys = 0
            for k in range(n_preys):
                lambda_dot_X_preys += dimless_params_long["lambda_tild_pred"+str(i+1)+"prey"+str(k+1)] * X_preys[k]
            coef_hol.append(  [1 / ( 1 + lambda_dot_X_preys )] * n_preys  )
        elif func_resp == "holling3":
            lambda_dot_X_preys = 0
            for k in range(n_preys):
                lambda_dot_X_preys_sq += dimless_params_long["lambda_tild_pred"+str(i+1)+"prey"+str(k+1)] * X_preys[k]**2
            coef_hol.append(  [ X_preys[j] / ( 1 + lambda_dot_X_preys_sq ) for j in range(n_preys) ]  )
    coef_hol = np.array(coef_hol)

    dX_tild_dt = []
    for i in range(n_preds):
        for k in range(n_preys):
            if k == 0:
                sum_X_preys_on_Tc_i_times_coef = X_preys[k] / dimless_params_long["Tc_tild_pred"+str(i+1)+"prey"+str(k+1)] * coef_hol[i,k]
            sum_X_preys_on_Tc_i_times_coef += X_preys[k] / dimless_params_long["Tc_tild_pred"+str(i+1)+"prey"+str(k+1)] * coef_hol[i,k]
        dX_tild_dt.append( Tref * state[i] * \
                          ( - 1 / dimless_params_long["Tr_tild_pred"+str(i+1)]   +   sum_X_preys_on_Tc_i_times_coef )  )
    for i in range(n_preds, n_preds+n_preys):
        for k in range(n_preds):
            if k == 0:
                sum_X_preds_on_Ta_k_times_coef = X_preds[k] / dimless_params_long["Ta_tild_pred"+str(k+1)+"prey"+str(i-n_preds+1)] * coef_hol[k,i-n_preds]
            sum_X_preds_on_Ta_k_times_coef += X_preds[k] / dimless_params_long["Ta_tild_pred"+str(k+1)+"prey"+str(i-n_preds+1)] * coef_hol[k,i-n_preds]
        dX_tild_dt.append( Tref * state[i] *  \
                          ( (1 - dimless_params_long["kappa_tild_prey"+str(i-n_preds+1)] * state[i] ) / dimless_params_long["Tr_tild_prey"+str(i-n_preds+1)]  \
                                                 - sum_X_preds_on_Ta_k_times_coef ) )
    return dX_tild_dt


def lv_model_Npop_derivatives_noarray(state, t, dimless_params_long):
    """
    Compute the derivatives of the dimensionless populations
    according to the Lotka-Voltera model
        NB: dimless_params_long also contains func_resp, Tref, n_preds, n_preys
    """
    dX_tild_dt = mod_dimless_LV_Npop_noarray(state, dimless_params_long,
                                             func_resp = dimless_params_long["func_resp"], Tref = dimless_params_long["Tref"],
                                             n_preds = dimless_params_long["n_preds"], n_preys = dimless_params_long["n_preys"])
    return dX_tild_dt





if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from scipy.integrate import odeint

    print("This is the models_catalogue.py file")

    print("\n\n\n- - - Test the Model LV 2 populations - - -")
    func_resp = "holling3"
    Tref = 10
    eco_params_2pop = {
        "a1": 0.088,
        "a2": 0.020,
        "e": 1,
        "c": 0.96,
        "K": 0.034,
        "S": 0.12,
        "xR1": 0.12,
        "xR2": 0.91
    }
    eco_params_2pop["b"] = eco_params_2pop["e"] / eco_params_2pop["S"]
    if func_resp == 'holling1' or func_resp == 'holling1_sat':
        eco_params_2pop["a"] = 0.086
    elif func_resp == 'holling2':
        eco_params_2pop["a"] = 0.13
    elif func_resp == 'holling3':
        eco_params_2pop["a"] = 1.6

    dimless_params_2pop = eco_to_dimless_2pop(eco_params_2pop, func_resp=func_resp)
    dimless_params_2pop["func_resp"] = func_resp
    dimless_params_2pop["Tref"] = Tref

    print("\nParameters:", eco_params_2pop)
    print("\nDimensionless parameters:", dimless_params_2pop)
    print("\nResponse function:", func_resp, "\n\n\n")

    tmax = 400
    x1_tild_ini, x2_tild_ini = 10, 20 # Predator, Prey
    nb_p_t = 10000

    t_simu = np.linspace(0, tmax, nb_p_t)
    X_simu = odeint(lv_model_2pop_derivatives, [x1_tild_ini, x2_tild_ini], t_simu,
                    args=(dimless_params_2pop,), atol=1e-8, rtol=1e-11)

    plt.figure()
    plt.plot(t_simu, X_simu, label=["Predator", "Prey"])
    plt.title(f"Lotka-Voltera model with 2 populations ({func_resp})")
    plt.legend()
    # plt.show()




    print("\n\n\n- - - Test the Model LV 6 populations - - -")

    func_resp = 'holling1'
    Tref = 10
    n_preds, n_preys = 2, 4
    if func_resp == "holling1" or func_resp == "holling1_sat":
        suff_hol = "H1"
    elif func_resp == "holling2":
        suff_hol = "H2"
    elif func_resp == "holling3":
        suff_hol = "H3"

    dimless_params_6pop = {"kappa_tild": params_6pop_dimless["kappa_tild"],
                           "Tc_tild": params_6pop_dimless["Tc_tild_"+suff_hol],
                           "Ta_tild": params_6pop_dimless["Ta_tild_"+suff_hol],
                           "lambda_tild": params_6pop_dimless["lambda_tild_"+suff_hol],
                           "Tr_tild": params_6pop_dimless["Tr_tild"] }
    dimless_params_6pop["func_resp"] = func_resp
    dimless_params_6pop["Tref"] = Tref
    dimless_params_6pop["n_preds"] = n_preds
    dimless_params_6pop["n_preys"] = n_preys

    dimless_params_6pop_long = dict_dimless_params_compact_to_long(dimless_params_6pop)
    print("\nDimensionless parameters:", dimless_params_6pop_long)
    print("\nResponse function:", func_resp, "\n\n\n")

    tmax = 100
    X_tild_ini = [10, 2, 5, 5, 30, 50] # 2 predators, 4 preys
    nb_p_t = 10000

    t_simu = np.linspace(0, tmax, nb_p_t)
    X_simu = odeint(lv_model_Npop_derivatives, X_tild_ini, t_simu,
                    args=(dimless_params_6pop_long,), atol=1e-8, rtol=1e-11)

    plt.figure()
    plt.plot(t_simu, X_simu, label=["Predator " + str(k+1) for k in range(n_preds)] + \
                                    ["Prey " + str(k+1) for k in range(n_preys)] )
    plt.title(f"Lotka-Voltera model with 6 populations ({func_resp})")
    plt.legend()
    # plt.show()




    print("\n\n\n- - - Test the Model Exponential decay - - -")

    param = {"k": 0.5}
    print("\nExponential decay parameter:", param["k"], "\n\n\n")

    tmax = 10
    x_ini = 2
    nb_p_t = 100

    t_simu = np.linspace(0, tmax, nb_p_t)
    X_simu = odeint(exp_decay_model_derivatives, [x_ini], t_simu, args=(param,), atol=1e-8, rtol=1e-11)

    plt.figure()
    plt.plot(t_simu, X_simu)
    plt.title(f"Exponential decay (parameter k={param['k']})")
    plt.show()
