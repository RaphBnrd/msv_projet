
import numpy as np
from scipy.integrate import odeint, solve_ivp
from scipy.optimize import curve_fit, minimize
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import time
import csv


class scipy_fitting:

    def __init__(self, model_derivatives, data):
        self.model_derivatives = model_derivatives
        self.data = data
        self.t_fit = None
        self.state_fit = None
        self.t_sample_common = all([set(self.data.t_samples[0].flatten()) == set(self.data.t_samples[i].flatten()) \
                                    for i in range(1, len(self.data.t_samples))]) # if True, all t_samples are the same, and we can use the curve_fit method
        self.time_full_train_in_sec = None

    def get_train_data(self, new_gen_data=False, 
                       plan=None, nbpoint_if_no_plan=10, noise_intensity=0, noise_seed=None,
                       permissible_range=[-np.inf, np.inf], default_value=0):
        if new_gen_data or self.data.states_data_noisy==None:
            if plan == None:
                self.data.get_data(self, plan=plan, nbpoint_if_no_plan=nbpoint_if_no_plan, 
                                   noise_intensity=noise_intensity, noise_seed=noise_seed, plot_data=False, 
                                   permissible_range=permissible_range, default_value=default_value)
        if self.t_sample_common:
            print("We will use the same t_samples for all states for the curve_fit method")
            sorted_indices0 = np.argsort(self.data.t_samples[0].flatten())
            sorted_t = np.array(self.data.t_samples[0].flatten())[sorted_indices0]
            self.observe_t = sorted_t
            self.obs_X = np.zeros((len(sorted_t), len(self.data.states_data_noisy)))
            self.obs_X[:, 0] = np.array(self.data.states_data_noisy[0])[sorted_indices0]
            for i in range(1, len(self.data.states_data_noisy)):
                self.obs_X[:, i] = np.array(self.data.states_data_noisy[i])[[
                    list(self.data.t_samples[i].flatten()).index(t) for t in sorted_t]]
        else:
            print("We will use the t_samples for each state for the minimize method")
            self.observe_t = [t_obs.reshape((len(t_obs))) for t_obs in self.data.t_samples]
            self.obs_X = self.data.states_data_noisy
        
    # ----------------- Methods with "minimize" -----------------
    def model_derivative_minimize(self, t, state, *params):
        dict_params = dict(zip(self.lst_names_fitted_params, params))
        full_params = {**dict_params, **self.fixed_params}
        return self.model_derivatives(state, t, full_params)

    def integrate_ode_minimize(self, val_params, X_ini, t_samp):
        sol = solve_ivp(self.model_derivative_minimize, [t_samp[0], t_samp[-1]], X_ini, 
                        t_eval=t_samp, args=tuple(val_params))
        return sol.y

    def loss_function_minimize(self, val_params, X_ini, t_samples, data_X):
        model_X = [self.integrate_ode_minimize(val_params, X_ini, t_samples[i])[i] \
                   for i in range(len(t_samples))]
        errors_X = [data_X[i] - model_X[i] for i in range(len(t_samples))]
        total_error = np.sum([np.sum(err**2) for err in errors_X])  # Sum of squared errors
        return total_error

    # ----------------- Methods with "curve_fit" -----------------
    def solver_model(self, state, t, params):
        dict_params = dict(zip(self.lst_names_fitted_params, params))
        full_params = {**dict_params, **self.fixed_params}
        return odeint(self.model_derivatives, state, t, args=(full_params,), atol=1e-8, rtol=1e-11)

    def integration_model(self, state_arr, *params):
        params_mod = list(params[:-len(self.data.states_data_noisy)])
        state_0 = list(params[-len(self.data.states_data_noisy):])
        res0 = self.solver_model(state_0, self.observe_t, params_mod)
        res1 = [self.solver_model(state, [t,t+1], params_mod)[-1] for t, state in enumerate(state_arr[:-1]) ]
        return np.concatenate([res0, res1]).flatten()

    # ----------------- Common functions predict -----------------
    def predict(self, npoint=500, params0={'k':1}, X_0=[2], fixed_params={}):
        self.dict_init_fitted_params = params0
        self.time_full_train_in_sec = 0
        start_time = time.time()
        
        self.lst_names_fitted_params = list(params0.keys())
        self.fixed_params = fixed_params
        
        if self.t_sample_common:
            print("Prediction with curve_fit method (common t_samples)")
            p0 = np.concatenate((list(params0.values()), X_0))

            data1_for_fit = self.obs_X
            data2_for_fit = np.concatenate([self.obs_X, self.obs_X[1:]]).flatten()

            params_fit, info_fit = curve_fit(self.integration_model, data1_for_fit, data2_for_fit, p0=p0)

            t_rebuild = np.linspace(np.min(self.data.t_samples), np.max(self.data.t_samples), npoint)
            state0_rebuild = self.obs_X[np.argmin(self.data.t_samples)]
            state_rebuild = self.solver_model(state0_rebuild, t_rebuild, params_fit[:-len(X_0)])
        else:
            print("Prediction with minimize method (different t_samples)")
            data_X = self.data.states_data_noisy
            t_samples = [t_samp.reshape(-1) for t_samp in self.data.t_samples]
            results = minimize(self.loss_function_minimize, list(params0.values()), 
                               args=(X_0, t_samples, data_X))
            params_fit = results.x
            
            t_rebuild = np.linspace(min([np.min(t_samp) for t_samp in self.data.t_samples]), 
                                    max([np.max(t_samp) for t_samp in self.data.t_samples]), 
                                    npoint)
            state0_rebuild = [self.data.states_data_noisy[i][np.argmin(self.data.t_samples[i])] \
                                for i in range(len(self.data.states_data_noisy))]
            state_rebuild = self.integrate_ode_minimize(params_fit, state0_rebuild, t_rebuild)

        self.t_fit, self.state_fit = t_rebuild, state_rebuild
        self.params_fit = params_fit
        
        end_time = time.time()
        self.time_full_train_in_sec = end_time - start_time
            
        return t_rebuild, state_rebuild
        
    def performance(self, bounds_error_true=None, n_pts_simu=10000, file_save_params = None):
        if bounds_error_true == None:
            bounds_error_true = [min(self.t_fit), max(self.t_fit)]
        
        # ¨Fixed parameters
        true_params_fixed = self.fixed_params
        # True fitted parameters
        true_params_fit = {k: v for k, v in self.data.params.items() if k in self.lst_names_fitted_params}
        # Final fitted parameters
        final_params_fit = {k: v for k, v in zip(self.lst_names_fitted_params, self.params_fit)}
        # Time for training
        # self.time_full_train_in_sec

        # * * * * Errors * * * *
        tot_n_data = sum([len(self.data.states_data_noisy[k]) for k in range(len(self.data.states_data_noisy))])
        t_simu = np.linspace(bounds_error_true[0], bounds_error_true[1], n_pts_simu)
        # Simulate the model from the NN parameters
        dict_params = dict(zip(self.lst_names_fitted_params, self.params_fit))
        all_params = {**dict_params, **self.fixed_params}
        simu_X_pred_modelFitparams = odeint(self.data.model, self.data.init_state, t_simu, args=(all_params,), atol=1e-8, rtol=1e-11)
        interp_func_X_pred_modelFitparams = interp1d(t_simu, simu_X_pred_modelFitparams.T, kind='cubic')
        # Simulate the true model
        X_simu_true = odeint(self.data.model, self.data.init_state, t_simu, args=(self.data.params,), atol=1e-8, rtol=1e-11)
        
        # > MSE data - prediction model with fitted parameters
        MSE_data_modelFitparams = 0
        for k in range(len(self.data.states_data_noisy)):
            t_this_state_long = self.data.t_samples[k][:,0]
            Xk_pred_modelFitparams = interp_func_X_pred_modelFitparams(t_this_state_long).T[:,k]
            MSE_data_modelFitparams += sum((Xk_pred_modelFitparams - self.data.states_data_noisy[k])**2)
        MSE_data_modelFitparams = MSE_data_modelFitparams/tot_n_data
        
        # > MSE true model - prediction model with fitted parameters
        tot_n_simu = n_pts_simu * len(self.data.states_data_noisy)
        MSE_true_modelFitparams = 0
        X_pred_modelFitparams = interp_func_X_pred_modelFitparams(t_simu).T
        for k in range(len(self.data.states_data_noisy)):
            MSE_true_modelFitparams += np.sum((X_pred_modelFitparams[:,k] - X_simu_true[:,k])**2)
        MSE_true_modelFitparams = MSE_true_modelFitparams/tot_n_simu
        
        # self.dict_perf = {"True parameters fixed:": true_params_fixed,
        #                   "True parameters fitted:": true_params_fit,
        #                   "Final fitted parameters:": final_params_fit,
        self.dict_perf = {"Fixed Parameters:": true_params_fixed,
                          "Aim:": true_params_fit,
                          "Init guess:": self.dict_init_fitted_params,
                          "Guess:": final_params_fit,
                          "Computation time (in sec):": self.time_full_train_in_sec,
                          "MSE data-prediction scipy:": MSE_data_modelFitparams,
                          "MSE true model-prediction scipy:": MSE_true_modelFitparams}
        
        if file_save_params != None:
            true_parameters = {**self.dict_perf["Aim:"], **self.dict_perf["Fixed Parameters:"]}
            initial_guess = self.dict_perf["Init guess:"]
            final_parameters = self.dict_perf["Guess:"]
            all_parameters = set(true_parameters.keys()) | set(initial_guess.keys()) | set(final_parameters.keys())
            data = []
            for param_name in all_parameters:
                row = {"Parameter": param_name,
                       "True value": true_parameters.get(param_name, ""),
                       "Initial value": initial_guess.get(param_name, ""),
                       "Final value": final_parameters.get(param_name, "")}
                data.append(row)
            with open(file_save_params, "w", newline="") as csvfile:
                fieldnames = ["Parameter", "True value", "Initial value", "Final value"]
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                writer.writerows(data)
            print(f"CSV file '{file_save_params}' has been created successfully.")

        
    def basic_plot(self, elements_to_plot={"simulation":[0,1], "data":[0,1], "prediction":[0,1]}, file_save_fig=None):
        if np.all(self.t_fit)==None and np.all(self.state_fit)==None:
            print("You need to train the model first")
            return None
        default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']

        plt.figure(figsize=(7, 4))
        if "simulation" in elements_to_plot.keys():
            t_sim, X_sim = self.data.t_simu_all, self.data.states_simu_all
            for k in elements_to_plot["simulation"]:
                plt.plot(t_sim, X_sim[:, k], '--', alpha=.5, 
                         color=default_col_pal[k%len(default_col_pal)], label=self.data.labels_states[k]+' (simulation)')
        if "data" in elements_to_plot.keys():
            for k in elements_to_plot["data"]:
                t_scatter, Xk_scatter = self.data.t_samples[k][:,0], self.data.states_data_noisy[k]
                plt.scatter(t_scatter, Xk_scatter, 
                            color=default_col_pal[k%len(default_col_pal)], label=self.data.labels_states[k]+' (data)')
        if "prediction" in elements_to_plot.keys():
            if self.t_sample_common:
                t_pred, X_pred = self.t_fit, self.state_fit
                for k in elements_to_plot["prediction"]:
                    plt.plot(t_pred, X_pred[:, k], color=default_col_pal[k%len(default_col_pal)], 
                            label=self.data.labels_states[k]+' (scipy)')
            else:
                t_pred, X_pred = self.t_fit, self.state_fit
                for k in elements_to_plot["prediction"]:
                    plt.plot(t_pred, X_pred[k], color=default_col_pal[k%len(default_col_pal)], 
                            label=self.data.labels_states[k]+' (scipy)')
        plt.legend()
        plt.title("Prediction from scipy (numerical method)")
        if file_save_fig != None:
            plt.savefig(file_save_fig, format='jpg')
    


if __name__ == "__main__":
    from models_catalogue import exp_decay_model_derivatives, lv_model_Npop_derivatives, lv_model_2pop_derivatives
    from models_catalogue import eco_to_dimless_2pop, params_6pop_dimless
    import data_generator as dg
    import sample_plan as sp

    import matplotlib.pyplot as plt



    print("\n\n\n- - - Test the Model Exponential decay - - -")
    
    param = {"k": 0.5}
    t_max, x_ini = 10, 2
    sample_plan_exp2 = sp.sample_plan(tmax=t_max, nbpoint=60, plan_type="random")
    datagen_exp2 = dg.data_generator(exp_decay_model_derivatives, param, t_max, [x_ini], 
                                    labels_states=["Deacy"], model_name="Exponential decay")
    datagen_exp2.get_data(plan=sample_plan_exp2, noise_intensity=0.1, noise_seed=None, plot_data=True, 
                        permissible_range=[-np.inf, np.inf], default_value=0)

    scipy_solver_exp_1 = scipy_fitting(exp_decay_model_derivatives, datagen_exp2)
    scipy_solver_exp_1.get_train_data(new_gen_data=False, permissible_range=[-np.inf, np.inf], default_value=0)

    t_rebuild, state_rebuild = scipy_solver_exp_1.predict(params0=param, X_0=[x_ini], fixed_params={})

    plt.plot(t_rebuild, state_rebuild)



    print("\n\n\n- - - Test the Model LV 2 populations - - -")

    Tref = 10
    eco_params_2pop = {"a1": 0.088, "a2": 0.020, "e": 1, "c": 0.96, "K": 0.034, "S": 0.12, "xR1": 0.12, "xR2": 0.91, 
                    "b":1/0.12, "Tref": Tref, "func_resp": "holling1", "a": 0.086} # b = e/S
    # eco_params_2pop = {"a1": 0.088, "a2": 0.020, "e": 1, "c": 0.96, "K": 0.034, "S": 0.12, "xR1": 0.12, "xR2": 0.91, 
    #                 "b":1/0.12, "Tref": Tref, "func_resp": "holling2", "a": 0.13}  # b = e/S
    # eco_params_2pop = {"a1": 0.088, "a2": 0.020, "e": 1, "c": 0.96, "K": 0.034, "S": 0.12, "xR1": 0.12, "xR2": 0.91, 
    #                 "b":1/0.12, "Tref": Tref, "func_resp": "holling3", "a": 1.6}   # b = e/S
    dimless_params_2pop = eco_to_dimless_2pop(eco_params_2pop, func_resp=eco_params_2pop["func_resp"])
    dimless_params_2pop["func_resp"], dimless_params_2pop["Tref"] = eco_params_2pop["func_resp"], Tref

    t_max = 100
    X_ini = [5, 2] # Predator, Prey

    sample_plan_LV2_1 = sp.sample_plan(tmax=t_max, nbpoint=30, plan_type="batch_random", batch_params={"nbr_batch": 3, "ratio_in_out_batch": 1/2})

    datagen_LV2_1 = dg.data_generator(lv_model_2pop_derivatives, dimless_params_2pop, t_max, X_ini, labels_states=["Predator", "Prey"], 
                                    model_name=f"Lotka-Voltera 2 populations ({dimless_params_2pop['func_resp']})")
    datagen_LV2_1.get_data(plan=sample_plan_LV2_1, noise_intensity=0.1, noise_seed=None, plot_data=True, 
                           permissible_range=[0, np.inf], default_value=0)

    scipy_solver_LV2_1 = scipy_fitting(lv_model_2pop_derivatives, datagen_LV2_1)
    scipy_solver_LV2_1.get_train_data(new_gen_data=False, permissible_range=[0, np.inf], default_value=0)

    fitted_params = ['kappa_tild']
    dimless_params_2pop_fitted = {key: value * (1+np.random.normal(0, 0.1)) \
                                  for key, value in dimless_params_2pop.items() if key in fitted_params}
    dimless_params_2pop_fixed = {key: value for key, value in dimless_params_2pop.items() if key not in fitted_params}
    t_rebuild, state_rebuild = scipy_solver_LV2_1.predict(params0=dimless_params_2pop_fitted, X_0=X_ini, fixed_params=dimless_params_2pop_fixed)

    plt.plot(t_rebuild, state_rebuild)
    
    plt.show()