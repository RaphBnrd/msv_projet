"""
This module contains the class data_generator which is used to generate data from a model
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.interpolate import interp1d
from utils.sample_plan import sample_plan


class data_generator:

    def __init__(self, model, params, t_max, init_state, 
                 labels_states=["Predator", "Prey"], model_name="Lotka-Volterra"):
        """
        model : function        - function which returns the derivative of the state vector
        params : dict           - dictionary containing the all the parameters of the model
        t_max : float           - maximum time of the simulation
        init_state : array      - array containing the initial values of the state vector
        labels_states : list    - list of the labels of the state vector
        model_name : str        - name of the model
        """
        self.model = model
        self.params = params
        self.t_max = t_max
        self.init_state = init_state
        self.labels_states = labels_states
        self.model_name = model_name
        # Simulation for a very fine time grid
        self.t_simu_all = None
        self.states_simu_all = None
        # Data for the sampling plan (with or without noise)
        self.t_data = None
        self.states_data_true = None
        self.states_data_noisy = None

    def simulate(self, nb_p_t=15000):
        """ Simulate the model for a very fine time grid """
        self.t_simu_all = np.linspace(0, self.t_max, nb_p_t)
        self.states_simu_all = odeint(self.model, self.init_state, self.t_simu_all, 
                                      args=(self.params,), atol=1e-8, rtol=1e-11)

    def get_data(self, plan=None, nbpoint_if_no_plan=10, t_start_all_sample=None,
                 noise_intensity=0, noise_seed=None, plot_data=False, 
                 permissible_range=[-np.inf, np.inf], default_value=0, nb_p_t_simu=15000):
        """
        Generate the data according to the sampling plan
        plan : sample_plan or list  - sampling plan or a list of sampling plans of lenght equal to the number of states
        nbpoint_if_no_plan : int    - number of points of the sampling plan if plan is None
        t_start_all_sample : float  - common start time of all the samples that will be generated
        noise_intensity : float     - intensity of the noise
        noise_seed : int            - seed for the random noise
        plot_data : bool            - if True, plot the data
        permissible_range : list    - list of the bounds of the permissible range for the data
        default_value : float       - default value for the data outside the permissible range
        """
        if plan == None:
            plan = sample_plan(self.t_max, nbpoint_if_no_plan, "uniform")
        if isinstance(plan, list):
            if len(plan) != len(self.init_state):
                raise ValueError("The number of sampling plans must be equal to the number of states")
            t_samples = [each_plan.t_samples for each_plan in plan]
        else:
            t_samples = [plan.t_samples for _ in range(len(self.init_state))]
        # Modify the sampling plan to have a common start time
        if t_start_all_sample == None:
            t_start_all_sample = min([min(t_samp) for t_samp in t_samples])
        clean_t_samples = []
        for k in range(len(t_samples)):
            t_samp = t_samples[k]
            if t_start_all_sample not in t_samp:
                print("Warning: we add the common start time " + str(t_start_all_sample) + " to the sampling plan n°"+str(k+1))
            clean_t_samples.append(
                np.sort(np.unique(
                    [t_start_all_sample] + [t_samp[i] for i in range(len(t_samp)) if t_samp[i] > t_start_all_sample]
                ))
            )
        t_samples = clean_t_samples

        full_t_samples = np.unique(np.concatenate(t_samples))
        self.t_max = max([np.max(t_samp) for t_samp in t_samples] + [self.t_max])
        if np.all(self.states_simu_all) == None:
            self.simulate(nb_p_t=nb_p_t_simu)
        np.random.seed(seed = noise_seed)
        interp_func = interp1d(self.t_simu_all, self.states_simu_all.T, kind='cubic')
        full_states_data_true = interp_func(full_t_samples).T
        idx_t_samples_in_full = [[np.where(full_t_samples == t_i[k])[0][0] \
                                   for k in range(len(t_i))] for t_i in t_samples]
        self.states_data_true = []
        for i in range(full_states_data_true.shape[1]):
            self.states_data_true.append(full_states_data_true[idx_t_samples_in_full[i], i])
        lst_mu = [0 for _ in range(self.states_simu_all.shape[1])]
        lst_sigma = [1 for _ in range(self.states_simu_all.shape[1])]
        # Add Gaussian noise to each column independently
        self.states_data_noisy = [
            self.states_data_true[i] * 1 + (noise_intensity * \
                np.random.normal(lst_mu[i], lst_sigma[i], len(self.states_data_true[i])))
            for i in range(len(self.states_data_true)) ]
        # Set the values outside the permissible range to a default value
        masks = [np.logical_and(permissible_range[0] <= self.states_data_noisy[i], 
                                self.states_data_noisy[i] <= permissible_range[1]) \
                 for i in range(len(self.states_data_noisy))]
        for i in range(len(self.states_data_noisy)):
            self.states_data_noisy[i][~masks[i]] = default_value
        
        self.t_samples = [np.array(t_samp).reshape((len(t_samp),1)) for t_samp in t_samples]

        if plot_data:
            self.plot_initial_data()


    def plot_initial_data(self, elements_to_plot=["simulation", "data"], title=None):
        if np.all(self.states_simu_all) == None:
            self.simulate()
        
        default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']
        for i in range(self.states_simu_all.shape[1]):
            if "simulation" in elements_to_plot:
                plt.plot(self.t_simu_all, self.states_simu_all[:, i], '--', alpha=.5,
                        color=default_col_pal[i%len(default_col_pal)], label=self.labels_states[i]+" (model)")
            if "data" in elements_to_plot:
                plt.scatter(self.t_samples[i], self.states_data_noisy[i],
                            color=default_col_pal[i%len(default_col_pal)], label=self.labels_states[i]+" (data)")
        plt.legend()
        # plt.ylim(bottom=0)
        plt.xlabel("Time")
        if title == None:
            title = self.model_name+" - Initial data"
        plt.title(title)
