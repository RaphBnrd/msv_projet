

import numpy as np
import deepxde as dde
import datetime
import time
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import re
import math
from scipy.integrate import odeint
from scipy.interpolate import interp1d
import tensorflow as tf
import pickle
import csv


class pinn:

    def __init__(self, model_derivatives=None, data=None, dict_fitted_params=None, dict_fixed_params=None, 
                 positive_params=[], rd_seed=None, t_max_geom=None, file_import_pinn_data=None):
        """
        model_derivatives : function    - function which returns the derivative of the state vector
        data : data_generator           - data generator object containing the data
        dict_fitted_params : dict       - dictionary containing the fitted parameters with there initial value (keys = parameter names)
        dict_fixed_params : dict        - dictionary containing the fixed parameters (keys = parameter names)
        """
        if file_import_pinn_data == None:
            np.random.seed(seed=rd_seed)
            self.model_derivatives = model_derivatives
            self.data = data
            self.dict_fitted_params = dict_fitted_params
            self.dict_init_fitted_params = dict_fitted_params
            self.dict_fixed_params = dict_fixed_params
            self.dde_vars = {}
            for fitted_param in self.dict_fitted_params.keys():
                if fitted_param in positive_params:
                    self.dde_vars[fitted_param] = tf.math.softplus(
                        dde.Variable(0.5, dtype=tf.float32) ) * self.dict_fitted_params[fitted_param]
                else:
                    self.dde_vars[fitted_param] = dde.Variable(self.dict_fitted_params[fitted_param], dtype=tf.float32)
            self.train_param = [self.dde_vars[fitted_param] for fitted_param in self.dict_fitted_params.keys()]

            if t_max_geom == None:
                self.t_max_geom = max([np.max(t_samp) for t_samp in self.data.t_samples])
            else:
                self.t_max_geom = t_max_geom
            if len(np.unique([np.min(t_samp) for t_samp in self.data.t_samples])) > 1:
                print(np.unique([np.min(t_samp) for t_samp in self.data.t_samples]))
                raise ValueError("The initial time of the data must be the same for all the states")
            self.t_min_geom = min([np.min(t_samp) for t_samp in self.data.t_samples])
            self.geom = dde.geometry.TimeDomain(self.t_min_geom, self.t_max_geom)
            self.X_init = [self.data.states_data_noisy[i][np.argmin(self.data.t_samples[i])] \
                        for i in range(len(self.data.states_data_noisy))]
            self.ic = [dde.IC(self.geom, (lambda val: lambda X: val)(self.X_init[i]), self.boundary, component=i) \
                        for i in range(len(self.X_init))]
            
            self.trained = False
            self.time_full_train_in_sec = None
            self.time_step_train_in_sec = None
            self.dict_perf = {}
        else:
            with open(file_import_pinn_data, 'rb') as f:
                pickle_data = pickle.load(f)
            for attr, value in pickle_data.items():
                setattr(self, attr, value)
            # tensorflow_attr = ['nn_model']
            # not_possible_to_save_attr = ['dde_vars', 'train_param', 'ic', 
            #                              'observe_X', 'traindata', 'variable', ]
            self.dde_vars = {}
            for fitted_param in self.dict_fitted_params.keys():
                if fitted_param in positive_params:
                    self.dde_vars[fitted_param] = tf.math.softplus(
                        dde.Variable(0.5, dtype=tf.float32) ) * self.dict_fitted_params[fitted_param]
                else:
                    self.dde_vars[fitted_param] = dde.Variable(self.dict_fitted_params[fitted_param], dtype=tf.float32)
            self.train_param = [self.dde_vars[fitted_param] for fitted_param in self.dict_fitted_params.keys()]
            self.ic = [dde.IC(self.geom, (lambda val: lambda X: val)(self.X_init[i]), self.boundary, component=i) \
                        for i in range(len(self.X_init))]
        self.func_feature_transform = None
    
    def set_feature_transform(self, size_out=10, type_transform="sin"):
        print("Feature transform added with "+type_transform+" type and "+str(size_out)+" features.")
        if type_transform == "sin":
            def feature_transform(t):
                time_window = self.t_max_geom - self.t_min_geom
                return tf.concat(
                    [t] + 
                    [tf.sin(t * 2*np.pi*k / time_window) for k in range(1, size_out+1)],
                    axis=1,
                )
            self.func_feature_transform = feature_transform
        else:
            raise ValueError("The type of feature transform isn't valid")


        
    
    def save_possible_attr(self, filename_save="save_data_pinn.pkl"):
        pickle_friendly_attr = ['X_init', 'data', 'dict_fitted_params', 'dict_fixed_params', 'dict_perf', 
                                'file_var_export', 'geom', 'losshistory', 'model_derivatives', 'niter_weights', 
                                'obs_X', 'observe_t', 't_max_geom', 't_min_geom', 'time_full_train_in_sec', 
                                'time_step_train_in_sec', 'train_state', 'trained', 'nnlayers', 
                                'activation_function', 'initcond', 'lr']
        # tensorflow_attr = ['nn_model']
        # not_possible_to_save_attr = ['ic', 'observe_X', 'train_param', 'traindata', 'variable', 'dde_vars']
        pickle_data_to_save = {}
        for attr in pickle_friendly_attr:
            pickle_data_to_save[attr] = getattr(self, attr, None)
        with open(filename_save, 'wb') as f:
            pickle.dump(pickle_data_to_save, f)

    def evolve_system(self, t, X):
        dX_dt_dde = [dde.grad.jacobian(X, t, i=k) for k in range(len(self.data.states_data_noisy))]
        all_params = {**self.dde_vars, **self.dict_fixed_params}
        dX_dt_model = self.model_derivatives([X[:,k:(k+1)] for k in range(len(self.data.states_data_noisy))], t, all_params)
        return [dX_dt_dde[k] - dX_dt_model[k] for k in range(len(self.data.states_data_noisy))]

    def boundary(self, _, on_initial):
        return on_initial

    def get_train_data(self, new_gen_data=False, 
                       plan=None, nbpoint_if_no_plan=10, noise_intensity=0, noise_seed=None,
                       permissible_range=[-np.inf, np.inf], default_value=0):
        if new_gen_data or self.data.states_data_noisy==None:
            if plan == None:
                self.data.get_data(self, plan=plan, nbpoint_if_no_plan=nbpoint_if_no_plan, 
                                   noise_intensity=noise_intensity, noise_seed=noise_seed, plot_data=False, 
                                   permissible_range=permissible_range, default_value=default_value)
        self.observe_t, self.obs_X = self.data.t_samples, self.data.states_data_noisy
        self.observe_X = [dde.icbc.PointSetBC(self.observe_t[k], self.obs_X[k].reshape(-1, 1), component=k) \
                            for k in range(len(self.obs_X))]
        self.traindata = dde.data.PDE(
            self.geom,
            self.evolve_system,
            self.ic + self.observe_X,
            # num_domain = round((self.t_max_geom - self.t_min_geom)/10),
            num_domain = 1000,
            num_boundary = len(self.observe_X),
            anchors = np.unique(np.concatenate(self.observe_t)).reshape(-1, 1),
        )

    def compile_train_model(self, lr=1e-3, nnlayers = [1] + [128] * 3 + [7], activation_function="swish", initcond="Glorot normal", 
                      weights=None, niter_weights=[1000,100000], metrics=[], file_var_export='train_params',
                      size_feature_transform=10, type_transform="sin"):
        self.niter_weights = niter_weights
        self.nnlayers = nnlayers
        self.activation_function = activation_function
        self.initcond = initcond
        self.lr=lr
        track_train_every = max(1, min(max(niter_weights)//250, min(niter_weights)//5))
        net = dde.nn.FNN(nnlayers, activation_function, initcond)
        self.set_feature_transform(size_out=size_feature_transform, type_transform=type_transform)
        net.apply_feature_transform(self.func_feature_transform)
        self.nn_model = dde.Model(self.traindata, net)
        # Path for the variable file exported
        self.file_var_export = file_var_export
        # if self.file_var_export != None:
        #     formatted_datetime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        #     self.file_var_export = formatted_datetime + "-" + self.file_var_export + ".dat"
        self.variable = dde.callbacks.VariableValue(
            self.train_param, period=track_train_every, filename=self.file_var_export
        )
        if weights == None:
            weights = [None for _ in range(len(niter_weights))]
        print("- - - - Train the PINN - - - -")
        print("Fitted parameters: ", " ; ".join(list(self.dict_fitted_params.keys())))
        print("Fixed parameters: ", " ; ".join(list(self.dict_fixed_params.keys())))
        print("Learning rate: ", lr)
        print("Architecture: ", nnlayers, "\n\n")

        self.time_full_train_in_sec = 0
        self.time_step_train_in_sec = []
        for i in range(len(niter_weights)):
            print("* * * * Part "+str(i+1)+" of the training * * * *")
            start_time_step = time.time()
            self.nn_model.compile("adam", lr=lr, loss_weights=weights[i], metrics=metrics, external_trainable_variables=self.train_param)
            if i != len(niter_weights)-1:
                self.nn_model.train(iterations=niter_weights[i], callbacks=[self.variable], display_every=track_train_every)
            else:
                self.losshistory, self.train_state = self.nn_model.train(iterations=niter_weights[i], callbacks=[self.variable], display_every=track_train_every)
            end_time_step = time.time()
            self.time_step_train_in_sec.append(end_time_step - start_time_step)
            self.time_full_train_in_sec += self.time_step_train_in_sec[-1]
            self.dict_perf["AfterStep"+str(i+1)] = self.performance()

        print("The total training took " + str(int(self.time_full_train_in_sec//60)) + \
              " min " + str(round(self.time_full_train_in_sec%60, 2))+" sec")
        self.trained = True

    def predict(self, npoint=1000):
        t = self.geom.uniform_points(npoint)
        X = self.nn_model.predict(t)
        return t, X
    
    def performance(self, bounds_error_true=None, n_pts_simu=10000, file_save_params=None):
        if bounds_error_true == None:
            bounds_error_true = [self.t_min_geom, self.t_max_geom]
        
        # ¨Fixed parameters
        true_params_fixed = self.dict_fixed_params
        # True fitted parameters
        true_params_fit = {k: v for k, v in self.data.params.items() if k in self.dict_fitted_params.keys()}
        # Initial fitted parameters
        # self.dict_init_fitted_params
        # Final fitted parameters
        with open(self.file_var_export, 'r') as file:
            last_line = file.readlines()[-1]
            last_param_hist_str = re.search(r'\[(.*?)\]', last_line).group(1)
            last_param_hist = [float(value) for value in last_param_hist_str.split(', ')]
        final_params_fit = {k: v for k, v in zip(list(self.dict_fitted_params.keys()), last_param_hist)}
        # Time for training
        # self.time_full_train_in_sec

        # * * * * Errors * * * *
        tot_n_data = sum([len(self.data.states_data_noisy[k]) for k in range(len(self.data.states_data_noisy))])
        t_simu = np.linspace(bounds_error_true[0], bounds_error_true[1], n_pts_simu)
        # Simulate the model from the NN parameters
        all_params = {**final_params_fit, **self.dict_fixed_params}
        simu_X_pred_modelNNparams = odeint(self.data.model, self.data.init_state, t_simu, args=(all_params,), atol=1e-8, rtol=1e-11)
        interp_func_X_pred_modelNNparams = interp1d(t_simu, simu_X_pred_modelNNparams.T, kind='cubic')
        # Simulate the true model
        X_simu_true = odeint(self.data.model, self.data.init_state, t_simu, args=(self.data.params,), atol=1e-8, rtol=1e-11)
        
        # > MSE data - prediction NN
        MSE_data_NNpred = 0
        for k in range(len(self.data.states_data_noisy)):
            t_this_state = self.data.t_samples[k]
            Xk_pred_NN = self.nn_model.predict(t_this_state)[:,k]
            MSE_data_NNpred += sum((Xk_pred_NN - self.data.states_data_noisy[k])**2)
        MSE_data_NNpred = MSE_data_NNpred/tot_n_data
        
        # > MSE data - prediction model with fitted parameters
        MSE_data_modelNNparams = 0
        for k in range(len(self.data.states_data_noisy)):
            t_this_state_long = self.data.t_samples[k][:,0]
            Xk_pred_modelNNparams = interp_func_X_pred_modelNNparams(t_this_state_long).T[:,k]
            MSE_data_modelNNparams += sum((Xk_pred_modelNNparams - self.data.states_data_noisy[k])**2)
        MSE_data_modelNNparams = MSE_data_modelNNparams/tot_n_data
        
        # > MSE true model - prediction NN
        tot_n_simu = n_pts_simu * len(self.data.states_data_noisy)
        MSE_true_NNpred = 0
        X_pred_NN = self.nn_model.predict(t_simu.reshape(-1, 1))
        for k in range(len(self.data.states_data_noisy)):
            MSE_true_NNpred += np.sum((X_pred_NN[:,k] - X_simu_true[:,k])**2)
        MSE_true_NNpred = MSE_true_NNpred/tot_n_simu
        
        # > MSE true model - prediction model with fitted parameters
        MSE_true_modelNNparams = 0
        X_pred_modelNNparams = interp_func_X_pred_modelNNparams(t_simu).T
        for k in range(len(self.data.states_data_noisy)):
            MSE_true_modelNNparams += np.sum((X_pred_modelNNparams[:,k] - X_simu_true[:,k])**2)
        MSE_true_modelNNparams = MSE_true_modelNNparams/tot_n_simu
        
        self.dict_perf = {"True parameters fixed:": true_params_fixed,
                          "True parameters fitted:": true_params_fit,
                          "Initial fitted parameters:": self.dict_init_fitted_params,
                          "Final fitted parameters:": final_params_fit,
                          "Training time (in sec):": self.time_full_train_in_sec,
                          "MSE data-prediction NN:": MSE_data_NNpred,
                          "MSE data-prediction model with fitted parameters:": MSE_data_modelNNparams,
                          "MSE true model-prediction NN:": MSE_true_NNpred,
                          "MSE true model-prediction model with fitted parameters:": MSE_true_modelNNparams}
        
        if file_save_params != None:
            true_parameters = {**self.dict_perf["True parameters fixed:"], **self.dict_perf["True parameters fitted:"]}
            initial_guess = self.dict_perf["Initial fitted parameters:"]
            final_parameters = self.dict_perf["Final fitted parameters:"]
            all_parameters = set(true_parameters.keys()) | set(initial_guess.keys()) | set(final_parameters.keys())
            data = []
            for param_name in all_parameters:
                row = {"Parameter": param_name,
                       "True value": true_parameters.get(param_name, ""),
                       "Initial value": initial_guess.get(param_name, ""),
                       "Final value": final_parameters.get(param_name, "")}
                data.append(row)
            with open(file_save_params, "w", newline="") as csvfile:
                fieldnames = ["Parameter", "True value", "Initial value", "Final value"]
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
                writer.writeheader()
                writer.writerows(data)
            print(f"CSV file '{file_save_params}' has been created successfully.")

        return self.dict_perf
        
    def basic_plot(self, elements_to_plot={"simulation":[0,1], "data":[0,1], "prediction":[0,1]}, file_save_fig=None):
        if self.trained == False:
            print("You need to train the model first")
            return None
        default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']
        
        plt.figure(figsize=(7, 4))
        if "simulation" in elements_to_plot.keys():
            t_sim, X_sim = self.data.t_simu_all, self.data.states_simu_all
            for k in elements_to_plot["simulation"]:
                plt.plot(t_sim, X_sim[:, k], '--', alpha=.5, 
                         color=default_col_pal[k%len(default_col_pal)], label='Simulation')
        if "data" in elements_to_plot.keys():
            for k in elements_to_plot["data"]:
                t_scatter, Xk_scatter = self.data.t_samples[k][:,0], self.data.states_data_noisy[k]
                plt.scatter(t_scatter, Xk_scatter, 
                            color=default_col_pal[k%len(default_col_pal)], label='Data')
        if "prediction" in elements_to_plot.keys():
            t_pred, X_pred = self.predict()
            for k in elements_to_plot["prediction"]:
                plt.plot(t_pred, X_pred[:, k], color=default_col_pal[k%len(default_col_pal)], 
                         label='PINN prediction')
        plt.ylabel('State')
        plt.xlabel('Step')
        plt.legend()
        plt.title("Prediction directly from the PINN")
        if file_save_fig != None:
            plt.savefig(file_save_fig, format='jpg')
    
    def plot_convergence(self, file_save_fig=None): 
        default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']
        if len(default_col_pal) > len(self.X_init):
            default_col_pal = default_col_pal[len(self.X_init):]

        steps_hist, param_hist = [], []
        with open(self.file_var_export, 'r') as file:
            for line in file:
                steps_hist.append(int(line.split(' ')[0]))
                param_hist.append( [float(value) for value in
                                    re.search(r'\[(.*?)\]', line).group(1).split(', ')] )
            steps_hist, param_hist = np.array(steps_hist), np.array(param_hist)
        
        if param_hist.shape[1] > 1:
            n_cols = math.ceil(math.sqrt(param_hist.shape[1]))
            n_rows = math.ceil(param_hist.shape[1] / n_cols)
            fig, axs = plt.subplots(n_rows, n_cols, figsize=(min(n_cols*4, 24), min(n_rows*4, 24)))
            fig.suptitle('Convergence of the parameters')
            for i, ax in enumerate(axs.flat):
                ax.plot(steps_hist, param_hist[:, i], label='PINN fitted value',
                        color=default_col_pal[i%len(default_col_pal)])
                ax.axhline(y=self.data.params[list(self.dict_fitted_params.keys())[i]], 
                           color=default_col_pal[i%len(default_col_pal)], linestyle='--', label='True value')
                for l in range(len(self.niter_weights)-1):
                    ax.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
                ax.set_ylabel('Estimation')
                ax.set_xlabel('Step')
                ax.set_title(list(self.dict_fitted_params.keys())[i])
                ax.legend()
        else:
            plt.figure(figsize=(6, 4))
            plt.plot(steps_hist, param_hist, label='PINN fitted value',
                     color=default_col_pal[0])
            plt.axhline(y=self.data.params[list(self.dict_fitted_params.keys())[0]], 
                        color=default_col_pal[0], linestyle='--', label='True value')
            for l in range(len(self.niter_weights)-1):
                plt.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
            plt.ylabel('Estimation')
            plt.xlabel('Step')
            plt.title('Convergence of the parameter ' + list(self.dict_fitted_params.keys())[0])
            plt.legend()
        plt.tight_layout()

        if file_save_fig != None:
            plt.savefig(file_save_fig, format='jpg')
    

    def plot_predicted_dynamic(self, elements_to_plot={"simulation":[0,1], "data":[0,1], "prediction":[0,1]}, 
                               file_save_fig=None, nb_p_t=10000):
        default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']

        with open(self.file_var_export, 'r') as file:
            last_line = file.readlines()[-1]
            last_param_hist_str = re.search(r'\[(.*?)\]', last_line).group(1)
            last_param_hist = [float(value) for value in last_param_hist_str.split(', ')]
        final_params = {k: v for k, v in zip(list(self.dict_fitted_params.keys()), last_param_hist)}
        all_params = {**final_params, **self.dict_fixed_params}
        t_simu = np.linspace(self.t_min_geom, self.t_max_geom, nb_p_t)
        states_simu = odeint(self.model_derivatives, self.X_init, t_simu, 
                                      args=(all_params,), atol=1e-8, rtol=1e-11)
        plt.figure(figsize=(7, 4))
        for i in range(len(self.X_init)):
            if "simulation" in elements_to_plot.keys():
                plt.plot(self.data.t_simu_all, self.data.states_simu_all[:, i], '--', alpha=.5,
                         color=default_col_pal[i%len(default_col_pal)], label=self.data.labels_states[i]+" (model)")
            if "data" in elements_to_plot.keys():
                plt.scatter(self.data.t_samples[i][:,0], self.data.states_data_noisy[i], 
                            color=default_col_pal[i%len(default_col_pal)], label=self.data.labels_states[i]+" (data)")
            if "prediction" in elements_to_plot.keys():
                plt.plot(t_simu, states_simu[:, i], 
                         color=default_col_pal[i%len(default_col_pal)], label=self.data.labels_states[i]+" (simu PINN)")
        plt.ylabel('State')
        plt.xlabel('Step')
        plt.legend()
        plt.title("Prediction of the model dynamic (PINN)")
        if file_save_fig != None:
            plt.savefig(file_save_fig, format='jpg')


    def plot_loss_history(self, file_save_fig=None, elements_to_plot=["loss_train", "loss_test"], 
                          start_step_idx=2, labels_loss = ["PDE", "Boundary conditions",  "Data"]):
        default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']
        
        if "loss_train" in elements_to_plot:
            n = np.shape(self.losshistory.loss_train)[1]
            if n > 1:
                n_cols = len(labels_loss)
                n_rows = math.ceil(n / n_cols)
                fig, axs = plt.subplots(n_rows, n_cols, figsize=(min(n_cols*4, 24), min(n_rows*4, 24)))
                fig.suptitle("Train loss evolution")
                for k, ax in enumerate(axs.T.flat):
                    ax.plot(self.losshistory.steps[start_step_idx:], 
                                                    np.array(self.losshistory.loss_train[start_step_idx:])[:, k],
                                                    color=default_col_pal[(k%len(self.X_init))%len(default_col_pal)])
                    for l in range(len(self.niter_weights)-1):
                        ax.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
                    ax.set_title(labels_loss[k//len(self.X_init)] + " out" + str(k%len(self.X_init) + 1))
                    ax.set_ylabel('Loss Train')
                    ax.set_xlabel('Step')
                for k in range(n, n_rows * n_cols):
                    ax.axis('off')
                plt.tight_layout()
            else:
                plt.figure(figsize=(7, 4))
                plt.plot(self.losshistory.steps[start_step_idx:], 
                         np.array(self.losshistory.loss_train[start_step_idx:])[:, 0],
                         color=default_col_pal[0])
                for l in range(len(self.niter_weights)-1):
                    plt.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
                plt.title("Train loss evolution - " + labels_loss[0] + " out" + str(1))
                plt.ylabel('Loss Train')
                plt.xlabel('Step')
            if file_save_fig != None:
                this_file_save_fig = file_save_fig[:file_save_fig.rfind('.')] + "_loss_train" + file_save_fig[file_save_fig.rfind('.'):]
                plt.savefig(this_file_save_fig, format='jpg')
        
            plt.figure(figsize=(10, 6))
            total_loss_train = np.sum(self.losshistory.loss_train[start_step_idx:], axis=1)
            plt.plot(self.losshistory.steps[start_step_idx:], total_loss_train)
            for l in range(len(self.niter_weights)-1):
                plt.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
            plt.title("Total train loss evolution")
            plt.ylabel('Total Loss Train')
            plt.xlabel('Step')
            if file_save_fig != None:
                this_file_save_fig = file_save_fig[:file_save_fig.rfind('.')] + "_total_loss_train" + file_save_fig[file_save_fig.rfind('.'):]
                plt.savefig(this_file_save_fig, format='jpg')

        if "loss_test" in elements_to_plot:
            n = np.shape(self.losshistory.loss_test)[1]
            if n > 1:
                n_cols = len(labels_loss)
                n_rows = math.ceil(n / n_cols)
                fig, axs = plt.subplots(n_rows, n_cols, figsize=(min(n_cols*4, 24), min(n_rows*4, 24)))
                fig.suptitle("Test loss evolution")
                for k, ax in enumerate(axs.T.flat):
                    ax.plot(self.losshistory.steps[start_step_idx:], 
                                                    np.array(self.losshistory.loss_test[start_step_idx:])[:, k],
                                                    color=default_col_pal[(k%len(self.X_init))%len(default_col_pal)])
                    for l in range(len(self.niter_weights)-1):
                        ax.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
                    ax.set_title(labels_loss[k//len(self.X_init)] + " out" + str(k%len(self.X_init) + 1))
                    ax.set_ylabel('Loss Test')
                    ax.set_xlabel('Step')
                for k in range(n, n_rows * n_cols):
                    ax.axis('off')
                plt.tight_layout()
            else:
                plt.figure(figsize=(7, 4))
                plt.plot(self.losshistory.steps[start_step_idx:], 
                         np.array(self.losshistory.loss_test[start_step_idx:])[:, 0],
                         color=default_col_pal[0])
                for l in range(len(self.niter_weights)-1):
                    plt.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
                plt.title("Test loss evolution - " + labels_loss[0] + " out" + str(1))
                plt.ylabel('Loss Test')
                plt.xlabel('Step')
            if file_save_fig != None:
                this_file_save_fig = file_save_fig[:file_save_fig.rfind('.')] + "_loss_test" + file_save_fig[file_save_fig.rfind('.'):]
                plt.savefig(this_file_save_fig, format='jpg')
            
            plt.figure(figsize=(10, 6))
            total_loss_test = np.sum(self.losshistory.loss_test[start_step_idx:], axis=1)
            plt.plot(self.losshistory.steps[start_step_idx:], total_loss_test)
            for l in range(len(self.niter_weights)-1):
                plt.axvline(x=self.niter_weights[l], color='grey', linestyle='--', alpha=0.7)
            plt.title("Total test loss evolution")
            plt.ylabel('Total Loss Test')
            plt.xlabel('Step')
            if file_save_fig != None:
                this_file_save_fig = file_save_fig[:file_save_fig.rfind('.')] + "_total_loss_test" + file_save_fig[file_save_fig.rfind('.'):]
                plt.savefig(this_file_save_fig, format='jpg')
        

    def gif_evolution(self, elements_to_plot={"simulation":[0,1], "data":[0,1], "prediction":[0,1]}, 
                      file_save_gif=None, nb_p_t=10000, max_gif_duration_in_sec=15):
        if self.trained == False:
            print("You need to train the model first")
            return None
        
        default_col_pal = plt.rcParams['axes.prop_cycle'].by_key()['color']
        
        steps_hist, param_hist = [], []
        with open(self.file_var_export, 'r') as file:
            for line in file:
                steps_hist.append(int(line.split(' ')[0]))
                param_hist.append( [float(value) for value in
                                    re.search(r'\[(.*?)\]', line).group(1).split(', ')] )
            steps_hist, param_hist = np.array(steps_hist), np.array(param_hist)
        
        all_t_simu, all_states_simu = [], []
        for k in range(np.shape(param_hist)[0]):
            params = {k: v for k, v in zip(list(self.dict_fitted_params.keys()), param_hist[k,:])}
            all_params = {**params, **self.dict_fixed_params}
            t_simu = np.linspace(self.t_min_geom, self.t_max_geom, nb_p_t)
            states_simu = odeint(self.model_derivatives, self.X_init, t_simu, 
                                 args=(all_params,), atol=1e-8, rtol=1e-11)
            all_t_simu.append(t_simu)
            all_states_simu.append(states_simu)
        # 10 images at the beginning and 10 at the end
        all_states_simu_with_rep = [all_states_simu[0]]*10 + all_states_simu + [all_states_simu[-1]]*20
        steps_hist_with_rep = [steps_hist[0]]*10 + list(steps_hist) + [steps_hist[-1]]*20
        # global_states_max = max([np.max(sim) for sim in all_states_simu] + [np.max(self.data.states_simu_all)] + [np.max(dat) for dat in self.data.states_data_noisy])
        # global_states_min = min([np.min(sim) for sim in all_states_simu] + [np.min(self.data.states_simu_all)] + [np.min(dat) for dat in self.data.states_data_noisy])
        global_states_max = max([np.max(self.data.states_simu_all)] + [np.max(dat) for dat in self.data.states_data_noisy])
        global_states_min = min([np.min(self.data.states_simu_all)] + [np.min(dat) for dat in self.data.states_data_noisy])

        fig, ax = plt.subplots(figsize=(21, 12))
        def update(frame):
            ax.clear()  # Clear the previous plot
            t_simu = np.linspace(self.t_min_geom, self.t_max_geom, nb_p_t)
            states_simu = all_states_simu_with_rep[frame]
            for i in range(len(self.X_init)):
                if "simulation" in elements_to_plot.keys():
                    ax.plot(self.data.t_simu_all, self.data.states_simu_all[:, i], '--', alpha=.5,
                            color=default_col_pal[i%len(default_col_pal)], label=self.data.labels_states[i]+" (model)")
                if "data" in elements_to_plot.keys():
                    ax.scatter(self.data.t_samples[i][:,0], self.data.states_data_noisy[i], 
                               color=default_col_pal[i%len(default_col_pal)], label=self.data.labels_states[i]+" (data)")
                if "prediction" in elements_to_plot.keys():
                    ax.plot(t_simu, states_simu[:, i], 
                            color=default_col_pal[i%len(default_col_pal)], label=self.data.labels_states[i]+" (simu PINN)")
            ax.set_xlabel('Time')
            ax.set_ylabel('States')
            ax.set_ylim((global_states_min - (global_states_max-global_states_min)*0.05, 
                         global_states_max + (global_states_max-global_states_min)*0.05))
            ax.legend(loc='upper right')
            ax.set_title(f'Step during training: {steps_hist_with_rep[frame]}')

        duration_per_frame = min(0.5, max_gif_duration_in_sec/np.shape(param_hist)[0])
        animation = FuncAnimation(fig, update, frames=len(all_states_simu_with_rep), interval=duration_per_frame)  # 50 milliseconds between frames
        
        if file_save_gif != None:
            animation.save(file_save_gif, writer='pillow', fps=1/duration_per_frame)  # 0.1 seconds per frame
        
        # plt.show()


