"""
This file contains the class sample_plan, which is used to generate a sample plan for the simulation
"""

import numpy as np
import matplotlib.pyplot as plt


class sample_plan:
    
    def __init__(self, tmax=500, nbpoint=50, plan_type="uniform", batch_params=None, custom_plan=None):
        """
        tmax : float
            maximum time of the simulation
        nbpoint : int
            number of points of the simulation
        plan_type : str
            type of the sampling plan, can be "uniform", "random", "batch_random", "batch_uniform" or "custom"
        batch_params : dict
            dictionary containing the batch sampling parameters
                > nbr_batch : int
                    number of batch (must be an integer, dividing nbpoint)
                > ratio_in_out_batch : float
                    ratio between the duration of the sampling in the batch and the total duration
        custom_plan : array
            personalized sampling plan (array of sampling times)
        """
        self.tmax = tmax
        self.nbpoint = nbpoint
        self.plan_type = plan_type
        self.batch_params = batch_params
        self.custom_plan = custom_plan
        self.t_samples = None
        self.get_samples()

    def get_samples(self):
        """
        Generate the sample plan according to the plan_type
        """
        if self.plan_type == "uniform":
            self.t_samples = np.linspace(0, self.tmax, self.nbpoint)
        
        elif self.plan_type == "random":
            self.t_samples = np.random.uniform(0, self.tmax, self.nbpoint)
        
        elif self.plan_type in ["batch_random", "batch_uniform"]:
            if self.batch_params == None:
                raise ValueError("Batch parameters not provided")
            elif self.batch_params['ratio_in_out_batch'] > 1:
                raise ValueError("Ratio in/out batch must be lower than 1")
            else: # the batch parameters are provided
                self.t_samples = np.array([])
                for i in range(self.batch_params['nbr_batch']):
                    t_min_this_batch = i / self.batch_params['nbr_batch'] * self.tmax
                    t_max_this_batch = t_min_this_batch + \
                            self.batch_params['ratio_in_out_batch'] \
                            * self.tmax / self.batch_params['nbr_batch']
                    if self.plan_type == "batch_random":
                        self.t_samples = np.append(
                            self.t_samples,
                            np.random.uniform(t_min_this_batch, t_max_this_batch,
                                                int(self.nbpoint / self.batch_params['nbr_batch']))
                        )
                    elif self.plan_type == "batch_uniform":
                        self.t_samples = np.append(
                            self.t_samples,
                            np.linspace(t_min_this_batch, t_max_this_batch,
                                            int(self.nbpoint / self.batch_params['nbr_batch']))
                    )
        
        elif self.plan_type == "custom":
            self.t_samples = self.custom_plan
            self.nbpoint = len(self.t_samples)
            self.tmax = np.max(self.t_samples)
        
        else:
            raise ValueError("Sample plan not recognized")

        if self.t_samples is not None:
            self.t_samples = np.sort(self.t_samples)
    

    def plot_samples(self, color=plt.rcParams['axes.prop_cycle'].by_key()['color'][0]):
        """
        Plot the sample plan
        """
        plt.scatter(self.t_samples, np.zeros_like(self.t_samples), color=color)
        plt.title(f"Sample plan - {self.plan_type}")
        plt.xlabel("Time")
        # plt.show()


