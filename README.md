# MSV_projet

**Sujet :** Physics Informed Neural networks pour la modélisation d'un réseau trophique

**TO DO :**
- Mettre modèle de base dans model_catalogue
- Redonner un sens aux paramètres -> simus sur 1000 jours, proies 
- 


- Benchmark scipy, CMA-ES, Filtre de Kalman et ajustement séquentiel
- Performance : temps de calcul, écart au carré entre donnée et prédiction, erreur sur les paramètres estimés -> par sampling plan
- Grande boucles sur les paramètres
- Identifier les paramètres pertinents / difficiles à trouver
- Commenter performance (PINN + scipy) et sauvegarde (PINN) dans 0-test_utils
